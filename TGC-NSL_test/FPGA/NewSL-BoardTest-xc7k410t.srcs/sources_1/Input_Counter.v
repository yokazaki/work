`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/02/06 17:09:50
// Design Name: 
// Module Name: Input_Counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Input_Counter(
    input CLK_IN,
    input reset_in,
    input counter_reset_in,
    input [16:0] glink0_in,
    input [16:0] glink1_in,
    input [16:0] glink2_in,
    input [16:0] glink3_in,
    input [16:0] glink4_in,
    input [16:0] glink5_in,
    input [16:0] glink6_in,
    input [16:0] glink7_in,
    input [16:0] glink8_in,
    input [16:0] glink9_in,
    input [16:0] glink10_in,
    input [16:0] glink11_in,
    output [15:0] glink0_counter_out,
    output [15:0] glink1_counter_out,
    output [15:0] glink2_counter_out,
    output [15:0] glink3_counter_out,
    output [15:0] glink4_counter_out,
    output [15:0] glink5_counter_out,
    output [15:0] glink6_counter_out,
    output [15:0] glink7_counter_out,
    output [15:0] glink8_counter_out,
    output [15:0] glink9_counter_out,
    output [15:0] glink10_counter_out,
    output [15:0] glink11_counter_out
    
    );
    
    wire RST_in = reset_in||counter_reset_in;
    wire [100:0] InputBuffer1;
    wire [100:0] InputBuffer2;
    wire [1:0] hpttrg0, hpttrg1, hpttrg2, hpttrg3, hpttrg4, hpttrg5, hpttrg6, hpttrg7, hpttrg8, hpttrg9, hpttrg10, hpttrg11;
    assign InputBuffer1 = {glink5_in, glink4_in, glink3_in, glink2_in, glink1_in, glink0_in};
    assign hpttrg0 = {1'b0 , |InputBuffer1[4:0]};
    assign hpttrg1 = {|InputBuffer1[21:17] , |InputBuffer1[11:7]};
    assign hpttrg2 = {|InputBuffer1[41:37] , |InputBuffer1[31:27]};
    assign hpttrg3 = {|InputBuffer1[61:57] , |InputBuffer1[51:47]};
    assign hpttrg4 = {|InputBuffer1[79:76] , |InputBuffer1[70:67]};
    assign hpttrg5 = {|InputBuffer1[96:93] , |InputBuffer1[88:85]};
    assign InputBuffer2 = {glink11_in, glink10_in, glink9_in, glink8_in, glink7_in, glink6_in};
    assign hpttrg6 = {1'b0 , |InputBuffer2[4:0]};
    assign hpttrg7 = {|InputBuffer2[21:17] , |InputBuffer2[11:7]};
    assign hpttrg8 = {|InputBuffer2[41:37] , |InputBuffer2[31:27]};
    assign hpttrg9 = {|InputBuffer2[61:57] , |InputBuffer2[51:47]};
    assign hpttrg10 = {|InputBuffer2[79:76] , |InputBuffer2[70:67]};
    assign hpttrg11 = {|InputBuffer2[96:93] , |InputBuffer2[88:85]};



Counter InputCounter_glink0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg0), .count_out(glink0_counter_out));
Counter InputCounter_glink1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg1), .count_out(glink1_counter_out));
Counter InputCounter_glink2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg2), .count_out(glink2_counter_out));
Counter InputCounter_glink3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg3), .count_out(glink3_counter_out));
Counter InputCounter_glink4(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg4), .count_out(glink4_counter_out));
Counter InputCounter_glink5(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg5), .count_out(glink5_counter_out));
Counter InputCounter_glink6(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg6), .count_out(glink6_counter_out));
Counter InputCounter_glink7(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg7), .count_out(glink7_counter_out));
Counter InputCounter_glink8(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg8), .count_out(glink8_counter_out));
Counter InputCounter_glink9(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg9), .count_out(glink9_counter_out));
Counter InputCounter_glink10(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg10), .count_out(glink10_counter_out));
Counter InputCounter_glink11(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg11), .count_out(glink11_counter_out));

endmodule
