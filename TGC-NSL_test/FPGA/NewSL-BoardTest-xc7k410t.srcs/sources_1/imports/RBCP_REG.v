/*******************************************************************************
*                                                                              *
* Module      : RBCP_REG                                                       *
* Version     : v 0.2.0 2010/03/31                                             *
*                                                                              *
* Description : Register file                                                  *
*                                                                              *
*                Copyright (c) 2010 Bee Beans Technologies Co.,Ltd.            *
*                All rights reserved                                           *
*                                                                              *
*******************************************************************************/

`define REG_INIT_X08 8'h08
`define REG_INIT_X09 8'h09
`define REG_INIT_X0A 8'h0A
`define REG_INIT_X0B 8'h0B
`define REG_INIT_X0C 8'h0C
`define REG_INIT_X0D 8'h0D
`define REG_INIT_X0E 8'h0E
`define REG_INIT_X0F 8'h0F

module RBCP_REG(
	// System
	CLK					,	// in	: System clock
	RST					,	// in	: System reset
	// RBCP I/F
//	RBCP_ACT			,	// in	: Active
	RBCP_ADDR			,	// in	: Address[31:0]
	RBCP_WE				,	// in	: Write enable
	RBCP_WD				,	// in	: Write data[7:0]
	RBCP_RE				,	// in	: Read enable
	RBCP_RD				,	// out	: Read data[7:0]
	RBCP_ACK				,	// out	: Acknowledge
	din0					,	// in
	din1					,	// in
	din2					,	// in
	din3					,	// in
	din4					,	// in
	din5					,	// in
	din6					,	// in
	din7					,	// in
	dout8					,	// out
	dout9					,	// out
	doutA					,	// out
	doutB					,	// out
	doutC					,	// out
	doutD					,	// out
	doutE					,	// out
	doutF						// out
);

//-------- Input/Output -------------
	input					CLK			;
	input					RST			;

//	input					RBCP_ACT		;
	input		[31:0]	RBCP_ADDR	;
	input					RBCP_WE		;
	input		[7:0]		RBCP_WD		;
	input					RBCP_RE		;
	output	[7:0]		RBCP_RD		;
	output				RBCP_ACK		;
	input		[7:0]		din0			;
	input		[7:0]		din1			;
	input		[7:0]		din2			;
	input		[7:0]		din3			;
	input		[7:0]		din4			;
	input		[7:0]		din5			;
	input		[7:0]		din6			;
	input		[7:0]		din7			;
	output	[7:0]		dout8			;
	output	[7:0]		dout9			;
	output	[7:0]		doutA			;
	output	[7:0]		doutB			;
	output	[7:0]		doutC			;
	output	[7:0]		doutD			;
	output	[7:0]		doutE			;
	output	[7:0]		doutF			;

//------------------------------------------------------------------------------
//	Control
//------------------------------------------------------------------------------
	reg	[31:0]	irAddr		;
	reg				irWe			;
	reg	[7:0]		irWd			;
	reg				irRe			;

	always@ (posedge CLK or posedge RST) begin
		if(RST)begin
			irAddr[31:0]	<= 0;
			irWe				<= 0;
			irWd[7:0]		<= 0;
			irRe				<= 0;
		end else begin
			irAddr[31:0]	<= RBCP_ADDR[31:0];
			irWe				<= RBCP_WE;
			irWd[7:0]		<= RBCP_WD[7:0];
			irRe				<= RBCP_RE;
		end
	end

	reg				regCs			;
	reg	[3:0]		regAddr		;
	reg	[7:0]		regWd			;
	reg				regWe			;
	reg				regRe			;

	always@ (posedge CLK) begin
		regCs				<= (irAddr[31:4]==28'd0);
		regAddr[3:0]	<= (irWe | irRe ? irAddr[3:0]	: regAddr[3:0]);
		regWd[7:0]		<= (irWe        ? irWd[7:0]   : regWd[7:0]   );
		regWe				<= irWe;
		regRe				<= irRe;
	end

//------------------------------------------------------------------------------
//	Registers
//------------------------------------------------------------------------------
	reg		[15:8]	regBe		;

	always@ (posedge CLK) begin
		regBe[8]		<= regCs & regWe & (regAddr[3:0] == 4'h8);
		regBe[9]		<= regCs & regWe & (regAddr[3:0] == 4'h9);
		regBe[10]	<= regCs & regWe & (regAddr[3:0] == 4'hA);
		regBe[11]	<= regCs & regWe & (regAddr[3:0] == 4'hB);
		regBe[12]	<= regCs & regWe & (regAddr[3:0] == 4'hC);
		regBe[13]	<= regCs & regWe & (regAddr[3:0] == 4'hD);
		regBe[14]	<= regCs & regWe & (regAddr[3:0] == 4'hE);
		regBe[15]	<= regCs & regWe & (regAddr[3:0] == 4'hF);
	end

//--------------------------------------
//	Register file
//--------------------------------------
	reg		[7:0]	regX08Data		;
	reg		[7:0]	regX09Data		;
	reg		[7:0]	regX0AData		;
	reg		[7:0]	regX0BData		;
	reg		[7:0]	regX0CData		;
	reg		[7:0]	regX0DData		;
	reg		[7:0]	regX0EData		;
	reg		[7:0]	regX0FData		;

	always@ (posedge CLK or posedge RST) begin
		if(RST)begin
			regX08Data[7:0]	<= `REG_INIT_X08;
			regX09Data[7:0]	<= `REG_INIT_X09;
			regX0AData[7:0]	<= `REG_INIT_X0A;
			regX0BData[7:0]	<= `REG_INIT_X0B;
			regX0CData[7:0]	<= `REG_INIT_X0C;
			regX0DData[7:0]	<= `REG_INIT_X0D;
			regX0EData[7:0]	<= `REG_INIT_X0E;
			regX0FData[7:0]	<= `REG_INIT_X0F;

		end else begin
			if(regBe[8])begin
				regX08Data[7:0]	<= regWd[7:0];
			end
			if(regBe[9])begin
				regX09Data[7:0]	<= regWd[7:0];
			end
			if(regBe[10])begin
				regX0AData[7:0]	<= regWd[7:0];
			end
			if(regBe[11])begin
				regX0BData[7:0]	<= regWd[7:0];
			end
			if(regBe[12])begin
				regX0CData[7:0]	<= regWd[7:0];
			end
			if(regBe[13])begin
				regX0DData[7:0]	<= regWd[7:0];
			end
			if(regBe[14])begin
				regX0EData[7:0]	<= regWd[7:0];
			end
			if(regBe[15])begin
				regX0FData[7:0]	<= regWd[7:0];
			end
		end
	end


	wire	[7:0]	X00Data		;
	wire	[7:0]	X01Data		;
	wire	[7:0]	X02Data		;
	wire	[7:0]	X03Data		;
	wire	[7:0]	X04Data		;
	wire	[7:0]	X05Data		;
	wire	[7:0]	X06Data		;
	wire	[7:0]	X07Data		;
	wire	[7:0]	X08Data		;
	wire	[7:0]	X09Data		;
	wire	[7:0]	X0AData		;
	wire	[7:0]	X0BData		;
	wire	[7:0]	X0CData		;
	wire	[7:0]	X0DData		;
	wire	[7:0]	X0EData		;
	wire	[7:0]	X0FData		;


	assign	X00Data[7:0] = din0[7:0];
	assign	X01Data[7:0] = din1[7:0];
	assign	X02Data[7:0] = din2[7:0];
	assign	X03Data[7:0] = din3[7:0];
	assign	X04Data[7:0] = din4[7:0];
	assign	X05Data[7:0] = din5[7:0];
	assign	X06Data[7:0] = din6[7:0];
	assign	X07Data[7:0] = din7[7:0];
	assign	X08Data[7:0] = regX08Data[7:0];
	assign	X09Data[7:0] = regX09Data[7:0];
	assign	X0AData[7:0] = regX0AData[7:0];
	assign	X0BData[7:0] = regX0BData[7:0];
	assign	X0CData[7:0] = regX0CData[7:0];
	assign	X0DData[7:0] = regX0DData[7:0];
	assign	X0EData[7:0] = regX0EData[7:0];
	assign	X0FData[7:0] = regX0FData[7:0];

//--------------------------------------
//	Read data mux.
//--------------------------------------
	reg		[7:0]	muxRegDataA		;
	reg				muxRegAck		;

	always@ (posedge CLK) begin
		case(regAddr[3:0])
			4'h0:	muxRegDataA[7:0] 		<= X00Data[7:0];
			4'h1:	muxRegDataA[7:0] 		<= X01Data[7:0];
			4'h2:	muxRegDataA[7:0] 		<= X02Data[7:0];
			4'h3:	muxRegDataA[7:0] 		<= X03Data[7:0];
			4'h4:	muxRegDataA[7:0] 		<= X04Data[7:0];
			4'h5:	muxRegDataA[7:0] 		<= X05Data[7:0];
			4'h6:	muxRegDataA[7:0] 		<= X06Data[7:0];
			4'h7:	muxRegDataA[7:0] 		<= X07Data[7:0];
			4'h8:	muxRegDataA[7:0] 		<= X08Data[7:0];
			4'h9:	muxRegDataA[7:0] 		<= X09Data[7:0];
			4'hA:	muxRegDataA[7:0] 		<= X0AData[7:0];
			4'hB:	muxRegDataA[7:0] 		<= X0BData[7:0];
			4'hC:	muxRegDataA[7:0] 		<= X0CData[7:0];
			4'hD:	muxRegDataA[7:0] 		<= X0DData[7:0];
			4'hE:	muxRegDataA[7:0]		<= X0EData[7:0];
			default:muxRegDataA[7:0]	<= X0FData[7:0];
		endcase

		muxRegAck		<= regCs & (regRe | regWe);
	end

//------------------------------------------------------------------------------
//	Output
//------------------------------------------------------------------------------
	reg				orAck			;
	reg		[7:0]	orRd			;

	always@ (posedge CLK) begin
		orAck		<= muxRegAck;
		orRd[7:0]	<=	(muxRegAck		? muxRegDataA[7:0]  : 8'd0);
	end

	wire			RBCP_ACK		;
	wire	[7:0]	RBCP_RD		;

	assign	RBCP_ACK			= orAck;
	assign	RBCP_RD[7:0]	= orRd[7:0];
	assign	dout8[7:0] 		= X08Data[7:0];
	assign	dout9[7:0] 		= X09Data[7:0];
	assign	doutA[7:0] 		= X0AData[7:0];
	assign	doutB[7:0] 		= X0BData[7:0];
	assign	doutC[7:0] 		= X0CData[7:0];
	assign	doutD[7:0] 		= X0DData[7:0];
	assign	doutE[7:0] 		= X0EData[7:0];
	assign	doutF[7:0] 		= X0FData[7:0];

//------------------------------------------------------------------------------
endmodule
