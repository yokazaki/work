`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2019/12/05 17:53:52
// Design Name: 
// Module Name: GLink_COMS33_IFD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_IFD
   // width of the data for the system
#(parameter SYS_W = 1,
// width of the data for the device
parameter DEV_W = 1,
//  iostandard
parameter IOSTANDARD = "LVCOMS33")
(
// From the system into the device
input  [SYS_W-1:0] data_in_from_pins,
output [DEV_W-1:0] data_in_to_device,
//output [DEV_W*4-1:0] data_in_to_device_all,
input              clk_in,        // Fast clock input from PLL/MMCM
input              clk_40_in,
input  [3:0]       phase_in,
input              io_reset);
wire clock_enable = 1'b1;
// Signal declarations
////------------------------------
// After the buffer
wire   [SYS_W-1:0] data_in_from_pins_int;
// Between the delay and serdes
wire [SYS_W-1:0]  data_in_from_pins_delay;
// Create the clock logic
//reg [SYS_W*4-1:0] data_in_from_pins_4phase;
//assign data_in_to_device_all[DEV_W*4-1:0] = data_in_from_pins_4phase[SYS_W*4-1:0];
// We have multiple bits- step over every bit, instantiating the required elements
genvar pin_count;
generate for (pin_count = 0; pin_count < SYS_W; pin_count = pin_count + 1) begin: pins
 // Instantiate the buffers
 ////------------------------------
 // Instantiate a buffer for every bit of the data bus
 IBUF
   #(.IOSTANDARD (IOSTANDARD))
  ibuf_inst
    (.I          (data_in_from_pins    [pin_count]),
     .O          (data_in_from_pins_int[pin_count]));

 // Pass through the delay
 ////-------------------------------
assign data_in_from_pins_delay[pin_count] = data_in_from_pins_int[pin_count];

 // Connect the delayed data to the fabric
 ////--------------------------------------

 // Pack the registers into the IOB

 wire data_in_to_device_int;
 reg [3:0] data_in_from_int;
 reg data_in_phase_select;
 (* IOB = "true" *)
 FDRE fdre_in_inst
   (.D              (data_in_from_pins_delay[pin_count]),
    .C              (clk_in),
    .CE             (clock_enable),
    .R              (io_reset),
    .Q              (data_in_to_device_int)
   );
 //assign data_in_to_device[pin_count] = data_in_to_device_int;
 
 always@(posedge clk_in) begin
    data_in_from_int[3:0] <= {data_in_from_int[2:0], data_in_to_device_int};
 end
 
 always@(posedge clk_40_in) begin
    if(phase_in == 4'h0) data_in_phase_select <= data_in_from_int[0];
    else if(phase_in == 4'h1) data_in_phase_select <= data_in_from_int[1];
    else if(phase_in == 4'h2) data_in_phase_select <= data_in_from_int[2];
    else if(phase_in == 4'h3) data_in_phase_select <= data_in_from_int[3];
    else data_in_phase_select <= data_in_from_int[0];
//    data_in_from_pins_4phase[pin_count] <= data_in_from_int[0];
//    data_in_from_pins_4phase[pin_count+21] <= data_in_from_int[1];
//    data_in_from_pins_4phase[pin_count+42] <= data_in_from_int[2];
//    data_in_from_pins_4phase[pin_count+63] <= data_in_from_int[3];
 end
 
 assign data_in_to_device[pin_count] = data_in_phase_select;
 
end
endgenerate

endmodule
