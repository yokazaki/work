`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/06/06 14:46:46
// Design Name: 
// Module Name: vme
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define    Addr_TEST                         12'h0
`define    Addr_BITFILE_VERSION              12'hb

`define    Addr_NIM_OUT                      12'h1

`define    Addr_RESET                        12'h101
`define    Addr_SCALER_RESET                 12'h102
`define    Addr_GTX_TX_RESET                 12'h103
`define    Addr_GTX_RX_RESET                 12'h104
`define    Addr_DELAY_RESET                  12'h105
//`define    Addr_L1BUFFER_RESET               12'h106
//`define    Addr_DERANDOMIZER_RESET           12'h107
`define    Addr_RESET_40                     12'h106
`define    Addr_RESET_160                    12'h107
`define    Addr_ZERO_SUPP_RESET              12'h108
`define    Addr_CLK_RESET                    12'h109
`define    Addr_FIFO_RESET                   12'h10a
`define    Addr_SITCP_RESET                  12'h10b
`define    Addr_SITCP_FIFO_RESET             12'h10c
`define    Addr_TX_LOGIC_RESET               12'h10d
`define    Addr_MONITORING_RESET             12'h10e
`define    Addr_LUT_INIT_RESET               12'h10f


`define    Addr_GT0_RX_RESET               12'h110
`define    Addr_GT1_RX_RESET               12'h111
`define    Addr_GT2_RX_RESET               12'h112
`define    Addr_GT3_RX_RESET               12'h113
`define    Addr_GT4_RX_RESET               12'h114
`define    Addr_GT5_RX_RESET               12'h115
`define    Addr_GT6_RX_RESET               12'h116
`define    Addr_GT7_RX_RESET               12'h117
`define    Addr_GT8_RX_RESET               12'h118
`define    Addr_GT9_RX_RESET               12'h119
`define    Addr_GT10_RX_RESET               12'h11a
`define    Addr_GT11_RX_RESET               12'h11b

`define    Addr_ERROR_SCALER_GT0             12'h200
`define    Addr_ERROR_SCALER_GT1             12'h201
`define    Addr_ERROR_SCALER_GT2             12'h202
`define    Addr_ERROR_SCALER_GT3             12'h203
`define    Addr_ERROR_SCALER_GT4             12'h204
`define    Addr_ERROR_SCALER_GT5             12'h205
`define    Addr_ERROR_SCALER_GT6             12'h206
`define    Addr_ERROR_SCALER_GT7             12'h207
`define    Addr_ERROR_SCALER_GT8             12'h208
`define    Addr_ERROR_SCALER_GT9             12'h209
`define    Addr_ERROR_SCALER_GT10            12'h20a
`define    Addr_ERROR_SCALER_GT11            12'h20b

`define    Addr_STATUS_GT0                   12'h210
`define    Addr_STATUS_GT1                   12'h211
`define    Addr_STATUS_GT2                   12'h212
`define    Addr_STATUS_GT3                   12'h213
`define    Addr_STATUS_GT4                   12'h214
`define    Addr_STATUS_GT5                   12'h215
`define    Addr_STATUS_GT6                   12'h216
`define    Addr_STATUS_GT7                   12'h217
`define    Addr_STATUS_GT8                   12'h218
`define    Addr_STATUS_GT9                   12'h219
`define    Addr_STATUS_GT10                  12'h21a
`define    Addr_STATUS_GT11                  12'h21b


`define    Addr_DELAY_GTX0                   12'h300
`define    Addr_DELAY_GTX1                   12'h301
`define    Addr_DELAY_GTX2                   12'h302
`define    Addr_DELAY_GTX3                   12'h303
`define    Addr_DELAY_GTX4                   12'h304
`define    Addr_DELAY_GTX5                   12'h305
`define    Addr_DELAY_GTX6                   12'h306
`define    Addr_DELAY_GTX7                   12'h307
`define    Addr_DELAY_GTX8                   12'h308
`define    Addr_DELAY_GTX9                   12'h309
`define    Addr_DELAY_GTX10                  12'h30a
`define    Addr_DELAY_GTX11                  12'h30b
`define    Addr_Phase_glink0                 12'h310
`define    Addr_Phase_glink1                 12'h311
`define    Addr_Phase_glink2                 12'h312
`define    Addr_Phase_glink3                 12'h313
`define    Addr_Phase_glink4                 12'h314
`define    Addr_Phase_glink5                 12'h315
`define    Addr_Phase_glink6                 12'h316
`define    Addr_Phase_glink7                 12'h317
`define    Addr_Phase_glink8                 12'h318
`define    Addr_Phase_glink9                 12'h319
`define    Addr_Phase_glink10                12'h31a
`define    Addr_Phase_glink11                12'h31b
`define    Addr_Phase_glink12                12'h31c
`define    Addr_Phase_glink13                12'h31d

`define    Addr_DELAY_L1A                    12'h320
`define    Addr_DELAY_BCR                    12'h321
`define    Addr_DELAY_ECR                    12'h322
`define    Addr_DELAY_TTC_RESET              12'h323
`define    Addr_DELAY_TEST_PULSE             12'h324
`define    Addr_DELAY_TRIG_BCR               12'h325

`define    Addr_MASK_RX0                    12'h330
`define    Addr_MASK_RX1                    12'h331
`define    Addr_MASK_RX2                    12'h332
`define    Addr_MASK_RX3                    12'h333
`define    Addr_MASK_RX4                    12'h334
`define    Addr_MASK_RX5                    12'h335
`define    Addr_MASK_RX6                    12'h336
`define    Addr_MASK_RX7                    12'h337
`define    Addr_MASK_RX8                    12'h338
`define    Addr_MASK_RX9                    12'h339
`define    Addr_MASK_RX10                   12'h33a
`define    Addr_MASK_RX11                   12'h33b

`define    Addr_MASK_L1A                    12'h340
`define    Addr_MASK_BCR                    12'h341
`define    Addr_MASK_ECR                    12'h342
`define    Addr_MASK_TTC_RESET              12'h343
`define    Addr_MASK_TEST_PULSE             12'h344
`define    Addr_MASK_TRIG_BCR               12'h345

`define    Addr_MASK_GLINK0                 12'h350
`define    Addr_MASK_GLINK1                 12'h351
`define    Addr_MASK_GLINK2                 12'h352
`define    Addr_MASK_GLINK3                 12'h353
`define    Addr_MASK_GLINK4                 12'h354
`define    Addr_MASK_GLINK5                 12'h355
`define    Addr_MASK_GLINK6                 12'h356
`define    Addr_MASK_GLINK7                 12'h357
`define    Addr_MASK_GLINK8                 12'h358
`define    Addr_MASK_GLINK9                 12'h359
`define    Addr_MASK_GLINK10                12'h35a
`define    Addr_MASK_GLINK11                12'h35b
`define    Addr_MASK_GLINK12                12'h35c
`define    Addr_MASK_GLINK13                12'h35d


`define    Addr_TEST_PULSE_LENGTH           12'h360
`define    Addr_TEST_PULSE_ENABLE           12'h361
`define    Addr_TEST_PULSE_WAIT_LENGTH      12'h362

`define    Addr_CHECK_PATTERN0                12'h370
`define    Addr_CHECK_PATTERN1                12'h371
`define    Addr_CHECK_PATTERN2                12'h372
`define    Addr_CHECK_PATTERN3                12'h373
`define    Addr_CHECKER_SELECTOR1             12'h374
`define    Addr_CHECKER_SELECTOR2             12'h375

`define    Addr_PATTERN_WIRE1                 12'h37a
`define    Addr_PATTERN_WIRE2                 12'h37b
`define    Addr_PATTERN_STRIP1                12'h37c
`define    Addr_PATTERN_STRIP2                12'h37d

`define    Addr_INPUT_COUNTER_GLINK0        12'h380
`define    Addr_INPUT_COUNTER_GLINK1        12'h381
`define    Addr_INPUT_COUNTER_GLINK2        12'h382
`define    Addr_INPUT_COUNTER_GLINK3        12'h383
`define    Addr_INPUT_COUNTER_GLINK4        12'h384
`define    Addr_INPUT_COUNTER_GLINK5        12'h385
`define    Addr_INPUT_COUNTER_GLINK6        12'h386
`define    Addr_INPUT_COUNTER_GLINK7        12'h387
`define    Addr_INPUT_COUNTER_GLINK8        12'h388
`define    Addr_INPUT_COUNTER_GLINK9        12'h389
`define    Addr_INPUT_COUNTER_GLINK10       12'h38a
`define    Addr_INPUT_COUNTER_GLINK11       12'h38b

`define    Addr_PATTERN_CHECKER_GLINK0        12'h390
`define    Addr_PATTERN_CHECKER_GLINK1        12'h391
`define    Addr_PATTERN_CHECKER_GLINK2        12'h392
`define    Addr_PATTERN_CHECKER_GLINK3        12'h393
`define    Addr_PATTERN_CHECKER_GLINK4        12'h394
`define    Addr_PATTERN_CHECKER_GLINK5        12'h395
`define    Addr_PATTERN_CHECKER_GLINK6        12'h396
`define    Addr_PATTERN_CHECKER_GLINK7        12'h397
`define    Addr_PATTERN_CHECKER_GLINK8        12'h398
`define    Addr_PATTERN_CHECKER_GLINK9        12'h399
`define    Addr_PATTERN_CHECKER_GLINK10        12'h39a
`define    Addr_PATTERN_CHECKER_GLINK11        12'h39b

`define    Addr_L1A_COUNTER0                12'h400
`define    Addr_L1A_COUNTER1                12'h401
`define    Addr_L1ID                        12'h402
`define    Addr_BCID                        12'h403
`define    Addr_SLID                        12'h404
`define    Addr_READOUT_BC                  12'h405
`define    Addr_L1BUFFER_DEPTH              12'h406
`define    Addr_TRIGL1BUFFER_DEPTH          12'h407
`define    Addr_L1BUFFER_BW_DEPTH           12'h408

`define    Addr_L1BUFFER_STATUS             12'h500

`define    Addr_DERANDOMIZER_STATUS         12'h510
`define    Addr_DERANDOMIZER_COUNT1_1       12'h511
`define    Addr_DERANDOMIZER_COUNT1_2       12'h512
`define    Addr_DERANDOMIZER_COUNT2         12'h513
`define    Addr_DERANDOMIZER_COUNT3         12'h514

`define    Addr_ZERO_SUPPRESS_STATUS        12'h520
`define    Addr_ZERO_SUPPRESS_COUNT         12'h521
`define    Addr_DATA_SIZE                   12'h522

`define    Addr_SITCP_COUNT                 12'h530

`define    Addr_BUSY_COUNT                  12'h540


`define    Addr_FULLBOARD_ID                12'h680

`define    Addr_DEBUG_MONITORING_FIFO_OUT_0        12'h600
`define    Addr_DEBUG_MONITORING_FIFO_OUT_1        12'h601
`define    Addr_DEBUG_MONITORING_FIFO_OUT_2        12'h602
`define    Addr_DEBUG_MONITORING_FIFO_OUT_3        12'h603
`define    Addr_DEBUG_MONITORING_FIFO_OUT_4        12'h604
`define    Addr_DEBUG_MONITORING_FIFO_OUT_5        12'h605
`define    Addr_DEBUG_MONITORING_FIFO_OUT_6        12'h606
`define    Addr_DEBUG_MONITORING_FIFO_OUT_7        12'h607
`define    Addr_DEBUG_MONITORING_FIFO_OUT_8        12'h608
`define    Addr_DEBUG_MONITORING_FIFO_OUT_9        12'h609
`define    Addr_DEBUG_MONITORING_FIFO_OUT_10       12'h60a
`define    Addr_DEBUG_MONITORING_FIFO_OUT_11       12'h60b


`define    Addr_MONITORING_FIFO_OUT_0        12'h700
`define    Addr_MONITORING_FIFO_OUT_1        12'h701
`define    Addr_MONITORING_FIFO_OUT_2        12'h702
`define    Addr_MONITORING_FIFO_OUT_3        12'h703
`define    Addr_MONITORING_FIFO_OUT_4        12'h704
`define    Addr_MONITORING_FIFO_OUT_5        12'h705
`define    Addr_MONITORING_FIFO_OUT_6        12'h706
`define    Addr_MONITORING_FIFO_OUT_7        12'h707
`define    Addr_MONITORING_FIFO_OUT_8        12'h708
`define    Addr_MONITORING_FIFO_OUT_9        12'h709
`define    Addr_MONITORING_FIFO_OUT_10       12'h70a
`define    Addr_MONITORING_FIFO_OUT_11       12'h70b
`define    Addr_MONITORING_FIFO_OUT_12       12'h70c
`define    Addr_MONITORING_FIFO_OUT_13       12'h70d
`define    Addr_MONITORING_FIFO_OUT_14       12'h70e
`define    Addr_MONITORING_FIFO_OUT_15       12'h70f
`define    Addr_MONITORING_FIFO_OUT_16       12'h726
`define    Addr_MONITORING_FIFO_OUT_17       12'h727
`define    Addr_MONITORING_FIFO_OUT_18       12'h728
`define    Addr_MONITORING_FIFO_OUT_19       12'h729
`define    Addr_MONITORING_FIFO_OUT_20       12'h72a
`define    Addr_MONITORING_FIFO_OUT_21       12'h72b
`define    Addr_MONITORING_FIFO_OUT_22       12'h72c
`define    Addr_MONITORING_FIFO_OUT_23       12'h72d

`define    Addr_MONITORING_FIFO_wr_en        12'h710
`define    Addr_MONITORING_FIFO_rd_en        12'h711
`define    Addr_LANE_SELECTOR                12'h712
`define    Addr_XADC_MON_ENABLE              12'h713
`define    Addr_XADC_MONITOR_OUT             12'h714
`define    Addr_XADC_MON_ADDR                12'h715
`define    Addr_MONITORING_wr_cnt            12'h716
`define    Addr_SIGNAL_SELECTOR              12'h717
`define    Addr_CONTROL_FLAG                 12'h718

`define    Addr_COUNTER_DOWN                 12'h71a
`define    Addr_COUNTER_UP                   12'h71b
`define    Addr_CHECKER_DOWN                 12'h71c
`define    Addr_CHECKER_UP                   12'h71d


`define    Addr_L1A_MANUAL_PARAMETER         12'h720
`define    Addr_BUSY_PROPAGATION             12'h721

`define    Addr_GTX0_TEST_DATA1             12'h801
`define    Addr_GTX0_TEST_DATA2             12'h802
`define    Addr_GTX0_TEST_DATA3             12'h803
`define    Addr_GTX0_TEST_DATA4             12'h804
`define    Addr_GTX0_TEST_DATA5             12'h805
`define    Addr_GTX0_TEST_DATA6             12'h806
`define    Addr_GTX0_TEST_DATA7             12'h807
`define    Addr_GTX1_TEST_DATA1             12'h811
`define    Addr_GTX1_TEST_DATA2             12'h812
`define    Addr_GTX1_TEST_DATA3             12'h813
`define    Addr_GTX1_TEST_DATA4             12'h814
`define    Addr_GTX1_TEST_DATA5             12'h815
`define    Addr_GTX1_TEST_DATA6             12'h816
`define    Addr_GTX1_TEST_DATA7             12'h817
`define    Addr_GTX2_TEST_DATA1             12'h821
`define    Addr_GTX2_TEST_DATA2             12'h822
`define    Addr_GTX2_TEST_DATA3             12'h823
`define    Addr_GTX2_TEST_DATA4             12'h824
`define    Addr_GTX2_TEST_DATA5             12'h825
`define    Addr_GTX2_TEST_DATA6             12'h826
`define    Addr_GTX2_TEST_DATA7             12'h827
`define    Addr_GTX3_TEST_DATA1             12'h831
`define    Addr_GTX3_TEST_DATA2             12'h832
`define    Addr_GTX3_TEST_DATA3             12'h833
`define    Addr_GTX3_TEST_DATA4             12'h834
`define    Addr_GTX3_TEST_DATA5             12'h835
`define    Addr_GTX3_TEST_DATA6             12'h836
`define    Addr_GTX3_TEST_DATA7             12'h837
`define    Addr_GTX4_TEST_DATA1             12'h841
`define    Addr_GTX4_TEST_DATA2             12'h842
`define    Addr_GTX4_TEST_DATA3             12'h843
`define    Addr_GTX4_TEST_DATA4             12'h844
`define    Addr_GTX4_TEST_DATA5             12'h845
`define    Addr_GTX4_TEST_DATA6             12'h846
`define    Addr_GTX4_TEST_DATA7             12'h847
`define    Addr_GTX5_TEST_DATA1             12'h851
`define    Addr_GTX5_TEST_DATA2             12'h852
`define    Addr_GTX5_TEST_DATA3             12'h853
`define    Addr_GTX5_TEST_DATA4             12'h854
`define    Addr_GTX5_TEST_DATA5             12'h855
`define    Addr_GTX5_TEST_DATA6             12'h856
`define    Addr_GTX5_TEST_DATA7             12'h857
`define    Addr_GTX6_TEST_DATA1             12'h861
`define    Addr_GTX6_TEST_DATA2             12'h862
`define    Addr_GTX6_TEST_DATA3             12'h863
`define    Addr_GTX6_TEST_DATA4             12'h864
`define    Addr_GTX6_TEST_DATA5             12'h865
`define    Addr_GTX6_TEST_DATA6             12'h866
`define    Addr_GTX6_TEST_DATA7             12'h867
`define    Addr_GTX7_TEST_DATA1             12'h871
`define    Addr_GTX7_TEST_DATA2             12'h872
`define    Addr_GTX7_TEST_DATA3             12'h873
`define    Addr_GTX7_TEST_DATA4             12'h874
`define    Addr_GTX7_TEST_DATA5             12'h875
`define    Addr_GTX7_TEST_DATA6             12'h876
`define    Addr_GTX7_TEST_DATA7             12'h877
`define    Addr_GTX8_TEST_DATA1             12'h881
`define    Addr_GTX8_TEST_DATA2             12'h882
`define    Addr_GTX8_TEST_DATA3             12'h883
`define    Addr_GTX8_TEST_DATA4             12'h884
`define    Addr_GTX8_TEST_DATA5             12'h885
`define    Addr_GTX8_TEST_DATA6             12'h886
`define    Addr_GTX8_TEST_DATA7             12'h887
`define    Addr_GTX9_TEST_DATA1             12'h891
`define    Addr_GTX9_TEST_DATA2             12'h892
`define    Addr_GTX9_TEST_DATA3             12'h893
`define    Addr_GTX9_TEST_DATA4             12'h894
`define    Addr_GTX9_TEST_DATA5             12'h895
`define    Addr_GTX9_TEST_DATA6             12'h896
`define    Addr_GTX9_TEST_DATA7             12'h897
`define    Addr_GTX10_TEST_DATA1            12'h8a1
`define    Addr_GTX10_TEST_DATA2            12'h8a2
`define    Addr_GTX10_TEST_DATA3            12'h8a3
`define    Addr_GTX10_TEST_DATA4            12'h8a4
`define    Addr_GTX10_TEST_DATA5            12'h8a5
`define    Addr_GTX10_TEST_DATA6            12'h8a6
`define    Addr_GTX10_TEST_DATA7            12'h8a7
`define    Addr_GTX11_TEST_DATA1            12'h8b1
`define    Addr_GTX11_TEST_DATA2            12'h8b2
`define    Addr_GTX11_TEST_DATA3            12'h8b3
`define    Addr_GTX11_TEST_DATA4            12'h8b4
`define    Addr_GTX11_TEST_DATA5            12'h8b5
`define    Addr_GTX11_TEST_DATA6            12'h8b6
`define    Addr_GTX11_TEST_DATA7            12'h8b7

`define    Addr_GLINK0_TEST_DATA1          12'h901
`define    Addr_GLINK0_TEST_DATA2          12'h902
`define    Addr_GLINK1_TEST_DATA1          12'h911
`define    Addr_GLINK1_TEST_DATA2          12'h912
`define    Addr_GLINK2_TEST_DATA1          12'h921
`define    Addr_GLINK2_TEST_DATA2          12'h922
`define    Addr_GLINK3_TEST_DATA1          12'h931
`define    Addr_GLINK3_TEST_DATA2          12'h932
`define    Addr_GLINK4_TEST_DATA1          12'h941
`define    Addr_GLINK4_TEST_DATA2          12'h942
`define    Addr_GLINK5_TEST_DATA1          12'h951
`define    Addr_GLINK5_TEST_DATA2          12'h952
`define    Addr_GLINK6_TEST_DATA1          12'h961
`define    Addr_GLINK6_TEST_DATA2          12'h962
`define    Addr_GLINK7_TEST_DATA1          12'h971
`define    Addr_GLINK7_TEST_DATA2          12'h972
`define    Addr_GLINK8_TEST_DATA1          12'h981
`define    Addr_GLINK8_TEST_DATA2          12'h982
`define    Addr_GLINK9_TEST_DATA1          12'h991
`define    Addr_GLINK9_TEST_DATA2          12'h992
`define    Addr_GLINK10_TEST_DATA1         12'h9a1
`define    Addr_GLINK10_TEST_DATA2         12'h9a2
`define    Addr_GLINK11_TEST_DATA1         12'h9b1
`define    Addr_GLINK11_TEST_DATA2         12'h9b2
`define    Addr_GLINK12_TEST_DATA1         12'h9c1
`define    Addr_GLINK12_TEST_DATA2         12'h9c2
`define    Addr_GLINK13_TEST_DATA1         12'h9d1
`define    Addr_GLINK13_TEST_DATA2         12'h9d2

`define    Addr_L1A_MONITOR                 12'ha00
`define    Addr_BCR_MONITOR                 12'ha01
`define    Addr_ECR_MONITOR                 12'ha02
`define    Addr_TTC_RESET_MONITOR           12'ha03
`define    Addr_TEST_PULSE_MONITOR          12'ha04

// Decoder
`define    Addr_ALIGN_ETA_NSW_0             12'hb00
`define    Addr_ALIGN_ETA_NSW_1             12'hb01
`define    Addr_ALIGN_ETA_NSW_2             12'hb02
`define    Addr_ALIGN_ETA_NSW_3             12'hb03
`define    Addr_ALIGN_ETA_NSW_4             12'hb04
`define    Addr_ALIGN_ETA_NSW_5             12'hb05
`define    Addr_ALIGN_ETA_RPC               12'hb06
`define    Addr_ALIGN_PHI_NSW_0             12'hb10
`define    Addr_ALIGN_PHI_NSW_1             12'hb11
`define    Addr_ALIGN_PHI_NSW_2             12'hb12
`define    Addr_ALIGN_PHI_NSW_3             12'hb13
`define    Addr_ALIGN_PHI_NSW_4             12'hb14
`define    Addr_ALIGN_PHI_NSW_5             12'hb15
`define    Addr_ALIGN_PHI_RPC               12'hb16

`define    Addr_EYE_RD_EN                   12'hc20
`define    Addr_EYE_GO                      12'hc30
`define    Addr_DRP_ADDR                    12'hc31
`define    Addr_DRP_DI                      12'hc32
`define    Addr_DRP_ENCMD                   12'hc33
`define    Addr_DRP_WECMD                   12'hc34
`define    Addr_H_STEPSIZE                  12'hc60
`define    Addr_V_STEPSIZE                  12'hc61
`define    Addr_H_STEPMAX                   12'hc64
`define    Addr_V_STEPMAX                   12'hc65
`define    Addr_H_INIT                      12'hc66
`define    Addr_V_INIT                      12'hc67
`define    Addr_PRBS_RESET                  12'hc70
`define    Addr_PRBS_SEL                    12'hc71

`define    Addr_EYE0_DATA0                  12'hc80
`define    Addr_EYE0_DATA1                  12'hc90
`define    Addr_EYE1_DATA0                  12'hc81
`define    Addr_EYE1_DATA1                  12'hc91
`define    Addr_EYE2_DATA0                  12'hc82
`define    Addr_EYE2_DATA1                  12'hc92
`define    Addr_EYE3_DATA0                  12'hc83
`define    Addr_EYE3_DATA1                  12'hc93
`define    Addr_EYE4_DATA0                  12'hc84
`define    Addr_EYE4_DATA1                  12'hc94
`define    Addr_EYE5_DATA0                  12'hc85
`define    Addr_EYE5_DATA1                  12'hc95
`define    Addr_EYE6_DATA0                  12'hc86
`define    Addr_EYE6_DATA1                  12'hc96
`define    Addr_EYE7_DATA0                  12'hc87
`define    Addr_EYE7_DATA1                  12'hc97
`define    Addr_EYE8_DATA0                  12'hc88
`define    Addr_EYE8_DATA1                  12'hc98
`define    Addr_EYE9_DATA0                  12'hc89
`define    Addr_EYE9_DATA1                  12'hc99
`define    Addr_EYE10_DATA0                 12'hc8a
`define    Addr_EYE10_DATA1                 12'hc9a
`define    Addr_EYE11_DATA0                 12'hc8b
`define    Addr_EYE11_DATA1                 12'hc9b

`define    Addr_EYE0_DATA_COUNT             12'hca0
`define    Addr_EYE1_DATA_COUNT             12'hca1
`define    Addr_EYE2_DATA_COUNT             12'hca2
`define    Addr_EYE3_DATA_COUNT             12'hca3
`define    Addr_EYE4_DATA_COUNT             12'hca4
`define    Addr_EYE5_DATA_COUNT             12'hca5
`define    Addr_EYE6_DATA_COUNT             12'hca6
`define    Addr_EYE7_DATA_COUNT             12'hca7
`define    Addr_EYE8_DATA_COUNT             12'hca8
`define    Addr_EYE9_DATA_COUNT             12'hca9
`define    Addr_EYE10_DATA_COUNT            12'hcaa
`define    Addr_EYE11_DATA_COUNT            12'hcab

`define    Addr_EYE0_STATE                  12'hcb0
`define    Addr_EYE1_STATE                  12'hcb1
`define    Addr_EYE2_STATE                  12'hcb2
`define    Addr_EYE3_STATE                  12'hcb3
`define    Addr_EYE4_STATE                  12'hcb4
`define    Addr_EYE5_STATE                  12'hcb5
`define    Addr_EYE6_STATE                  12'hcb6
`define    Addr_EYE7_STATE                  12'hcb7
`define    Addr_EYE8_STATE                  12'hcb8
`define    Addr_EYE9_STATE                  12'hcb9
`define    Addr_EYE10_STATE                 12'hcba
`define    Addr_EYE11_STATE                 12'hcbb

`define    Addr_GT0_DRPDO                   12'hcc0
`define    Addr_GT1_DRPDO                   12'hcc1
`define    Addr_GT2_DRPDO                   12'hcc2
`define    Addr_GT3_DRPDO                   12'hcc3
`define    Addr_GT4_DRPDO                   12'hcc4
`define    Addr_GT5_DRPDO                   12'hcc5
`define    Addr_GT6_DRPDO                   12'hcc6
`define    Addr_GT7_DRPDO                   12'hcc7
`define    Addr_GT8_DRPDO                   12'hcc8
`define    Addr_GT9_DRPDO                   12'hcc9
`define    Addr_GT10_DRPDO                  12'hcca
`define    Addr_GT11_DRPDO                  12'hccb

`define    Addr_LPMEN                       12'hcd0

`define    Addr_ERROR_COUNTER_PHASE0_GLINK0        12'hd30
`define    Addr_ERROR_COUNTER_PHASE0_GLINK1        12'hd31
`define    Addr_ERROR_COUNTER_PHASE0_GLINK2        12'hd32
`define    Addr_ERROR_COUNTER_PHASE0_GLINK3        12'hd33
`define    Addr_ERROR_COUNTER_PHASE0_GLINK4        12'hd34
`define    Addr_ERROR_COUNTER_PHASE0_GLINK5        12'hd35
`define    Addr_ERROR_COUNTER_PHASE0_GLINK6        12'hd36
`define    Addr_ERROR_COUNTER_PHASE0_GLINK7        12'hd37
`define    Addr_ERROR_COUNTER_PHASE0_GLINK8        12'hd38
`define    Addr_ERROR_COUNTER_PHASE0_GLINK9        12'hd39
`define    Addr_ERROR_COUNTER_PHASE0_GLINK10       12'hd3a
`define    Addr_ERROR_COUNTER_PHASE0_GLINK11       12'hd3b
`define    Addr_ERROR_COUNTER_PHASE0_GLINK12       12'hd3c
`define    Addr_ERROR_COUNTER_PHASE0_GLINK13       12'hd3d
`define    Addr_ERROR_COUNTER_PHASE1_GLINK0        12'hd40
`define    Addr_ERROR_COUNTER_PHASE1_GLINK1        12'hd41
`define    Addr_ERROR_COUNTER_PHASE1_GLINK2        12'hd42
`define    Addr_ERROR_COUNTER_PHASE1_GLINK3        12'hd43
`define    Addr_ERROR_COUNTER_PHASE1_GLINK4        12'hd44
`define    Addr_ERROR_COUNTER_PHASE1_GLINK5        12'hd45
`define    Addr_ERROR_COUNTER_PHASE1_GLINK6        12'hd46
`define    Addr_ERROR_COUNTER_PHASE1_GLINK7        12'hd47
`define    Addr_ERROR_COUNTER_PHASE1_GLINK8        12'hd48
`define    Addr_ERROR_COUNTER_PHASE1_GLINK9        12'hd49
`define    Addr_ERROR_COUNTER_PHASE1_GLINK10       12'hd4a
`define    Addr_ERROR_COUNTER_PHASE1_GLINK11       12'hd4b
`define    Addr_ERROR_COUNTER_PHASE1_GLINK12       12'hd4c
`define    Addr_ERROR_COUNTER_PHASE1_GLINK13       12'hd4d
`define    Addr_ERROR_COUNTER_PHASE2_GLINK0        12'hd50
`define    Addr_ERROR_COUNTER_PHASE2_GLINK1        12'hd51
`define    Addr_ERROR_COUNTER_PHASE2_GLINK2        12'hd52
`define    Addr_ERROR_COUNTER_PHASE2_GLINK3        12'hd53
`define    Addr_ERROR_COUNTER_PHASE2_GLINK4        12'hd54
`define    Addr_ERROR_COUNTER_PHASE2_GLINK5        12'hd55
`define    Addr_ERROR_COUNTER_PHASE2_GLINK6        12'hd56
`define    Addr_ERROR_COUNTER_PHASE2_GLINK7        12'hd57
`define    Addr_ERROR_COUNTER_PHASE2_GLINK8        12'hd58
`define    Addr_ERROR_COUNTER_PHASE2_GLINK9        12'hd59
`define    Addr_ERROR_COUNTER_PHASE2_GLINK10       12'hd5a
`define    Addr_ERROR_COUNTER_PHASE2_GLINK11       12'hd5b
`define    Addr_ERROR_COUNTER_PHASE2_GLINK12       12'hd5c
`define    Addr_ERROR_COUNTER_PHASE2_GLINK13       12'hd5d
`define    Addr_ERROR_COUNTER_PHASE3_GLINK0        12'hd60
`define    Addr_ERROR_COUNTER_PHASE3_GLINK1        12'hd61
`define    Addr_ERROR_COUNTER_PHASE3_GLINK2        12'hd62
`define    Addr_ERROR_COUNTER_PHASE3_GLINK3        12'hd63
`define    Addr_ERROR_COUNTER_PHASE3_GLINK4        12'hd64
`define    Addr_ERROR_COUNTER_PHASE3_GLINK5        12'hd65
`define    Addr_ERROR_COUNTER_PHASE3_GLINK6        12'hd66
`define    Addr_ERROR_COUNTER_PHASE3_GLINK7        12'hd67
`define    Addr_ERROR_COUNTER_PHASE3_GLINK8        12'hd68
`define    Addr_ERROR_COUNTER_PHASE3_GLINK9        12'hd69
`define    Addr_ERROR_COUNTER_PHASE3_GLINK10       12'hd6a
`define    Addr_ERROR_COUNTER_PHASE3_GLINK11       12'hd6b
`define    Addr_ERROR_COUNTER_PHASE3_GLINK12       12'hd6c
`define    Addr_ERROR_COUNTER_PHASE3_GLINK13       12'hd6d

`define    Addr_GLINK_PATTERN0              12'hd10
`define    Addr_GLINK_PATTERN1              12'hd11
`define    Addr_GLINK_PATTERN2              12'hd12
`define    Addr_GLINK_PATTERN3              12'hd13
`define    Addr_GLINK_PATTERN4              12'hd14
`define    Addr_GLINK_PATTERN5              12'hd15
`define    Addr_GLINK_PATTERN6              12'hd16
`define    Addr_GLINK_PATTERN7              12'hd17
`define    Addr_GLINK_PATTERN8              12'hd18
`define    Addr_GLINK_PATTERN9              12'hd19
`define    Addr_GLINK_PATTERN10             12'hd1a
`define    Addr_GLINK_PATTERN11             12'hd1b
`define    Addr_GLINK_PATTERN12             12'hd1c
`define    Addr_GLINK_PATTERN13             12'hd1d
`define    Addr_GLINK_PATTERN14             12'hd1e
`define    Addr_GLINK_PATTERN15             12'hd1f
`define    Addr_GLINK_PATTERN16             12'hd20
`define    Addr_GLINK_PATTERN17             12'hd21
`define    Addr_GLINK_PATTERN18             12'hd22
`define    Addr_GLINK_PATTERN19             12'hd23
`define    Addr_GLINK_PATTERN20             12'hd24
`define    Addr_GLINK_PATTERN21             12'hd25
`define    Addr_GLINK_PATTERN22             12'hd26
`define    Addr_GLINK_PATTERN23             12'hd27
`define    Addr_GLINK_PATTERN24             12'hd28
`define    Addr_GLINK_PATTERN25             12'hd29
`define    Addr_GLINK_PATTERN26             12'hd2a
`define    Addr_GLINK_PATTERN27             12'hd2b

`define    Addr_EXT_IP1                       12'he01
`define    Addr_EXT_IP2                       12'he02



module vme  (
    input wire TTC_CLK,
    input wire CLK_40,
    input wire CLK_160,
    input wire TXUSRCLK_in,
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

    output reg Reset_TTC_out, // Global resets
    output reg Reset_40_out, // Global resets
    output reg Reset_160_out, // Global resets
    //output reg Reset_TX_out,  // Global resets

    output reg Scaler_reset_out,
    output reg GTX_TX_reset_out,
    output reg GTX_RX_reset_out,
    output reg Delay_reset_out,
    //output reg L1Buffer_reset_out,
    //output reg Derandomizer_reset_out,
    //output reg ZeroSupp_reset_out,
    output reg CLK_reset_out,
    output reg FIFO_reset_out,
    output reg SiTCP_reset_out,
    output reg SiTCP_FIFO_reset_out,
    output reg TX_Logic_reset_out,
    //output reg Monitoring_reset_out,
    //output reg LUT_init_reset_out,
    
    output reg gt0_rx_reset_out,
    output reg gt1_rx_reset_out,
    output reg gt2_rx_reset_out,
    output reg gt3_rx_reset_out,
    output reg gt4_rx_reset_out,
    output reg gt5_rx_reset_out,
    output reg gt6_rx_reset_out,
    output reg gt7_rx_reset_out,
    output reg gt8_rx_reset_out,
    output reg gt9_rx_reset_out,
    output reg gt10_rx_reset_out,
    output reg gt11_rx_reset_out,

    /*input wire [15:0] Error_Scaler_gt_0,
    input wire [15:0] Error_Scaler_gt_1,
    input wire [15:0] Error_Scaler_gt_2,
    input wire [15:0] Error_Scaler_gt_3,
    input wire [15:0] Error_Scaler_gt_4,
    input wire [15:0] Error_Scaler_gt_5,
    input wire [15:0] Error_Scaler_gt_6,
    input wire [15:0] Error_Scaler_gt_7,
    input wire [15:0] Error_Scaler_gt_8,
    input wire [15:0] Error_Scaler_gt_9,
    input wire [15:0] Error_Scaler_gt_10,
    input wire [15:0] Error_Scaler_gt_11,

    input wire [15:0] Status_gt_0,
    input wire [15:0] Status_gt_1,
    input wire [15:0] Status_gt_2,
    input wire [15:0] Status_gt_3,
    input wire [15:0] Status_gt_4,
    input wire [15:0] Status_gt_5,
    input wire [15:0] Status_gt_6,
    input wire [15:0] Status_gt_7,
    input wire [15:0] Status_gt_8,
    input wire [15:0] Status_gt_9,
    input wire [15:0] Status_gt_10,
    input wire [15:0] Status_gt_11,*/

    output reg [3:0] Phase_glink0,
    output reg [3:0] Phase_glink1,
    output reg [3:0] Phase_glink2,
    output reg [3:0] Phase_glink3,
    output reg [3:0] Phase_glink4,
    output reg [3:0] Phase_glink5,
    output reg [3:0] Phase_glink6,
    output reg [3:0] Phase_glink7,
    output reg [3:0] Phase_glink8,
    output reg [3:0] Phase_glink9,
    output reg [3:0] Phase_glink10,
    output reg [3:0] Phase_glink11,
    output reg [3:0] Phase_glink12,
    output reg [3:0] Phase_glink13,
            
    input wire [3:0] Phase_glink0_mon,
    input wire [3:0] Phase_glink1_mon,
    input wire [3:0] Phase_glink2_mon,
    input wire [3:0] Phase_glink3_mon,
    input wire [3:0] Phase_glink4_mon,
    input wire [3:0] Phase_glink5_mon,
    input wire [3:0] Phase_glink6_mon,
    input wire [3:0] Phase_glink7_mon,
    input wire [3:0] Phase_glink8_mon,
    input wire [3:0] Phase_glink9_mon,
    input wire [3:0] Phase_glink10_mon,
    input wire [3:0] Phase_glink11_mon,
    input wire [3:0] Phase_glink12_mon,
    input wire [3:0] Phase_glink13_mon,
    
    input wire [31:0] detect_pattern_wire,
    input wire [31:0] detect_pattern_wire2,
    input wire [31:0] detect_pattern_strip,
    
    input wire [31:0] counter,
    input wire [31:0] checker,
    
    input wire [31:0] glink0_counter,
    input wire [31:0] glink1_counter,
    input wire [31:0] glink2_counter,
    input wire [31:0] glink3_counter,
    input wire [31:0] glink4_counter,
    input wire [31:0] glink5_counter,
    input wire [31:0] glink6_counter,
    input wire [31:0] glink7_counter,
    input wire [31:0] glink8_counter,
    input wire [31:0] glink9_counter,
    input wire [31:0] glink10_counter,
    input wire [31:0] glink11_counter,

    input wire [31:0] glink0_checker,
    input wire [31:0] glink1_checker,
    input wire [31:0] glink2_checker,
    input wire [31:0] glink3_checker,
    input wire [31:0] glink4_checker,
    input wire [31:0] glink5_checker,
    input wire [31:0] glink6_checker,
    input wire [31:0] glink7_checker,
    input wire [31:0] glink8_checker,
    input wire [31:0] glink9_checker,
    input wire [31:0] glink10_checker,
    input wire [31:0] glink11_checker,

    output reg [1:0] pattern_selector1,
    output reg [1:0] pattern_selector2,
    output reg [4:0] check_pattern0,
    output reg [4:0] check_pattern1,
    output reg [4:0] check_pattern2,
    output reg [4:0] check_pattern3,
    
    output reg [203:0] glink_pattern,
    output reg [16:0] glink0_pattern,
    output reg [16:0] glink1_pattern,
    output reg [16:0] glink2_pattern,
    output reg [16:0] glink3_pattern,
    output reg [16:0] glink4_pattern,
    output reg [16:0] glink5_pattern,
    output reg [16:0] glink6_pattern,
    output reg [16:0] glink7_pattern,
    output reg [16:0] glink8_pattern,
    output reg [16:0] glink9_pattern,
    output reg [16:0] glink10_pattern,
    output reg [16:0] glink11_pattern,
    output reg [16:0] glink12_pattern,
    output reg [16:0] glink13_pattern,
    input wire [31:0] glink0_error_counter_phase0,
    input wire [31:0] glink1_error_counter_phase0,
    input wire [31:0] glink2_error_counter_phase0,
    input wire [31:0] glink3_error_counter_phase0,
    input wire [31:0] glink4_error_counter_phase0,
    input wire [31:0] glink5_error_counter_phase0,
    input wire [31:0] glink6_error_counter_phase0,
    input wire [31:0] glink7_error_counter_phase0,
    input wire [31:0] glink8_error_counter_phase0,
    input wire [31:0] glink9_error_counter_phase0,
    input wire [31:0] glink10_error_counter_phase0,
    input wire [31:0] glink11_error_counter_phase0,
    input wire [31:0] glink12_error_counter_phase0,
    input wire [31:0] glink13_error_counter_phase0,
    input wire [31:0] glink0_error_counter_phase1,
    input wire [31:0] glink1_error_counter_phase1,
    input wire [31:0] glink2_error_counter_phase1,
    input wire [31:0] glink3_error_counter_phase1,
    input wire [31:0] glink4_error_counter_phase1,
    input wire [31:0] glink5_error_counter_phase1,
    input wire [31:0] glink6_error_counter_phase1,
    input wire [31:0] glink7_error_counter_phase1,
    input wire [31:0] glink8_error_counter_phase1,
    input wire [31:0] glink9_error_counter_phase1,
    input wire [31:0] glink10_error_counter_phase1,
    input wire [31:0] glink11_error_counter_phase1,
    input wire [31:0] glink12_error_counter_phase1,
    input wire [31:0] glink13_error_counter_phase1,
    input wire [31:0] glink0_error_counter_phase2,
    input wire [31:0] glink1_error_counter_phase2,
    input wire [31:0] glink2_error_counter_phase2,
    input wire [31:0] glink3_error_counter_phase2,
    input wire [31:0] glink4_error_counter_phase2,
    input wire [31:0] glink5_error_counter_phase2,
    input wire [31:0] glink6_error_counter_phase2,
    input wire [31:0] glink7_error_counter_phase2,
    input wire [31:0] glink8_error_counter_phase2,
    input wire [31:0] glink9_error_counter_phase2,
    input wire [31:0] glink10_error_counter_phase2,
    input wire [31:0] glink11_error_counter_phase2,
    input wire [31:0] glink12_error_counter_phase2,
    input wire [31:0] glink13_error_counter_phase2,
    input wire [31:0] glink0_error_counter_phase3,
    input wire [31:0] glink1_error_counter_phase3,
    input wire [31:0] glink2_error_counter_phase3,
    input wire [31:0] glink3_error_counter_phase3,
    input wire [31:0] glink4_error_counter_phase3,
    input wire [31:0] glink5_error_counter_phase3,
    input wire [31:0] glink6_error_counter_phase3,
    input wire [31:0] glink7_error_counter_phase3,
    input wire [31:0] glink8_error_counter_phase3,
    input wire [31:0] glink9_error_counter_phase3,
    input wire [31:0] glink10_error_counter_phase3,
    input wire [31:0] glink11_error_counter_phase3,
    input wire [31:0] glink12_error_counter_phase3,
    input wire [31:0] glink13_error_counter_phase3,
    
    input wire [15:0] SiTCP_Count,
    input wire [15:0] monitoring_FIFO_out_0,
    input wire [15:0] monitoring_FIFO_out_1,
    input wire [15:0] monitoring_FIFO_out_2,
    input wire [15:0] monitoring_FIFO_out_3,
    input wire [15:0] monitoring_FIFO_out_4,
    input wire [15:0] monitoring_FIFO_out_5,
    input wire [15:0] monitoring_FIFO_out_6,
    input wire [15:0] monitoring_FIFO_out_7,
    input wire [15:0] monitoring_FIFO_out_8,
    input wire [15:0] monitoring_FIFO_out_9,
    input wire [15:0] monitoring_FIFO_out_10,
    input wire [15:0] monitoring_FIFO_out_11,
    input wire [15:0] monitoring_FIFO_out_12,
   input wire [15:0] monitoring_FIFO_out_13,
   input wire [15:0] monitoring_FIFO_out_14,
   input wire [15:0] monitoring_FIFO_out_15,
  input wire [15:0] monitoring_FIFO_out_16,
  input wire [15:0] monitoring_FIFO_out_17,
  input wire [15:0] monitoring_FIFO_out_18,
  input wire [15:0] monitoring_FIFO_out_19,
  input wire [15:0] monitoring_FIFO_out_20,
  input wire [15:0] monitoring_FIFO_out_21,
  input wire [15:0] monitoring_FIFO_out_22,
  input wire [15:0] monitoring_FIFO_out_23,

    input wire [15:0] debug_monitoring_FIFO_out_0,
    input wire [15:0] debug_monitoring_FIFO_out_1,
    input wire [15:0] debug_monitoring_FIFO_out_2,
    input wire [15:0] debug_monitoring_FIFO_out_3,
    input wire [15:0] debug_monitoring_FIFO_out_4,
    input wire [15:0] debug_monitoring_FIFO_out_5,
    input wire [15:0] debug_monitoring_FIFO_out_6,
    input wire [15:0] debug_monitoring_FIFO_out_7,
    input wire [15:0] debug_monitoring_FIFO_out_8,
    input wire [15:0] debug_monitoring_FIFO_out_9,
    input wire [15:0] debug_monitoring_FIFO_out_10,
    input wire [15:0] debug_monitoring_FIFO_out_11,

    output reg monitoring_FIFO_wr_en,
    output reg monitoring_FIFO_rd_en,
    input wire [13:0] monitoring_wr_cnt,
    output reg [6:0] lane_selector_out,
    output reg [1:0]  signal_selector_out,
    input wire control_flag,    
       
    input wire [15:0] L1A_counter,
    input wire [15:0] BCR_counter,
    input wire [15:0] ECR_counter,
    input wire [15:0] TTC_RESET_counter,
    input wire [15:0] TEST_PULSE_counter,

    output reg [7:0] NIM_OUT,
    input wire [31:0] eye0_data,
    input wire [31:0] eye1_data,
    input wire [31:0] eye2_data,
    input wire [31:0] eye3_data,
    input wire [31:0] eye4_data,
    input wire [31:0] eye5_data,
    input wire [31:0] eye6_data,
    input wire [31:0] eye7_data,
    input wire [31:0] eye8_data,
    input wire [31:0] eye9_data,
    input wire [31:0] eye10_data,
    input wire [31:0] eye11_data,
    output reg eye_rd_en,
    output reg eye_go,
    output reg [8:0] drpaddrReg,
    output reg [15:0] drpdiReg,
    output reg drpEnCmd,
    output reg drpWeCmd,
    input wire [14:0] eye0_data_count,
    input wire [14:0] eye1_data_count,
    input wire [14:0] eye2_data_count,
    input wire [14:0] eye3_data_count,
    input wire [14:0] eye4_data_count,
    input wire [14:0] eye5_data_count,
    input wire [14:0] eye6_data_count,
    input wire [14:0] eye7_data_count,
    input wire [14:0] eye8_data_count,
    input wire [14:0] eye9_data_count,
    input wire [14:0] eye10_data_count,
    input wire [14:0] eye11_data_count,
    input wire [4:0] eye0_state,
    input wire [4:0] eye1_state,
    input wire [4:0] eye2_state,
    input wire [4:0] eye3_state,
    input wire [4:0] eye4_state,
    input wire [4:0] eye5_state,
    input wire [4:0] eye6_state,
    input wire [4:0] eye7_state,
    input wire [4:0] eye8_state,
    input wire [4:0] eye9_state,
    input wire [4:0] eye10_state,
    input wire [4:0] eye11_state,
    output reg [11:0] hstepSize,
    output reg [10:0] vstepSize,
    output reg      prbs_reset,
    output reg  [2:0] prbssel,
    output reg [8:0] hstep_max,
    output reg [8:0] vstep_max,
    output reg [11:0] horz_init,
    output reg [10:0] vert_init,
    
    input wire [15:0] gt0_drpdo,
    input wire [15:0] gt1_drpdo,
    input wire [15:0] gt2_drpdo,
    input wire [15:0] gt3_drpdo,
    input wire [15:0] gt4_drpdo,
    input wire [15:0] gt5_drpdo,
    input wire [15:0] gt6_drpdo,
    input wire [15:0] gt7_drpdo,
    input wire [15:0] gt8_drpdo,
    input wire [15:0] gt9_drpdo,
    input wire [15:0] gt10_drpdo,
    input wire [15:0] gt11_drpdo,
    
        output reg [15:0] EXT_IP1,
    output reg [15:0] EXT_IP2,
    
    output reg lpmen
    );
    
    
      
    //VME protocol
    wire rs,ws;
    assign rs = !(!OE & !CE);
    assign ws = !(!WE & !CE);
    wire [15:0] VME_DIN;     //VME data input
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_DOUT;    //VME data output
    (* ASYNC_REG = "TRUE" *) reg [11:0] VME_Address;
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_Data;
    assign VME_DIN = VME_D;
    assign VME_D = ((!OE & !CE)) ? VME_DOUT : 16'hzzzz;

    reg [15:0] Test_reg;    
    reg [15:0] bitfile_version;
   
initial begin
    Test_reg <= 16'h0020;
    bitfile_version <= 16'h0021;

    Reset_TTC_out <= 1'b0;
    //Reset_TX_out <= 1'b0;
    Reset_40_out <= 1'b0;
    Reset_160_out <= 1'b0;
    Scaler_reset_out <= 1'b0;
    GTX_TX_reset_out <= 1'b0;
    GTX_RX_reset_out <= 1'b0;
    Delay_reset_out <= 1'b0;
    CLK_reset_out <= 1'b0;
    FIFO_reset_out <= 1'b0;
    SiTCP_reset_out <= 1'b0;
    TX_Logic_reset_out <= 1'b0;
    gt0_rx_reset_out <= 1'b0;
    gt1_rx_reset_out <= 1'b0;
    gt2_rx_reset_out <= 1'b0;
    gt3_rx_reset_out <= 1'b0;
    gt4_rx_reset_out <= 1'b0;
    gt5_rx_reset_out <= 1'b0;
    gt6_rx_reset_out <= 1'b0;
    gt7_rx_reset_out <= 1'b0;
    gt8_rx_reset_out <= 1'b0;
    gt9_rx_reset_out <= 1'b0;
    gt10_rx_reset_out <= 1'b0;
    gt11_rx_reset_out <= 1'b0;
    
    Phase_glink0 <= 4'h0;
    Phase_glink1 <= 4'h0;
    Phase_glink2 <= 4'h0;
    Phase_glink3 <= 4'h0;
    Phase_glink4 <= 4'h0;
    Phase_glink5 <= 4'h0;
    Phase_glink6 <= 4'h0;
    Phase_glink7 <= 4'h0;
    Phase_glink8 <= 4'h0;
    Phase_glink9 <= 4'h0;
    Phase_glink10 <= 4'h0;
    Phase_glink11 <= 4'h0;
    Phase_glink12 <= 4'h0;
    Phase_glink13 <= 4'h0;

    pattern_selector1 <= 2'b0;
    pattern_selector2 <= 2'b0;
    check_pattern0 <= 5'b00001;
    check_pattern1 <= 5'b00010;
    check_pattern2 <= 5'b00100;
    check_pattern3 <= 5'b01000;
    
    glink_pattern <= 204'b0;
    glink0_pattern <= 17'b0;
    glink1_pattern <= 17'b0;
    glink2_pattern <= 17'b0;
    glink3_pattern <= 17'b0;
    glink4_pattern <= 17'b0;
    glink5_pattern <= 17'b0;
    glink6_pattern <= 17'b0;
    glink7_pattern <= 17'b0;
    glink8_pattern <= 17'b0;
    glink9_pattern <= 17'b0;
    glink10_pattern <= 17'b0;
    glink11_pattern <= 17'b0;
    glink12_pattern <= 17'b0;
    glink13_pattern <= 17'b0;

    monitoring_FIFO_wr_en <= 1'b0;
    monitoring_FIFO_rd_en <= 1'b0;
    lane_selector_out <= 7'h0;
    signal_selector_out <= 2'h0;
    
    NIM_OUT <= 8'b0;
    
    eye_rd_en       <= 1'b0;
    eye_go          <= 1'b0;
    drpaddrReg      <= 9'b0;
    drpdiReg      <= 16'b0;
    drpEnCmd      <= 1'b0;
    drpWeCmd      <= 1'b0;
    hstepSize       <=12'h4;
    vstepSize       <=11'h4;
    prbs_reset      <=1'b0;
    prbssel <= 3'b001;
    hstep_max <= 9'd32;
    vstep_max <= 9'd64;
    horz_init <= 12'd63;
    vert_init <= 11'd127;
    
        EXT_IP1 <= 16'hc0a8;
    EXT_IP2 <= 16'hfe10;
    

    lpmen <= 1'b1;
end

reg [2:0] shift_reg_40;     
reg [2:0] shift_reg_TTC;     
reg [2:0] shift_reg_160;    
reg [2:0] shift_reg_TX;    
reg write_40; 
reg write_TTC; 
reg write_160; 
reg write_TX; 
        



// WRITE asynchronus
always@(posedge ws) begin
    case (VME_A)        
// Reset
    `Addr_NIM_OUT : begin
        NIM_OUT[7:0] <= VME_DIN[7:0];
    end
    default : begin
    end    
    endcase
end


// Write 40 MHz
always@(posedge TTC_CLK) begin
    shift_reg_TTC[2:0] <= {shift_reg_TTC[1:0], ws};
    if(write_TTC) begin
        write_TTC <= 1'b0;
    end
    else if (shift_reg_TTC[2]==1'b0 && shift_reg_TTC[0] == 1'b1) begin
        write_TTC <= 1'b1;
        case (VME_A)
        `Addr_SCALER_RESET : begin
            Scaler_reset_out <= VME_DIN[0];
        end
        default : begin
        end
        endcase
    end
end

always@(posedge TXUSRCLK_in) begin
    shift_reg_TX[2:0] <= {shift_reg_TX[1:0], ws};
    if(write_TX) begin
        write_TX <= 1'b0;
    end
    else if (shift_reg_TX[2]==1'b0 && shift_reg_TX[0]==1'b1) begin
        write_TX <= 1'b1;
        case (VME_A)
        `Addr_TX_LOGIC_RESET : begin
            TX_Logic_reset_out <= VME_DIN[0];
        end
        endcase
        end
end

// Write 40 MHz
always@(posedge CLK_40) begin
    shift_reg_40[2:0] <= {shift_reg_40[1:0], ws};
    if(write_40) begin
        write_40 <= 1'b0;
    end
    else if (shift_reg_40[2]==1'b0 && shift_reg_40[0] == 1'b1) begin
        write_40 <= 1'b1;
        case (VME_A)
        `Addr_TEST : begin
            Test_reg <= VME_DIN[15:0];
        end
        `Addr_RESET_40 : begin
            Reset_40_out <= VME_DIN[0];
        end        
        `Addr_RESET : begin
            Reset_TTC_out <= VME_DIN[0];
        end
        `Addr_DELAY_RESET : begin
            Delay_reset_out <= VME_DIN[0];
        end
        `Addr_CLK_RESET : begin
            CLK_reset_out <= VME_DIN[0];
        end
        `Addr_GTX_TX_RESET : begin
            GTX_TX_reset_out <= VME_DIN[0];
        end
        `Addr_GTX_RX_RESET : begin
            GTX_RX_reset_out <= VME_DIN[0];
        end
        `Addr_GT0_RX_RESET : begin
            gt0_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT1_RX_RESET : begin
            gt1_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT2_RX_RESET : begin
            gt2_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT3_RX_RESET : begin
            gt3_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT4_RX_RESET : begin
            gt4_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT5_RX_RESET : begin
            gt5_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT6_RX_RESET : begin
            gt6_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT7_RX_RESET : begin
            gt7_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT8_RX_RESET : begin
            gt8_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT9_RX_RESET : begin
            gt9_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT10_RX_RESET : begin
            gt10_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT11_RX_RESET : begin
            gt11_rx_reset_out <= VME_DIN[0];
        end
        `Addr_Phase_glink0 : begin
            Phase_glink0[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink1 : begin
            Phase_glink1[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink2 : begin
            Phase_glink2[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink3 : begin
            Phase_glink3[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink4 : begin
            Phase_glink4[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink5 : begin
            Phase_glink5[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink6 : begin
            Phase_glink6[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink7 : begin
            Phase_glink7[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink8 : begin
            Phase_glink8[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink9 : begin
            Phase_glink9[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink10 : begin
            Phase_glink10[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink11 : begin
            Phase_glink11[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink12 : begin
            Phase_glink12[3:0] <= VME_DIN[3:0];
        end
        `Addr_Phase_glink13 : begin
            Phase_glink13[3:0] <= VME_DIN[3:0];
        end
        
        `Addr_CHECKER_SELECTOR1 : begin
            pattern_selector1[1:0] <= VME_DIN[1:0];
        end        
        `Addr_CHECKER_SELECTOR2 : begin
            pattern_selector2[1:0] <= VME_DIN[1:0];
        end
        `Addr_CHECK_PATTERN0 : begin
            check_pattern0[4:0] <= VME_DIN[4:0];
        end
        `Addr_CHECK_PATTERN1 : begin
            check_pattern1[4:0] <= VME_DIN[4:0];
        end
        `Addr_CHECK_PATTERN2 : begin
            check_pattern2[4:0] <= VME_DIN[4:0];
        end
        `Addr_CHECK_PATTERN3 : begin
            check_pattern3[4:0] <= VME_DIN[4:0];
        end

        `Addr_GLINK_PATTERN0 : begin
            glink_pattern[15:0] <= VME_DIN[15:0];
            glink0_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN1 : begin
            glink_pattern[16] <= VME_DIN[0];
            glink0_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN2 : begin
            glink_pattern[32:17] <= VME_DIN[15:0];
            glink1_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN3 : begin
            glink_pattern[33] <= VME_DIN[0];
            glink1_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN4 : begin
            glink_pattern[49:34] <= VME_DIN[15:0];
            glink2_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN5 : begin
            glink_pattern[50] <= VME_DIN[0];
            glink2_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN6 : begin
            glink_pattern[66:51] <= VME_DIN[15:0];
            glink3_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN7 : begin
            glink_pattern[67] <= VME_DIN[0];
            glink3_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN8 : begin
            glink_pattern[83:68] <= VME_DIN[15:0];
            glink4_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN9 : begin
            glink_pattern[84] <= VME_DIN[0];
            glink4_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN10 : begin
            glink_pattern[100:85] <= VME_DIN[15:0];
            glink5_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN11 : begin
            glink_pattern[101] <= VME_DIN[0];
            glink5_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN12 : begin
            glink_pattern[117:102] <= VME_DIN[15:0];
            glink6_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN13 : begin
            glink_pattern[118] <= VME_DIN[0];
            glink6_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN14 : begin
            glink_pattern[134:119] <= VME_DIN[15:0];
            glink7_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN15 : begin
            glink_pattern[135] <= VME_DIN[0];
            glink7_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN16 : begin
            glink_pattern[151:136] <= VME_DIN[15:0];
            glink8_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN17 : begin
            glink_pattern[152] <= VME_DIN[0];
            glink8_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN18 : begin
            glink_pattern[168:153] <= VME_DIN[15:0];
            glink9_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN19 : begin
            glink_pattern[169] <= VME_DIN[0];
            glink9_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN20 : begin
            glink_pattern[185:170] <= VME_DIN[15:0];
            glink10_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN21 : begin
            glink_pattern[186] <= VME_DIN[0];
            glink10_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN22 : begin
            glink_pattern[202:187] <= VME_DIN[15:0];
            glink11_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN23 : begin
            glink_pattern[203] <= VME_DIN[0];
            glink11_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN24 : begin
            glink12_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN25 : begin
            glink12_pattern[16] <= VME_DIN[0];
        end
        `Addr_GLINK_PATTERN26 : begin
            glink13_pattern[15:0] <= VME_DIN[15:0];
        end
        `Addr_GLINK_PATTERN27 : begin
            glink13_pattern[16] <= VME_DIN[0];
        end

        `Addr_LANE_SELECTOR : begin
            lane_selector_out[6:0] <= VME_DIN[6:0];
        end
        `Addr_EYE_RD_EN : begin
            eye_rd_en    <= VME_DIN[0];
        end
        `Addr_EYE_GO : begin
            eye_go    <= VME_DIN[0];
        end
        `Addr_DRP_ADDR : begin
            drpaddrReg    <= VME_DIN[8:0];
        end
        `Addr_DRP_DI : begin
            drpdiReg    <= VME_DIN[15:0];
        end
        `Addr_DRP_ENCMD : begin
            drpEnCmd    <= VME_DIN[0];
        end
        `Addr_DRP_WECMD : begin
            drpWeCmd    <= VME_DIN[0];
        end
        `Addr_H_STEPSIZE : begin
            hstepSize    <= VME_DIN[11:0];
        end
        `Addr_V_STEPSIZE : begin
            vstepSize    <= VME_DIN[10:0];
        end
        `Addr_H_STEPMAX : begin
            hstep_max    <= VME_DIN[8:0];
        end
        `Addr_V_STEPMAX : begin
            vstep_max    <= VME_DIN[8:0];
        end
        `Addr_H_INIT : begin
            horz_init    <= VME_DIN[11:0];
        end
        `Addr_V_INIT : begin
            vert_init    <= VME_DIN[10:0];
        end
        `Addr_PRBS_RESET : begin
            prbs_reset    <= VME_DIN[0];
        end
        `Addr_PRBS_SEL : begin
            prbssel    <= VME_DIN[2:0];
        end
        `Addr_LPMEN : begin
            lpmen    <= VME_DIN[0];
        end
        `Addr_SIGNAL_SELECTOR  : begin
            signal_selector_out <= VME_DIN[1:0];
        end
                `Addr_EXT_IP1  : begin
            EXT_IP1 <= VME_DIN[15:0];
        end                                                                                                                                                                                                                                     
        `Addr_EXT_IP2  : begin
            EXT_IP2 <= VME_DIN[15:0];
        end                                                                                                                                                                                                                                     

        default : begin
        end
        endcase
    end
end



// WRITE 160 MHz
always@(posedge CLK_160) begin
    shift_reg_160[2:0] <= {shift_reg_160[1:0], ws};
    if(write_160) begin
        write_160 <= 1'b0;
    end
    else if (shift_reg_160[2]==1'b0 && shift_reg_160[0]==1'b1) begin
        write_160 <= 1'b1;
        case (VME_A)
        `Addr_RESET_160 : begin
            Reset_160_out <= VME_DIN[0];
        end        
        `Addr_SITCP_RESET : begin
            SiTCP_reset_out <= VME_DIN[0];
        end        
        `Addr_SITCP_FIFO_RESET : begin
            SiTCP_FIFO_reset_out <= VME_DIN[0];
        end        
        `Addr_FIFO_RESET : begin
            FIFO_reset_out <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_wr_en : begin
            monitoring_FIFO_wr_en <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_rd_en : begin
            monitoring_FIFO_rd_en    <= VME_DIN[0];
        end
        endcase
    end
end

    
    //Read
always@(negedge rs) begin     
    case (VME_A [11:0])

// Test and bifile version
    `Addr_TEST : begin
        VME_DOUT[15:0] <= Test_reg[15:0];
    end
    `Addr_BITFILE_VERSION : begin
        VME_DOUT[15:0] <= bitfile_version[15:0];
    end
    `Addr_NIM_OUT : begin
        VME_DOUT[15:0] <= {8'b0, NIM_OUT[7:0]};
    end


// Reset
    `Addr_RESET : begin
        VME_DOUT[15:0] <= {15'b0, Reset_TTC_out};
    end
    `Addr_SCALER_RESET : begin
        VME_DOUT[15:0] <= {15'b0, Scaler_reset_out};
    end
    `Addr_DELAY_RESET : begin
        VME_DOUT[15:0] <= {15'b0, Delay_reset_out};
    end
    `Addr_CLK_RESET : begin
        VME_DOUT[15:0] <= {15'b0, CLK_reset_out};
    end
    `Addr_GTX_TX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, GTX_TX_reset_out};
    end
    `Addr_GTX_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, GTX_RX_reset_out};
    end
    `Addr_TX_LOGIC_RESET : begin
      VME_DOUT[15:0] <= {15'b0, TX_Logic_reset_out};
    end
    `Addr_FIFO_RESET : begin
        VME_DOUT[15:0] <= {15'b0, FIFO_reset_out};
    end
    `Addr_SITCP_RESET : begin
        VME_DOUT[15:0] <= {15'b0, SiTCP_reset_out};
    end
    `Addr_SITCP_FIFO_RESET : begin
        VME_DOUT[15:0] <= {15'b0, SiTCP_FIFO_reset_out};
    end
        `Addr_GT0_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt0_rx_reset_out};
    end
    `Addr_GT1_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt1_rx_reset_out};
    end
    `Addr_GT2_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt2_rx_reset_out};
    end
    `Addr_GT3_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt3_rx_reset_out};
    end
    `Addr_GT4_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt4_rx_reset_out};
    end
    `Addr_GT5_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt5_rx_reset_out};
    end
    `Addr_GT6_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt6_rx_reset_out};
    end
    `Addr_GT7_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt7_rx_reset_out};
    end
    `Addr_GT8_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt8_rx_reset_out};
    end
    `Addr_GT9_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt9_rx_reset_out};
    end
    `Addr_GT10_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt10_rx_reset_out};
    end
    `Addr_GT11_RX_RESET : begin
      VME_DOUT[15:0] <= {15'b0, gt11_rx_reset_out};
    end

    `Addr_Phase_glink0 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink0_mon[3:0], Phase_glink0[3:0]};
    end
    `Addr_Phase_glink1 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink1_mon[3:0], Phase_glink1[3:0]};
    end
    `Addr_Phase_glink2 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink2_mon[3:0], Phase_glink2[3:0]};
    end
    `Addr_Phase_glink3 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink3_mon[3:0], Phase_glink3[3:0]};
    end
    `Addr_Phase_glink4 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink4_mon[3:0], Phase_glink4[3:0]};
    end
    `Addr_Phase_glink5 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink5_mon[3:0], Phase_glink5[3:0]};
    end
    `Addr_Phase_glink6 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink6_mon[3:0], Phase_glink6[3:0]};
    end
    `Addr_Phase_glink7 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink7_mon[3:0], Phase_glink7[3:0]};
    end
    `Addr_Phase_glink8 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink8_mon[3:0], Phase_glink8[3:0]};
    end
    `Addr_Phase_glink9 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink9_mon[3:0], Phase_glink9[3:0]};
    end
    `Addr_Phase_glink10 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink10_mon[3:0], Phase_glink10[3:0]};
    end
    `Addr_Phase_glink11 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink11_mon[3:0], Phase_glink11[3:0]};
    end
    `Addr_Phase_glink12 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink12_mon[3:0], Phase_glink12[3:0]};
    end
    `Addr_Phase_glink13 : begin
        VME_DOUT [15:0] <= {8'b0, Phase_glink13_mon[3:0], Phase_glink13[3:0]};
    end

    `Addr_PATTERN_WIRE1 : begin
        VME_DOUT [15:0] <= detect_pattern_wire[15:0];
    end
    `Addr_PATTERN_WIRE2 : begin
        VME_DOUT [15:0] <= detect_pattern_wire[31:16];
    end
    `Addr_PATTERN_STRIP1 : begin
        VME_DOUT [15:0] <= detect_pattern_wire2[15:0];
    end
    `Addr_PATTERN_STRIP2 : begin
        VME_DOUT [15:0] <= detect_pattern_wire2[31:16];
    end

    `Addr_COUNTER_DOWN : begin
        VME_DOUT [15:0] <= counter[15:0];
    end
    `Addr_COUNTER_UP : begin
        VME_DOUT [15:0] <= counter[31:16];
    end
    `Addr_CHECKER_DOWN : begin
        VME_DOUT [15:0] <= checker[15:0];
    end
    `Addr_CHECKER_UP : begin
        VME_DOUT [15:0] <= checker[31:16];
    end

    `Addr_INPUT_COUNTER_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_counter[15:0];
    end
    `Addr_INPUT_COUNTER_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_counter[15:0];
    end

    `Addr_PATTERN_CHECKER_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_checker[15:0];
    end
    `Addr_PATTERN_CHECKER_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_checker[15:0];
    end
    
    `Addr_CHECKER_SELECTOR1 : begin
        VME_DOUT [15:0] <= {14'b0, pattern_selector1[1:0]};
    end
    `Addr_CHECKER_SELECTOR2 : begin
        VME_DOUT [15:0] <= {14'b0, pattern_selector2[1:0]};
    end
    `Addr_CHECK_PATTERN0 : begin
        VME_DOUT [15:0] <= {11'b0, check_pattern0[4:0]};
    end
    `Addr_CHECK_PATTERN1 : begin
        VME_DOUT [15:0] <= {11'b0, check_pattern1[4:0]};
    end
    `Addr_CHECK_PATTERN2 : begin
        VME_DOUT [15:0] <= {11'b0, check_pattern2[4:0]};
    end
    `Addr_CHECK_PATTERN3 : begin
        VME_DOUT [15:0] <= {11'b0, check_pattern3[4:0]};
    end

    `Addr_ERROR_COUNTER_PHASE0_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK12 : begin
        VME_DOUT [15:0] <= glink12_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE0_GLINK13 : begin
        VME_DOUT [15:0] <= glink13_error_counter_phase0[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK12 : begin
        VME_DOUT [15:0] <= glink12_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE1_GLINK13 : begin
        VME_DOUT [15:0] <= glink13_error_counter_phase1[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK12 : begin
        VME_DOUT [15:0] <= glink12_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE2_GLINK13 : begin
        VME_DOUT [15:0] <= glink13_error_counter_phase2[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK0 : begin
        VME_DOUT [15:0] <= glink0_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK1 : begin
        VME_DOUT [15:0] <= glink1_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK2 : begin
        VME_DOUT [15:0] <= glink2_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK3 : begin
        VME_DOUT [15:0] <= glink3_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK4 : begin
        VME_DOUT [15:0] <= glink4_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK5 : begin
        VME_DOUT [15:0] <= glink5_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK6 : begin
        VME_DOUT [15:0] <= glink6_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK7 : begin
        VME_DOUT [15:0] <= glink7_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK8 : begin
        VME_DOUT [15:0] <= glink8_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK9 : begin
        VME_DOUT [15:0] <= glink9_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK10 : begin
        VME_DOUT [15:0] <= glink10_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK11 : begin
        VME_DOUT [15:0] <= glink11_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK12 : begin
        VME_DOUT [15:0] <= glink12_error_counter_phase3[15:0];
    end
    `Addr_ERROR_COUNTER_PHASE3_GLINK13 : begin
        VME_DOUT [15:0] <= glink13_error_counter_phase3[15:0];
    end
    
    `Addr_GLINK_PATTERN0 : begin
        VME_DOUT [15:0] <= glink0_pattern[15:0];
    end
    `Addr_GLINK_PATTERN1 : begin
        VME_DOUT [15:0] <= {15'b0, glink0_pattern[16]};
    end
    `Addr_GLINK_PATTERN2 : begin
        VME_DOUT [15:0] <= glink1_pattern[15:0];
    end
    `Addr_GLINK_PATTERN3 : begin
        VME_DOUT [15:0] <= {15'b0, glink1_pattern[16]};
    end
    `Addr_GLINK_PATTERN4 : begin
        VME_DOUT [15:0] <= glink2_pattern[15:0];
    end
    `Addr_GLINK_PATTERN5 : begin
        VME_DOUT [15:0] <= {15'b0, glink2_pattern[16]};
    end
    `Addr_GLINK_PATTERN6 : begin
        VME_DOUT [15:0] <= glink3_pattern[15:0];
    end
    `Addr_GLINK_PATTERN7 : begin
        VME_DOUT [15:0] <= {15'b0, glink3_pattern[16]};
    end
    `Addr_GLINK_PATTERN8 : begin
        VME_DOUT [15:0] <= glink4_pattern[15:0];
    end
    `Addr_GLINK_PATTERN9 : begin
        VME_DOUT [15:0] <= {15'b0, glink4_pattern[16]};
    end
    `Addr_GLINK_PATTERN10 : begin
        VME_DOUT [15:0] <= glink5_pattern[15:0];
    end
    `Addr_GLINK_PATTERN11 : begin
        VME_DOUT [15:0] <= {15'b0, glink5_pattern[16]};
    end
    `Addr_GLINK_PATTERN12 : begin
        VME_DOUT [15:0] <= glink6_pattern[15:0];
    end
    `Addr_GLINK_PATTERN13 : begin
        VME_DOUT [15:0] <= {15'b0, glink6_pattern[16]};
    end
    `Addr_GLINK_PATTERN14 : begin
        VME_DOUT [15:0] <= glink7_pattern[15:0];
    end
    `Addr_GLINK_PATTERN15 : begin
        VME_DOUT [15:0] <= {15'b0, glink7_pattern[16]};
    end
    `Addr_GLINK_PATTERN16 : begin
        VME_DOUT [15:0] <= glink8_pattern[15:0];
    end
    `Addr_GLINK_PATTERN17 : begin
        VME_DOUT [15:0] <= {15'b0, glink8_pattern[16]};
    end
    `Addr_GLINK_PATTERN18 : begin
        VME_DOUT [15:0] <= glink9_pattern[15:0];
    end
    `Addr_GLINK_PATTERN19 : begin
        VME_DOUT [15:0] <= {15'b0, glink9_pattern[16]};
    end
    `Addr_GLINK_PATTERN20 : begin
        VME_DOUT [15:0] <= glink10_pattern[15:0];
    end
    `Addr_GLINK_PATTERN21 : begin
        VME_DOUT [15:0] <= {15'b0, glink10_pattern[16]};
    end
    `Addr_GLINK_PATTERN22 : begin
        VME_DOUT [15:0] <= glink11_pattern[15:0];
    end
    `Addr_GLINK_PATTERN23 : begin
        VME_DOUT [15:0] <= {15'b0, glink11_pattern[16]};
    end
    `Addr_GLINK_PATTERN24 : begin
        VME_DOUT [15:0] <= glink12_pattern[15:0];
    end
    `Addr_GLINK_PATTERN25 : begin
        VME_DOUT [15:0] <= {15'b0, glink12_pattern[16]};
    end
    `Addr_GLINK_PATTERN26 : begin
        VME_DOUT [15:0] <= glink13_pattern[15:0];
    end
    `Addr_GLINK_PATTERN27 : begin
        VME_DOUT [15:0] <= {15'b0, glink13_pattern[16]};
    end

    `Addr_SITCP_COUNT : begin
        VME_DOUT [15:0] <= SiTCP_Count;      
    end

    `Addr_MONITORING_FIFO_OUT_0 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_0[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_1 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_1[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_2 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_2[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_3 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_3[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_4 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_4[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_5 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_5[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_6 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_6[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_7 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_7[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_8 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_8[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_9 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_9[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_10 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_10[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_11 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_11[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_12 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_12[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_13 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_13[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_14 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_14[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_15 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_15[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_16 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_16[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_17 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_17[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_18 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_18[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_19 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_19[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_20 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_20[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_21 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_21[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_22 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_22[15:0];       
    end
    `Addr_MONITORING_FIFO_OUT_23 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_23[15:0];       
    end

    `Addr_DEBUG_MONITORING_FIFO_OUT_0 : begin
        VME_DOUT [15:0] <= monitoring_FIFO_out_0[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_1 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_1[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_2 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_2[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_3 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_3[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_4 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_4[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_5 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_5[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_6 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_6[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_7 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_7[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_8 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_8[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_9 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_9[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_10 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_10[15:0];       
    end
    `Addr_DEBUG_MONITORING_FIFO_OUT_11 : begin
        VME_DOUT [15:0] <= debug_monitoring_FIFO_out_11[15:0];       
    end

    `Addr_MONITORING_FIFO_wr_en : begin
        VME_DOUT [15:0] <= {15'b0, monitoring_FIFO_wr_en};      
    end
    `Addr_MONITORING_FIFO_rd_en : begin
      VME_DOUT [15:0]<= {15'b0, monitoring_FIFO_rd_en};
    end
    `Addr_LANE_SELECTOR : begin
        VME_DOUT [15:0] <= {9'h0, lane_selector_out[6:0]}; 
    end
    `Addr_MONITORING_wr_cnt : begin
      VME_DOUT [15:0]<= {3'b0, monitoring_wr_cnt[12:0]};
    end
    `Addr_SIGNAL_SELECTOR : begin
      VME_DOUT [15:0]<= {14'b0, signal_selector_out[1:0]};
    end
    `Addr_CONTROL_FLAG : begin
      VME_DOUT [15:0]<= {15'b0, control_flag};
    end    
    `Addr_L1A_MONITOR: begin
        VME_DOUT [15:0] <= L1A_counter; 
    end
    `Addr_BCR_MONITOR: begin
        VME_DOUT [15:0] <= BCR_counter; 
    end
    `Addr_ECR_MONITOR: begin
        VME_DOUT [15:0] <= ECR_counter; 
    end
    `Addr_TTC_RESET_MONITOR: begin
        VME_DOUT [15:0] <= TTC_RESET_counter; 
    end
    `Addr_TEST_PULSE_MONITOR: begin
        VME_DOUT [15:0] <= TEST_PULSE_counter; 
    end
        `Addr_EYE0_DATA0 : begin
        VME_DOUT[15:0] <= {eye0_data[15:0]};
    end
    `Addr_EYE0_DATA1 : begin
        VME_DOUT[15:0] <= {eye0_data[31:16]};
    end
    `Addr_EYE1_DATA0 : begin
        VME_DOUT[15:0] <= {eye1_data[15:0]};
    end
    `Addr_EYE1_DATA1 : begin
        VME_DOUT[15:0] <= {eye1_data[31:16]};
    end
    `Addr_EYE2_DATA0 : begin
        VME_DOUT[15:0] <= {eye2_data[15:0]};
    end
    `Addr_EYE2_DATA1 : begin
        VME_DOUT[15:0] <= {eye2_data[31:16]};
    end
    `Addr_EYE3_DATA0 : begin
        VME_DOUT[15:0] <= {eye3_data[15:0]};
    end
    `Addr_EYE3_DATA1 : begin
        VME_DOUT[15:0] <= {eye3_data[31:16]};
    end
    `Addr_EYE4_DATA0 : begin
        VME_DOUT[15:0] <= {eye4_data[15:0]};
    end
    `Addr_EYE4_DATA1 : begin
        VME_DOUT[15:0] <= {eye4_data[31:16]};
    end
    `Addr_EYE5_DATA0 : begin
        VME_DOUT[15:0] <= {eye5_data[15:0]};
    end
    `Addr_EYE5_DATA1 : begin
        VME_DOUT[15:0] <= {eye5_data[31:16]};
    end
    `Addr_EYE6_DATA0 : begin
        VME_DOUT[15:0] <= {eye6_data[15:0]};
    end
    `Addr_EYE6_DATA1 : begin
        VME_DOUT[15:0] <= {eye6_data[31:16]};
    end
    `Addr_EYE7_DATA0 : begin
        VME_DOUT[15:0] <= {eye7_data[15:0]};
    end
    `Addr_EYE7_DATA1 : begin
        VME_DOUT[15:0] <= {eye7_data[31:16]};
    end
    `Addr_EYE8_DATA0 : begin
        VME_DOUT[15:0] <= {eye8_data[15:0]};
    end
    `Addr_EYE8_DATA1 : begin
        VME_DOUT[15:0] <= {eye8_data[31:16]};
    end
    `Addr_EYE9_DATA0 : begin
        VME_DOUT[15:0] <= {eye9_data[15:0]};
    end
    `Addr_EYE9_DATA1 : begin
        VME_DOUT[15:0] <= {eye9_data[31:16]};
    end
    `Addr_EYE10_DATA0 : begin
        VME_DOUT[15:0] <= {eye10_data[15:0]};
    end
    `Addr_EYE10_DATA1 : begin
        VME_DOUT[15:0] <= {eye10_data[31:16]};
    end
    `Addr_EYE11_DATA0 : begin
        VME_DOUT[15:0] <= {eye11_data[15:0]};
    end
    `Addr_EYE11_DATA1 : begin
        VME_DOUT[15:0] <= {eye11_data[31:16]};
    end
    `Addr_EYE_RD_EN : begin
        VME_DOUT[15:0] <= {15'b0, eye_rd_en};
    end
    `Addr_EYE_GO : begin
        VME_DOUT[15:0] <= {15'b0, eye_go};
    end
    `Addr_DRP_ADDR : begin
        VME_DOUT[15:0]    <= {7'b0, drpaddrReg};
    end
    `Addr_DRP_DI : begin
        VME_DOUT[15:0]    <= {drpdiReg};
    end
    `Addr_DRP_ENCMD : begin
        VME_DOUT[15:0]    <= {15'b0, drpEnCmd};
    end
    `Addr_DRP_WECMD : begin
        VME_DOUT[15:0]    <= {15'b0, drpWeCmd};
    end
    `Addr_EYE0_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye0_data_count};
    end
    `Addr_EYE1_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye1_data_count};
    end
    `Addr_EYE2_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye2_data_count};
    end
    `Addr_EYE3_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye3_data_count};
    end
    `Addr_EYE4_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye4_data_count};
    end
    `Addr_EYE5_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye5_data_count};
    end
    `Addr_EYE6_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye6_data_count};
    end
    `Addr_EYE7_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye7_data_count};
    end
    `Addr_EYE8_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye8_data_count};
    end
    `Addr_EYE9_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye9_data_count};
    end
    `Addr_EYE10_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye10_data_count};
    end
    `Addr_EYE11_DATA_COUNT : begin
        VME_DOUT[15:0] <= {1'b0, eye11_data_count};
    end
    `Addr_EYE0_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye0_state};
    end
    `Addr_EYE1_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye1_state};
    end
    `Addr_EYE2_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye2_state};
    end
    `Addr_EYE3_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye3_state};
    end
    `Addr_EYE4_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye4_state};
    end
    `Addr_EYE5_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye5_state};
    end
    `Addr_EYE6_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye6_state};
    end
    `Addr_EYE7_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye7_state};
    end
    `Addr_EYE8_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye8_state};
    end
    `Addr_EYE9_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye9_state};
    end
    `Addr_EYE10_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye10_state};
    end
    `Addr_EYE11_STATE : begin
        VME_DOUT[15:0] <= {11'b0, eye11_state};
    end
    `Addr_H_STEPSIZE : begin
        VME_DOUT[15:0] <= {4'b0, hstepSize};
    end
    `Addr_V_STEPSIZE : begin
        VME_DOUT[15:0] <= {5'b0, vstepSize};
    end
    `Addr_H_STEPMAX : begin
        VME_DOUT[15:0] <= {7'b0, hstep_max};
    end
    `Addr_V_STEPMAX : begin
        VME_DOUT[15:0] <= {7'b0, vstep_max};
    end
    `Addr_H_INIT : begin
        VME_DOUT[15:0] <= {4'b0, horz_init};
    end
    `Addr_V_INIT : begin
        VME_DOUT[15:0] <= {5'b0, vert_init};
    end
    `Addr_PRBS_RESET : begin
        VME_DOUT[15:0] <= {15'b0, prbs_reset};
    end
    `Addr_PRBS_SEL : begin
        VME_DOUT[15:0] <= {13'b0, prbssel};
    end
    `Addr_GT0_DRPDO : begin
        VME_DOUT[15:0] <= {gt0_drpdo};
    end
    `Addr_GT1_DRPDO : begin
        VME_DOUT[15:0] <= {gt1_drpdo};
    end
    `Addr_GT2_DRPDO : begin
        VME_DOUT[15:0] <= {gt2_drpdo};
    end
    `Addr_GT3_DRPDO : begin
        VME_DOUT[15:0] <= {gt3_drpdo};
    end
    `Addr_GT4_DRPDO : begin
        VME_DOUT[15:0] <= {gt4_drpdo};
    end
    `Addr_GT5_DRPDO : begin
        VME_DOUT[15:0] <= {gt5_drpdo};
    end
    `Addr_GT6_DRPDO : begin
        VME_DOUT[15:0] <= {gt6_drpdo};
    end
    `Addr_GT7_DRPDO : begin
        VME_DOUT[15:0] <= {gt7_drpdo};
    end
    `Addr_GT8_DRPDO : begin
        VME_DOUT[15:0] <= {gt8_drpdo};
    end
    `Addr_GT9_DRPDO : begin
        VME_DOUT[15:0] <= {gt9_drpdo};
    end
    `Addr_GT10_DRPDO : begin
        VME_DOUT[15:0] <= {gt10_drpdo};
    end
    `Addr_GT11_DRPDO : begin
        VME_DOUT[15:0] <= {gt11_drpdo};
    end
    `Addr_LPMEN : begin
        VME_DOUT[15:0] <= {15'b0, lpmen};
    end
    default : begin
        VME_DOUT [15:0] <= 16'h1992;
    end
    endcase
end
    
endmodule
