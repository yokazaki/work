`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/02/06 17:09:50
// Design Name: 
// Module Name: GLink_Input_Monitor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_Input_Monitor(
    input CLK_IN,
    input reset_in,
    input counter_reset_in,
    input [4:0] pattern1_in,
    input [4:0] pattern2_in,
    input [16:0] glink0_in,
    input [16:0] glink1_in,
    input [16:0] glink2_in,
    input [16:0] glink3_in,
    input [16:0] glink4_in,
    input [16:0] glink5_in,
    input [16:0] glink6_in,
    input [16:0] glink7_in,
    input [16:0] glink8_in,
    input [16:0] glink9_in,
    input [16:0] glink10_in,
    input [16:0] glink11_in,
    output [31:0] glink0_counter_out,
    output [31:0] glink1_counter_out,
    output [31:0] glink2_counter_out,
    output [31:0] glink3_counter_out,
    output [31:0] glink4_counter_out,
    output [31:0] glink5_counter_out,
    output [31:0] glink6_counter_out,
    output [31:0] glink7_counter_out,
    output [31:0] glink8_counter_out,
    output [31:0] glink9_counter_out,
    output [31:0] glink10_counter_out,
    output [31:0] glink11_counter_out,
    output [31:0] glink0_pattern_checker_out,
    output [31:0] glink1_pattern_checker_out,
    output [31:0] glink2_pattern_checker_out,
    output [31:0] glink3_pattern_checker_out,
    output [31:0] glink4_pattern_checker_out,
    output [31:0] glink5_pattern_checker_out,
    output [31:0] glink6_pattern_checker_out,
    output [31:0] glink7_pattern_checker_out,
    output [31:0] glink8_pattern_checker_out,
    output [31:0] glink9_pattern_checker_out,
    output [31:0] glink10_pattern_checker_out,
    output [31:0] glink11_pattern_checker_out,
    output debug_flag,
    output stop_flag
    
    );
    
    wire RST_in = reset_in||counter_reset_in;
    wire [101:0] InputBuffer1;
    wire [101:0] InputBuffer2;
    wire debug_flag0, debug_flag1, debug_flag2, debug_flag3, debug_flag4, debug_flag5, debug_flag6, debug_flag7, debug_flag8, debug_flag9, debug_flag10, debug_flag11; 
    wire stop_flag0, stop_flag1, stop_flag2, stop_flag3, stop_flag4, stop_flag5, stop_flag6, stop_flag7, stop_flag8, stop_flag9, stop_flag10, stop_flag11; 
    wire [9:0] hpttrg0, hpttrg1, hpttrg2, hpttrg3, hpttrg4, hpttrg5, hpttrg6, hpttrg7, hpttrg8, hpttrg9, hpttrg10, hpttrg11;
    assign InputBuffer1 = {glink5_in[16:0], glink4_in[16:0], glink3_in[16:0], glink2_in[16:0], glink1_in[16:0], glink0_in[16:0]};
    assign hpttrg0 = {5'b0 , InputBuffer1[4:0]};                                 //candidates of wire0
    assign hpttrg1 = {InputBuffer1[21:17] , InputBuffer1[11:7]};                 //candidates of wire1
    assign hpttrg2 = {InputBuffer1[41:37] , InputBuffer1[31:27]};                //candidates of wire2
    assign hpttrg3 = {InputBuffer1[61:57] , InputBuffer1[51:47]};                //candidates of wire3
    assign hpttrg4 = {1'b0 , InputBuffer1[80:77] , 1'b0 , InputBuffer1[71:68]};           //candidates of strip0
    assign hpttrg5 = {1'b0 , InputBuffer1[97:94] , 1'b0 , InputBuffer1[89:86]};           //candidates of strip1
    assign InputBuffer2 = {glink11_in[16:0], glink10_in[16:0], glink9_in[16:0], glink8_in[16:0], glink7_in[16:0], glink6_in[16:0]};
    assign hpttrg6 = {5'b0 , InputBuffer2[4:0]};
    assign hpttrg7 = {InputBuffer2[21:17] , InputBuffer2[11:7]};
    assign hpttrg8 = {InputBuffer2[41:37] , InputBuffer2[31:27]};
    assign hpttrg9 = {InputBuffer2[61:57] , InputBuffer2[51:47]};
    assign hpttrg10 = {1'b0 , InputBuffer2[80:77] , 1'b0 , InputBuffer2[71:68]};
    assign hpttrg11 = {1'b0 , InputBuffer2[97:94] , 1'b0 , InputBuffer2[89:86]};
    
    assign stop_flag = stop_flag0;
    assign debug_flag = |{debug_flag11, debug_flag10, debug_flag9, debug_flag8, debug_flag7, debug_flag6, debug_flag5, debug_flag4, debug_flag3, debug_flag2, debug_flag1, debug_flag0}; 

Monitor InputMonitor_glink0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg0), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink0_counter_out), .pattern_checker_out(glink0_pattern_checker_out), .debug_flag(debug_flag0), .stop_flag(stop_flag0));
Monitor InputMonitor_glink1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg1), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink1_counter_out), .pattern_checker_out(glink1_pattern_checker_out), .debug_flag(debug_flag1), .stop_flag(stop_flag1));
Monitor InputMonitor_glink2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg2), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink2_counter_out), .pattern_checker_out(glink2_pattern_checker_out), .debug_flag(debug_flag2), .stop_flag(stop_flag2));
Monitor InputMonitor_glink3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg3), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink3_counter_out), .pattern_checker_out(glink3_pattern_checker_out), .debug_flag(debug_flag3), .stop_flag(stop_flag3));
Monitor InputMonitor_glink4(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg4), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink4_counter_out), .pattern_checker_out(glink4_pattern_checker_out), .debug_flag(debug_flag4), .stop_flag(stop_flag4));
Monitor InputMonitor_glink5(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg5), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink5_counter_out), .pattern_checker_out(glink5_pattern_checker_out), .debug_flag(debug_flag5), .stop_flag(stop_flag5));
Monitor InputMonitor_glink6(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg6), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink6_counter_out), .pattern_checker_out(glink6_pattern_checker_out), .debug_flag(debug_flag6), .stop_flag(stop_flag6));
Monitor InputMonitor_glink7(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg7), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink7_counter_out), .pattern_checker_out(glink7_pattern_checker_out), .debug_flag(debug_flag7), .stop_flag(stop_flag7));
Monitor InputMonitor_glink8(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg8), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink8_counter_out), .pattern_checker_out(glink8_pattern_checker_out), .debug_flag(debug_flag8), .stop_flag(stop_flag8));
Monitor InputMonitor_glink9(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg9), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink9_counter_out), .pattern_checker_out(glink9_pattern_checker_out), .debug_flag(debug_flag9), .stop_flag(stop_flag9));
Monitor InputMonitor_glink10(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg10), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink10_counter_out), .pattern_checker_out(glink10_pattern_checker_out), .debug_flag(debug_flag10), .stop_flag(stop_flag10));
Monitor InputMonitor_glink11(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(hpttrg11), .pattern1_in(pattern1_in), .pattern2_in(pattern2_in), .count_out(glink11_counter_out), .pattern_checker_out(glink11_pattern_checker_out), .debug_flag(debug_flag11), .stop_flag(stop_flag11));

endmodule
