`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/05/31 21:53:34
// Design Name: 
// Module Name: eyeScan
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
// Title      : Eye Scan FSM                                            
// Project    : Eye Scan RTL                                                     
//-----------------------------------------------------------------------------
// File       : eyeScan.v                                          
//-----------------------------------------------------------------------------
// Description: This file contains the 
// 10GBASE-R clocking and reset logic which can be shared between multiple cores                
//-----------------------------------------------------------------------------
// (c) Copyright 2009 - 2013 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and 
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// Version .9    3/31/2015
 module eyeScan
    (
     (* keep = "true" *)  input             CLK,
     (* keep = "true" *)  input             TXFSMRESETDONE,
     (* keep = "true" *)  input             RXFSMRESETDONE,
     (* keep = "true" *)  input wire       SOFRST,
    (* keep = "true" *)   input [15:0]      DRPDO,
    (* keep = "true" *)   input             DRPRDY,
    (* keep = "true" *)   output reg [8:0]  DRPADDR,
    (* keep = "true" *)   output reg        DRPWE,
    (* keep = "true" *)   output reg        DRPEN,
    (* keep = "true" *)   output reg [15:0] DRPDI,
    output wire [31:0] data_out,
    output wire [14:0] eye_data_count,
    output wire [4:0] eye_state,
    input wire rd_clk,
    input wire rd_en,
    input wire go,
    input wire [8:0] drpaddrReg,
    input wire [15:0] drpdiReg,
    input wire drpEnCmd,
    input wire drpWeCmd,
    input wire [11:0] hstepSize,
    input wire [10:0] vstepSize,
    input wire [8:0] hstep_max,
    input wire [8:0] vstep_max,
    input wire [11:0] horz_init,
    input wire [10:0] vert_init
    );
  (* keep = "true" *)  reg [4:0] state;
    reg setRun = 0;
    reg [3:0]   status;
    reg [15:0]  errors;
    reg [8:0]   hsteps;
    reg [8:0]   vsteps;
    //reg [11:0]  hstepSize = 4;
    reg [5:0]   cycles;
    //reg [11:0]  vstepSize = 4;
    reg [5:0]   cnt;
    reg [11:0]  horz;
    reg [10:0]  vert;
    reg [15:0]  samples;
    //reg         ilaClk;
    reg [8:0]   drpAddr;
    reg         drpWe;
    reg         drpEn;
    reg [15:0]  drpDi;
    wire        rstRtl;
    reg         drpDone;
    //reg [15:0] h_max;
    wire full;

 //(* keep = "true" *)   wire [8:0]  drpaddrReg;  // output wire [8 : 0] probe_out0
 //(* keep = "true" *)   wire [15:0] drpdiReg;  // output wire [14 : 0] probe_out1
 //(* keep = "true" *)   wire        drpEnCmd;  // output wire [0 : 0] probe_out2
 //(* keep = "true" *)   wire        drpWeCmd;  // output wire [0 : 0] probe_out3
 //(* keep = "true" *)   wire        go;
    
    parameter waitGoCmd=0, run = 1, waitStart = 2, waitComplete = 3,getErrors=4, storeErrors=5,getSamples=6,storeSamples=7,rstCmd=8,waitRstCmd=9,updateHorizontal=10,updateVertical =11, waitRun = 12, waitHorizontal=13, waitVertical=14,testStart=15,testComplete = 16,storeData = 17;

 
    /*always @ (posedge CLK) begin
       if (DRPRDY && ((drpAddr == 9'h150) || ( drpAddr == 9'h14F) || (drpAddr == 9'h03C) || (drpAddr == 9'h03B)))
          ilaClk <= 1'b1;
       else 
          ilaClk <= 1'b0;
       if (state <= waitGoCmd)
          ilaClk <= !ilaClk;      
       if (rstRtl)
          ilaClk <= 1'b0;
    end*/
       /*
    ila_0 ilaEye (
      .clk(ilaClk),//CLK), //ilaClk),                  // input wire clk
      .trig_in(go),
      .trig_in_ack(),  // output wire trig_in_ack
      .probe0(state[4:0]),
      .probe1(drpAddr),
      .probe2(samples),            // input wire [30 : 0] probe0
      .probe3(errors),
      .probe4(horz),
      .probe5(vert)
    );
    */
    always @ (posedge CLK) if (rstRtl)
	 state <= waitGoCmd;
    else
    case (state)
       waitGoCmd: begin
           drpWe <=0;
           drpEn <= 0;
           drpAddr <= 0;
           drpDi <= 0;
           hsteps <= 0;
	        vsteps <= 0;
           //horz <= 11'b00000111111;
           horz <= horz_init;
           vert <= vert_init;
           if (go)
               state <= updateHorizontal;
           else 
               state<=waitGoCmd;
       end
       updateHorizontal: begin
	        cnt <= 0;
           drpEn <=0;
           drpWe <= 1;
           drpAddr <= 9'h03C;
	   //if (hsteps < 32) begin
	   if (hsteps < hstep_max) begin
	            horz <= horz - hstepSize;
               drpDi <= horz;
               hsteps <= hsteps +1;
            end
            else begin
	            drpDi <= 12'b000000111111;
               horz <= horz_init;
               hsteps <= 0;
	    end
            state <= waitHorizontal;
       end
       waitHorizontal:begin
	        cnt <= cnt+1;
           drpEn <=0;
           drpWe <= 0;
	   if (DRPRDY || cnt[5]) 
               if (hsteps == 9'd0)
                   state <= updateVertical;
	            else
		             state <= run;
            else
               state <= waitHorizontal;
       end
       updateVertical: begin
	        cnt <= 0;
           //if (vsteps < 64 ) begin
           if (vsteps < vstep_max ) begin
                vsteps <= vsteps+1;
                drpWe <= 1;
                drpEn <=0;
                drpAddr <= 9'h03B;
                vert <= vert - vstepSize;
                drpDi <= (vert - vstepSize) |16'h2000;
                state <= waitVertical;
           end
           else 
                state <= waitGoCmd;
       end
       waitVertical:begin
           drpWe <= 0;
           drpEn <=0;
	        cnt <= cnt+1;
           if (DRPRDY || cnt[5])
                state <= run;
           else
                state <= waitVertical;
       end
       run: begin
	        cnt <= 0;
           state <= waitRun;
           drpWe <= 1;
           drpEn <=0;
           drpAddr <= 9'h03D;  //Set address to ES_CONTROL
           drpDi <= 16'hE301;  // Set run bit and eye Scan enable bit
       end
       waitRun:begin
	        cnt <= cnt+1;
           drpWe <= 0;
           drpEn <=0;
           drpAddr <= 9'b0;  //reset address to ES_CONTROL
//           drpDi <= 16'b0;  // reset data port
           if (DRPRDY  || cnt[5])
              state <= testStart;
           else
              state <= waitRun;
       end
       testStart: begin
           state <= waitStart;
           drpWe <= 0;
           drpEn <= 1;         //Read Status
           cnt<=0;
           drpAddr <= 9'h151;  // ES_CONTROL_STATUS
       end
       waitStart: begin
           drpEn <= 0;
           drpWe <=0;
           cnt<= cnt+1;
           if (DRPRDY || cnt[5]) begin
              if (DRPDO == 6) //running
                state <=testComplete;
              else
                state <= testStart;
           end
           else
                state <=waitStart;  
       end
       testComplete: begin
           state <= waitComplete;
	        cnt <= 0;
           drpEn <= 1;         //Read Status
           drpWe <= 0;
           drpAddr <= 9'h151;  // ES_CONTROL_STATUS
       end
       waitComplete: begin
           drpEn <=0;
	        cnt <= cnt +1;
           if (DRPRDY || cnt[5]) 
               if ( DRPDO[3:1] == 2 || DRPDO[3:1] == 0)
                  state <= rstCmd;
               else 
                  state <= testComplete;
           else 
              state <=waitComplete;
       end
       rstCmd: begin
    	   drpAddr <= 9'h03D;
	      drpWe <= 1'b1;
         drpEn <= 0;
	      drpDi <= 16'hE300;
         state <= waitRstCmd;
	      cnt <= 0;
       end
       waitRstCmd: begin
	        drpAddr <= 9'b0;
           drpWe <= 1'b0;
           drpEn <= 0;
//           drpDi <= 16'b0;
	        cnt <= cnt+1;
           if (DRPRDY || cnt[5])	   
              state <= getSamples;
           else 
              state <=waitRstCmd;	 
       end
       getSamples: begin
           drpAddr <= 9'h150;
           drpEn <= 1;
           drpWe <= 0;
           state <= storeSamples;
	        cnt <= 0;
       end
       storeSamples: begin
           drpEn <= 0;
	        cnt <= cnt + 1;
           if (DRPRDY || cnt[5]) begin
	           state <= getErrors;
              samples <= DRPDO;
           end
           else 
              state <= storeSamples;
       end
       getErrors: begin
           drpAddr <= 9'h14F;
           drpEn <= 1;
           state <= storeErrors;
	        cnt <= 0;
       end
       storeErrors: begin
           drpEn <= 0;
	        cnt <= cnt + 1;
           if (DRPRDY || cnt[5]) begin
	          state <=updateHorizontal ;
             errors <= DRPDO;
           end
           else 
              state <= storeErrors;
       end
       default:
           state <= waitGoCmd;  // Wait for start command
    endcase
/*
    vio_0 drpCntrlIns (
         .clk(CLK),                // input wire clk
         .probe_in0(DRPDO),    // input wire [15 : 0] probe_in0
         .probe_in1(TXFSMRESETDONE),
         .probe_in2(RXFSMRESETDONE),
         .probe_in3(DRPRDY),
         .probe_out0(drpaddrReg[8:0]),  // output wire [8 : 0] probe_out0
         .probe_out1(drpdiReg[15:0]),  // output wire [14 : 0] probe_out1
         .probe_out2(drpWeCmd),  // output wire [0 : 0] probe_out3
         .probe_out3(drpEnCmd),
         .probe_out4(SOFRST),  // output wire [0 : 0] probe_out4
         .probe_out5(go),
         .probe_out6(rstRtl)
    );
*/

      


    always @(posedge CLK ) begin                   // Routine to run the drp using VIO or eyeScan inputs
          if (drpDone) begin
            DRPEN <= 1'b0;
            DRPADDR <= 9'b0;
            DRPDI <= 16'b0;
            DRPWE <= 1'b0;
          end
          if (!drpWeCmd && !drpEnCmd && !drpWe && !drpEn   ) begin
            drpDone <=1'b0;
            DRPEN <= 1'b0;
            DRPADDR <= 9'b0;
            DRPDI <= 16'b0;
            DRPWE <= 1'b0;
          end 
          if ((drpWeCmd || drpWe) && !drpDone) begin
            DRPWE <= 1'b1;
            DRPADDR <= state == waitGoCmd ? drpaddrReg : drpAddr;
            DRPDI <= state == waitGoCmd ? drpdiReg : drpDi;
            DRPEN <= 1'b1;
            drpDone <= 1'b1;
          end
          if ((drpEnCmd || drpEn) && !drpDone) begin
            DRPEN <=1'b1;
            DRPWE <=1'b0;
            DRPADDR <= state == waitGoCmd ? drpaddrReg : drpAddr;
            DRPDI <= state == waitGoCmd ? drpdiReg : drpDi;
            drpDone <= 1'b1; 
          end 
    end

wire write_ready = (DRPRDY && (( drpAddr == 9'h14F) || (drpAddr == 9'h03B)));
wire wr_en = write_ready && !full;

     wire FIFO_read_enb;
     reg [2:0] shift_reg;
     reg read_enb_reg;
     assign FIFO_read_enb = (!shift_reg[2] && shift_reg[1]);
     always @(posedge rd_clk) begin
         shift_reg[2:0] <= {shift_reg[1:0], rd_en};
     end

wire empty;

wire eye_rd_en = FIFO_read_enb && !empty;

assign eye_state = state;


Eye_FIFO Eye_FIFO(
    .wr_clk(CLK),
    .rst(SOFRST),
    .wr_en(wr_en),
    .full(full),
    .wr_data_count  (eye_data_count),
    .din({DRPDO[15:0], samples[15:0]}),
    .rd_clk(rd_clk),
    .rd_en(eye_rd_en),
    .empty  (empty),
    .dout(data_out[31:0])
);


endmodule




