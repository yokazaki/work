`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/02/06 17:09:50
// Design Name: 
// Module Name: GLink_Error_Counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_Error_Counter(
    input CLK_IN,
    input reset_in,
    input counter_reset_in,
    input [67:0] glink0_in,
    input [67:0] glink1_in,
    input [67:0] glink2_in,
    input [67:0] glink3_in,
    input [67:0] glink4_in,
    input [67:0] glink5_in,
    input [67:0] glink6_in,
    input [67:0] glink7_in,
    input [67:0] glink8_in,
    input [67:0] glink9_in,
    input [67:0] glink10_in,
    input [67:0] glink11_in,
    input [67:0] glink12_in,
    input [67:0] glink13_in,
    input [16:0] glink0_pattern,
    input [16:0] glink1_pattern,
    input [16:0] glink2_pattern,
    input [16:0] glink3_pattern,
    input [16:0] glink4_pattern,
    input [16:0] glink5_pattern,
    input [16:0] glink6_pattern,
    input [16:0] glink7_pattern,
    input [16:0] glink8_pattern,
    input [16:0] glink9_pattern,
    input [16:0] glink10_pattern,
    input [16:0] glink11_pattern,
    input [16:0] glink12_pattern,
    input [16:0] glink13_pattern,
    output [31:0] glink0_error_counter_out_phase0,
    output [31:0] glink1_error_counter_out_phase0,
    output [31:0] glink2_error_counter_out_phase0,
    output [31:0] glink3_error_counter_out_phase0,
    output [31:0] glink4_error_counter_out_phase0,
    output [31:0] glink5_error_counter_out_phase0,
    output [31:0] glink6_error_counter_out_phase0,
    output [31:0] glink7_error_counter_out_phase0,
    output [31:0] glink8_error_counter_out_phase0,
    output [31:0] glink9_error_counter_out_phase0,
    output [31:0] glink10_error_counter_out_phase0,
    output [31:0] glink11_error_counter_out_phase0,
    output [31:0] glink12_error_counter_out_phase0,
    output [31:0] glink13_error_counter_out_phase0,
    output [31:0] glink0_error_counter_out_phase1,
    output [31:0] glink1_error_counter_out_phase1,
    output [31:0] glink2_error_counter_out_phase1,
    output [31:0] glink3_error_counter_out_phase1,
    output [31:0] glink4_error_counter_out_phase1,
    output [31:0] glink5_error_counter_out_phase1,
    output [31:0] glink6_error_counter_out_phase1,
    output [31:0] glink7_error_counter_out_phase1,
    output [31:0] glink8_error_counter_out_phase1,
    output [31:0] glink9_error_counter_out_phase1,
    output [31:0] glink10_error_counter_out_phase1,
    output [31:0] glink11_error_counter_out_phase1,
    output [31:0] glink12_error_counter_out_phase1,
    output [31:0] glink13_error_counter_out_phase1,
    output [31:0] glink0_error_counter_out_phase2,
    output [31:0] glink1_error_counter_out_phase2,
    output [31:0] glink2_error_counter_out_phase2,
    output [31:0] glink3_error_counter_out_phase2,
    output [31:0] glink4_error_counter_out_phase2,
    output [31:0] glink5_error_counter_out_phase2,
    output [31:0] glink6_error_counter_out_phase2,
    output [31:0] glink7_error_counter_out_phase2,
    output [31:0] glink8_error_counter_out_phase2,
    output [31:0] glink9_error_counter_out_phase2,
    output [31:0] glink10_error_counter_out_phase2,
    output [31:0] glink11_error_counter_out_phase2,
    output [31:0] glink12_error_counter_out_phase2,
    output [31:0] glink13_error_counter_out_phase2,
    output [31:0] glink0_error_counter_out_phase3,
    output [31:0] glink1_error_counter_out_phase3,
    output [31:0] glink2_error_counter_out_phase3,
    output [31:0] glink3_error_counter_out_phase3,
    output [31:0] glink4_error_counter_out_phase3,
    output [31:0] glink5_error_counter_out_phase3,
    output [31:0] glink6_error_counter_out_phase3,
    output [31:0] glink7_error_counter_out_phase3,
    output [31:0] glink8_error_counter_out_phase3,
    output [31:0] glink9_error_counter_out_phase3,
    output [31:0] glink10_error_counter_out_phase3,
    output [31:0] glink11_error_counter_out_phase3,
    output [31:0] glink12_error_counter_out_phase3,
    output [31:0] glink13_error_counter_out_phase3
    
    );
    
    wire RST_in = reset_in||counter_reset_in;
    
Error_Counter Error_Counter_glink0_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink0_in[16:0]), .pattern_in(glink0_pattern), .count_out(glink0_error_counter_out_phase0));
Error_Counter Error_Counter_glink1_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink1_in[16:0]), .pattern_in(glink1_pattern), .count_out(glink1_error_counter_out_phase0));
Error_Counter Error_Counter_glink2_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink2_in[16:0]), .pattern_in(glink2_pattern), .count_out(glink2_error_counter_out_phase0));
Error_Counter Error_Counter_glink3_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink3_in[16:0]), .pattern_in(glink3_pattern), .count_out(glink3_error_counter_out_phase0));
Error_Counter Error_Counter_glink4_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink4_in[16:0]), .pattern_in(glink4_pattern), .count_out(glink4_error_counter_out_phase0));
Error_Counter Error_Counter_glink5_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink5_in[16:0]), .pattern_in(glink5_pattern), .count_out(glink5_error_counter_out_phase0));
Error_Counter Error_Counter_glink6_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink6_in[16:0]), .pattern_in(glink6_pattern), .count_out(glink6_error_counter_out_phase0));
Error_Counter Error_Counter_glink7_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink7_in[16:0]), .pattern_in(glink7_pattern), .count_out(glink7_error_counter_out_phase0));
Error_Counter Error_Counter_glink8_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink8_in[16:0]), .pattern_in(glink8_pattern), .count_out(glink8_error_counter_out_phase0));
Error_Counter Error_Counter_glink9_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink9_in[16:0]), .pattern_in(glink9_pattern), .count_out(glink9_error_counter_out_phase0));
Error_Counter Error_Counter_glink10_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink10_in[16:0]), .pattern_in(glink10_pattern), .count_out(glink10_error_counter_out_phase0));
Error_Counter Error_Counter_glink11_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink11_in[16:0]), .pattern_in(glink11_pattern), .count_out(glink11_error_counter_out_phase0));
Error_Counter Error_Counter_glink12_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink12_in[16:0]), .pattern_in(glink12_pattern), .count_out(glink12_error_counter_out_phase0));
Error_Counter Error_Counter_glink13_phase0(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink13_in[16:0]), .pattern_in(glink13_pattern), .count_out(glink13_error_counter_out_phase0));

Error_Counter Error_Counter_glink0_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink0_in[33:17]), .pattern_in(glink0_pattern), .count_out(glink0_error_counter_out_phase1));
Error_Counter Error_Counter_glink1_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink1_in[33:17]), .pattern_in(glink1_pattern), .count_out(glink1_error_counter_out_phase1));
Error_Counter Error_Counter_glink2_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink2_in[33:17]), .pattern_in(glink2_pattern), .count_out(glink2_error_counter_out_phase1));
Error_Counter Error_Counter_glink3_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink3_in[33:17]), .pattern_in(glink3_pattern), .count_out(glink3_error_counter_out_phase1));
Error_Counter Error_Counter_glink4_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink4_in[33:17]), .pattern_in(glink4_pattern), .count_out(glink4_error_counter_out_phase1));
Error_Counter Error_Counter_glink5_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink5_in[33:17]), .pattern_in(glink5_pattern), .count_out(glink5_error_counter_out_phase1));
Error_Counter Error_Counter_glink6_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink6_in[33:17]), .pattern_in(glink6_pattern), .count_out(glink6_error_counter_out_phase1));
Error_Counter Error_Counter_glink7_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink7_in[33:17]), .pattern_in(glink7_pattern), .count_out(glink7_error_counter_out_phase1));
Error_Counter Error_Counter_glink8_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink8_in[33:17]), .pattern_in(glink8_pattern), .count_out(glink8_error_counter_out_phase1));
Error_Counter Error_Counter_glink9_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink9_in[33:17]), .pattern_in(glink9_pattern), .count_out(glink9_error_counter_out_phase1));
Error_Counter Error_Counter_glink10_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink10_in[33:17]), .pattern_in(glink10_pattern), .count_out(glink10_error_counter_out_phase1));
Error_Counter Error_Counter_glink11_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink11_in[33:17]), .pattern_in(glink11_pattern), .count_out(glink11_error_counter_out_phase1));
Error_Counter Error_Counter_glink12_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink12_in[33:17]), .pattern_in(glink12_pattern), .count_out(glink12_error_counter_out_phase1));
Error_Counter Error_Counter_glink13_phase1(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink13_in[33:17]), .pattern_in(glink13_pattern), .count_out(glink13_error_counter_out_phase1));

Error_Counter Error_Counter_glink0_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink0_in[50:34]), .pattern_in(glink0_pattern), .count_out(glink0_error_counter_out_phase2));
Error_Counter Error_Counter_glink1_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink1_in[50:34]), .pattern_in(glink1_pattern), .count_out(glink1_error_counter_out_phase2));
Error_Counter Error_Counter_glink2_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink2_in[50:34]), .pattern_in(glink2_pattern), .count_out(glink2_error_counter_out_phase2));
Error_Counter Error_Counter_glink3_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink3_in[50:34]), .pattern_in(glink3_pattern), .count_out(glink3_error_counter_out_phase2));
Error_Counter Error_Counter_glink4_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink4_in[50:34]), .pattern_in(glink4_pattern), .count_out(glink4_error_counter_out_phase2));
Error_Counter Error_Counter_glink5_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink5_in[50:34]), .pattern_in(glink5_pattern), .count_out(glink5_error_counter_out_phase2));
Error_Counter Error_Counter_glink6_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink6_in[50:34]), .pattern_in(glink6_pattern), .count_out(glink6_error_counter_out_phase2));
Error_Counter Error_Counter_glink7_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink7_in[50:34]), .pattern_in(glink7_pattern), .count_out(glink7_error_counter_out_phase2));
Error_Counter Error_Counter_glink8_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink8_in[50:34]), .pattern_in(glink8_pattern), .count_out(glink8_error_counter_out_phase2));
Error_Counter Error_Counter_glink9_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink9_in[50:34]), .pattern_in(glink9_pattern), .count_out(glink9_error_counter_out_phase2));
Error_Counter Error_Counter_glink10_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink10_in[50:34]), .pattern_in(glink10_pattern), .count_out(glink10_error_counter_out_phase2));
Error_Counter Error_Counter_glink11_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink11_in[50:34]), .pattern_in(glink11_pattern), .count_out(glink11_error_counter_out_phase2));
Error_Counter Error_Counter_glink12_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink12_in[50:34]), .pattern_in(glink12_pattern), .count_out(glink12_error_counter_out_phase2));
Error_Counter Error_Counter_glink13_phase2(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink13_in[50:34]), .pattern_in(glink13_pattern), .count_out(glink13_error_counter_out_phase2));

Error_Counter Error_Counter_glink0_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink0_in[67:51]), .pattern_in(glink0_pattern), .count_out(glink0_error_counter_out_phase3));
Error_Counter Error_Counter_glink1_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink1_in[67:51]), .pattern_in(glink1_pattern), .count_out(glink1_error_counter_out_phase3));
Error_Counter Error_Counter_glink2_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink2_in[67:51]), .pattern_in(glink2_pattern), .count_out(glink2_error_counter_out_phase3));
Error_Counter Error_Counter_glink3_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink3_in[67:51]), .pattern_in(glink3_pattern), .count_out(glink3_error_counter_out_phase3));
Error_Counter Error_Counter_glink4_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink4_in[67:51]), .pattern_in(glink4_pattern), .count_out(glink4_error_counter_out_phase3));
Error_Counter Error_Counter_glink5_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink5_in[67:51]), .pattern_in(glink5_pattern), .count_out(glink5_error_counter_out_phase3));
Error_Counter Error_Counter_glink6_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink6_in[67:51]), .pattern_in(glink6_pattern), .count_out(glink6_error_counter_out_phase3));
Error_Counter Error_Counter_glink7_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink7_in[67:51]), .pattern_in(glink7_pattern), .count_out(glink7_error_counter_out_phase3));
Error_Counter Error_Counter_glink8_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink8_in[67:51]), .pattern_in(glink8_pattern), .count_out(glink8_error_counter_out_phase3));
Error_Counter Error_Counter_glink9_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink9_in[67:51]), .pattern_in(glink9_pattern), .count_out(glink9_error_counter_out_phase3));
Error_Counter Error_Counter_glink10_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink10_in[67:51]), .pattern_in(glink10_pattern), .count_out(glink10_error_counter_out_phase3));
Error_Counter Error_Counter_glink11_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink11_in[67:51]), .pattern_in(glink11_pattern), .count_out(glink11_error_counter_out_phase3));
Error_Counter Error_Counter_glink12_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink12_in[67:51]), .pattern_in(glink12_pattern), .count_out(glink12_error_counter_out_phase3));
Error_Counter Error_Counter_glink13_phase3(.CLK_in(CLK_IN), .reset_in(RST_in), .data_in(glink13_in[67:51]), .pattern_in(glink13_pattern), .count_out(glink13_error_counter_out_phase3));

endmodule
