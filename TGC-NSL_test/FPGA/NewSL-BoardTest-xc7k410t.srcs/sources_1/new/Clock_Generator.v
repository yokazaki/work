`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/04/26 21:13:19
// Design Name: 
// Module Name: Clock_Generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Clock_Generator(
    input wire FPGA_CLK_in,
    input wire FPGA_CLK40_in,
    input wire reset_in,
    output wire TTC_CLK_out,
    output wire CLK_25_out,
    output wire CLK_40_0_out,
    output wire CLK_40_1_out,
    output wire CLK_40_2_out,
    output wire CLK_40_3_out,
    output wire CLK_125_out,
    output wire CLK_160_out,
    output wire locked_out
    );
    

wire locked25;
wire locked125;
wire locked160;
wire locked40;
wire clk_125_int;
    
BUFG FPGA_CLK_BUFG ( .I(FPGA_CLK_in), .O(TTC_CLK_out));

wire TTC_CLK;
assign TTC_CLK = TTC_CLK_out;
    
dcm40_to_4phases dcm40_to_4phases(
    .clk_in     (TTC_CLK_out),
    .clk40_0    (CLK_40_0_out),
    .clk40_1    (CLK_40_1_out),
    .clk40_2    (CLK_40_2_out),
    .clk40_3    (CLK_40_3_out),
    .reset      (reset_in),
    .locked     (locked40)
);

dcm40_to_160 dcm40_to_160(
    .clk_in     (CLK_40_0_out),
    .clk160     (CLK_160_out),
    .reset      (reset_in),
    .locked     (locked160)
);
    
dcm40_to_125 dcm40_to_125(
    .clk_in     (CLK_40_0_out),
    .clk125     (clk_125_int),
    .reset      (reset_in),
    .locked     (locked125)
);
    
dcm125_to_25_125 dcm125_to_25_125(
    .clk_in     (clk_125_int),
    .clk25      (CLK_25_out),
    .clk125     (CLK_125_out),
    .reset      (reset_in),
    .locked     (locked25)
);
    
    assign locked_out = locked40 && locked160 && locked125 && locked25;

endmodule
