`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 21:31:37
// Design Name: 
// Module Name: Monitoring
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Monitoring(
    input wire wr_CLK_in,
    input wire rd_CLK_in,
    input wire [203:0] data_in,
    input wire RST_in,
    input wire write_enb,
    input wire read_enb,    
    input wire debug_flag,
    output wire [12:0] wr_cnt,
    output wire control_flag,
    output wire [203:0] FIFO_data_out
    );
    /////////// Monitoring FIFO ////////////////////
    
    // 160 bit RX data is put into 256bit -> 256 bit FIFO.
    // write enable and read enable signals are controlled by VME (comment by akatsuka)
    
    wire wr_en;
    wire rd_en;
    wire full;
    wire empty;

   //FIFO WRITE ENABLE
     wire FIFO_read_enb;
     reg [2:0] shift_reg;
     reg read_enb_reg;
     reg write_enb_reg;
     reg control_flag_reg;
            
      assign FIFO_read_enb = (!shift_reg[2] && shift_reg[1]);
      always @(posedge rd_CLK_in) begin
           shift_reg[2:0] <= {shift_reg[1:0], read_enb};
      end

      always @(posedge wr_CLK_in) begin
        if(RST_in) begin
            control_flag_reg <= 1'b0;
        end
        else if(full) begin
            control_flag_reg <= 1'b1;
        end
      end
 
      wire cf = control_flag_reg;  
      assign rd_en = FIFO_read_enb && !empty;
      assign control_flag = control_flag_reg;
 //     assign wr_en = write_enb && !full;
      assign wr_en = write_enb && !cf;
//      assign wr_en = !cf && debug_flag;

    
    monitoring_fifo monitoring_fifo(
      .rst(RST_in),                // input wire srst
      .wr_clk(wr_CLK_in),                  // inpsut wire clk
      .rd_clk(rd_CLK_in),                  // inpsut wire clk
      .din(data_in),                  // input wire [15 : 0] din
      .wr_en(wr_en),              // input wire wr_en
      .rd_en(rd_en),              // input wire rd_en
      .wr_data_count    (wr_cnt),
      .dout(FIFO_data_out),                // output wire [15 : 0] dout
      .full(full),                // output wire full
      .empty(empty)              // output wire empty
    );
endmodule
