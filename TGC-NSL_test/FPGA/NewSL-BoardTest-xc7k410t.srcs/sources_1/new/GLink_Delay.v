`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/04/26 21:25:13
// Design Name: 
// Module Name: GLink_Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_Delay(
    input wire CLK_0_in,
    input wire CLK_1_in,
    input wire CLK_2_in,
    input wire CLK_3_in,
    input wire reset_in,
    input wire Delay_reset_in,

    input wire [20:0] glink0_data_in,
    input wire [20:0] glink1_data_in,
    input wire [20:0] glink2_data_in,
    input wire [20:0] glink3_data_in,
    input wire [20:0] glink4_data_in,
    input wire [20:0] glink5_data_in,
    input wire [20:0] glink6_data_in,
    input wire [20:0] glink7_data_in,
    input wire [20:0] glink8_data_in,
    input wire [20:0] glink9_data_in,
    input wire [20:0] glink10_data_in,
    input wire [20:0] glink11_data_in,
    input wire [20:0] glink12_data_in,
    input wire [20:0] glink13_data_in,

    input wire [3:0] glink0_phase_in,
    input wire [3:0] glink1_phase_in,
    input wire [3:0] glink2_phase_in,
    input wire [3:0] glink3_phase_in,
    input wire [3:0] glink4_phase_in,
    input wire [3:0] glink5_phase_in,
    input wire [3:0] glink6_phase_in,
    input wire [3:0] glink7_phase_in,
    input wire [3:0] glink8_phase_in,
    input wire [3:0] glink9_phase_in,
    input wire [3:0] glink10_phase_in,
    input wire [3:0] glink11_phase_in,    
    input wire [3:0] glink12_phase_in,    
    input wire [3:0] glink13_phase_in,    

    output wire [20:0] glink0_delayed_out,
    output wire [20:0] glink1_delayed_out,
    output wire [20:0] glink2_delayed_out,
    output wire [20:0] glink3_delayed_out,
    output wire [20:0] glink4_delayed_out,
    output wire [20:0] glink5_delayed_out,
    output wire [20:0] glink6_delayed_out,
    output wire [20:0] glink7_delayed_out,
    output wire [20:0] glink8_delayed_out,
    output wire [20:0] glink9_delayed_out,
    output wire [20:0] glink10_delayed_out,
    output wire [20:0] glink11_delayed_out,
    output wire [20:0] glink12_delayed_out,
    output wire [20:0] glink13_delayed_out,

    output wire [3:0] glink0_phase_out,
    output wire [3:0] glink1_phase_out,
    output wire [3:0] glink2_phase_out,
    output wire [3:0] glink3_phase_out,
    output wire [3:0] glink4_phase_out,
    output wire [3:0] glink5_phase_out,
    output wire [3:0] glink6_phase_out,
    output wire [3:0] glink7_phase_out,
    output wire [3:0] glink8_phase_out,
    output wire [3:0] glink9_phase_out,
    output wire [3:0] glink10_phase_out,
    output wire [3:0] glink11_phase_out,
    output wire [3:0] glink12_phase_out,
    output wire [3:0] glink13_phase_out
    
    );

wire RST_in = reset_in||Delay_reset_in;

PhaseAlign PhaseAlign_glink0 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink0_data_in), .phase_in(glink0_phase_in), .phase_out(glink0_phase_out), .reset_in(RST_in), .data_out(glink0_delayed_out));
PhaseAlign PhaseAlign_glink1 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink1_data_in), .phase_in(glink1_phase_in), .phase_out(glink1_phase_out), .reset_in(RST_in), .data_out(glink1_delayed_out));
PhaseAlign PhaseAlign_glink2 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink2_data_in), .phase_in(glink2_phase_in), .phase_out(glink2_phase_out), .reset_in(RST_in), .data_out(glink2_delayed_out));
PhaseAlign PhaseAlign_glink3 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink3_data_in), .phase_in(glink3_phase_in), .phase_out(glink3_phase_out), .reset_in(RST_in), .data_out(glink3_delayed_out));
PhaseAlign PhaseAlign_glink4 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink4_data_in), .phase_in(glink4_phase_in), .phase_out(glink4_phase_out), .reset_in(RST_in), .data_out(glink4_delayed_out));
PhaseAlign PhaseAlign_glink5 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink5_data_in), .phase_in(glink5_phase_in), .phase_out(glink5_phase_out), .reset_in(RST_in), .data_out(glink5_delayed_out));
PhaseAlign PhaseAlign_glink6 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink6_data_in), .phase_in(glink6_phase_in), .phase_out(glink6_phase_out), .reset_in(RST_in), .data_out(glink6_delayed_out));
PhaseAlign PhaseAlign_glink7 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink7_data_in), .phase_in(glink7_phase_in), .phase_out(glink7_phase_out), .reset_in(RST_in), .data_out(glink7_delayed_out));
PhaseAlign PhaseAlign_glink8 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink8_data_in), .phase_in(glink8_phase_in), .phase_out(glink8_phase_out), .reset_in(RST_in), .data_out(glink8_delayed_out));
PhaseAlign PhaseAlign_glink9 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink9_data_in), .phase_in(glink9_phase_in), .phase_out(glink9_phase_out), .reset_in(RST_in), .data_out(glink9_delayed_out));
PhaseAlign PhaseAlign_glink10 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink10_data_in), .phase_in(glink10_phase_in), .phase_out(glink10_phase_out), .reset_in(RST_in), .data_out(glink10_delayed_out));
PhaseAlign PhaseAlign_glink11 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink11_data_in), .phase_in(glink11_phase_in), .phase_out(glink11_phase_out), .reset_in(RST_in), .data_out(glink11_delayed_out));
PhaseAlign PhaseAlign_glink12 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink12_data_in), .phase_in(glink12_phase_in), .phase_out(glink12_phase_out), .reset_in(RST_in), .data_out(glink12_delayed_out));
PhaseAlign PhaseAlign_glink13 ( .CLK_0_in(CLK_0_in), .CLK_1_in(CLK_1_in), .CLK_2_in(CLK_2_in), .CLK_3_in(CLK_3_in),  .data_in(glink13_data_in), .phase_in(glink13_phase_in), .phase_out(glink13_phase_out), .reset_in(RST_in), .data_out(glink13_delayed_out));



endmodule