`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/01 17:21:02
// Design Name: 
// Module Name: Monitor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Monitor(
    input wire CLK_in,
    input wire reset_in,
    input wire [9:0] data_in,
    input wire [4:0] pattern1_in,
    input wire [4:0] pattern2_in,
    output reg [31:0] count_out,
    output reg [31:0] pattern_checker_out,
    output reg debug_flag,
    output reg stop_flag
    );


reg [40:0] count_range;

always @(posedge CLK_in) begin
    if (reset_in) begin
        count_range[40:0] <= 41'b0;
        stop_flag <= 1'b0;
        debug_flag <= 1'b0;
        count_out[31:0] <= 32'b0;
        pattern_checker_out[31:0] <= 32'b0;
    end
    else begin
        count_range[40:0] <= count_range[40:0] + 41'b1;
        if(count_range[31:0] == 40'ha000000000)  stop_flag <= 1'b1;
        if ((|data_in != 1'b0) && (stop_flag != 1'b1)) begin
            count_out[31:0] <= count_out[31:0] + 32'b1;
            if( (data_in[9:5] != pattern2_in[4:0]) && (data_in[4:0] != pattern1_in[4:0])) begin
                pattern_checker_out[31:0] <= pattern_checker_out[31:0] + 32'b1;
            end
            if(count_out != pattern_checker_out) debug_flag <= 1'b1;
        end
    end
end



endmodule
