`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/06/27 15:21:23
// Design Name: 
// Module Name: SiTCP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module SiTCP(
    input wire [31:0] EXT_IP,
//for SiTCP && FPGA other circuit
    input   wire CLK_25_in,//40
    input   wire CLK_125_in,
    input   wire rd_clk_in,
    input   wire SiTCP_wr_in,	 
    input   wire [15:0] data_in,
	output  wire SiTCP_full_out,
	input   wire reset_in,
	input   wire SiTCP_reset_in,
	input   wire SiTCP_FIFO_reset_in, 
	input   wire ETH_TXCLK,
	output  wire ETH_CLKIN,
	output  wire ETH_GTXCLK,
	output  wire ETH_RESET_B,
    input   wire ETH_CRS,
    input   wire ETH_COL,
    output  wire [7:0] ETH_TXD,
    output  wire ETH_TXEN,
    output  wire ETH_TXER,
    input   wire ETH_RXCLK,
    input   wire [7:0] ETH_RXD,
    input   wire ETH_RXDV,
    input   wire ETH_RXER,
    output  wire ETH_MDC,
    inout   wire ETH_MDIO,
	output  wire ETH_HPD,
    output  wire PROM_CS,
    output  wire PROM_SK,
    output  wire PROM_DI,
    input   wire PROM_DO,
    
    output wire [12:0] SiTCP_data_count,
    output wire SiTCP_busy
    );
    wire ETH_RXCLK_buf;
    BUFG bufg(.I(ETH_RXCLK),.O(ETH_RXCLK_buf));

assign ETH_HPD = 1'b0;
//------------------------------------------------------------------------------
//	definition of clock & Reset
//------------------------------------------------------------------------------
	wire			int_ETH_TXCLK		;
	wire			sitcpFifoRe			;
	wire			sitcpFifoEmpty		;

    assign int_ETH_TXCLK = CLK_125_in;
    assign ETH_GTXCLK = CLK_125_in;
    assign ETH_CLKIN = CLK_25_in;
//------------------------------------------------------------------------------
//	NETWORK PROTOCOL PROCESSOR (wire reg)
//------------------------------------------------------------------------------
	wire	[31:0]	IP_ADDR			;
	wire	[15:0]	TCP_PORT			;
	wire	[15:0]	RBCP_PORT		;

	wire	[15:0]	TCP_RX_WC = 16'h0;
	wire	[7:0]		TCP_RX_DATA		;
	wire	[7:0]		TCP_TX_DATA		;
	wire				TCP_RX_WR		;
	wire				TCP_TX_WR		;
	wire				TCP_TX_FULL		;
	wire				TCP_OPEN			;
	wire				TCP_CLOSE		;
	wire				TCP_ERROR		;

	wire	[31:0]	RBCP_ADDR		;
	wire	[7:0]		RBCP_WD			;
	wire	[7:0]		RBCP_RD			;
	wire				RBCP_WE			;
	wire				RBCP_RE			;
	wire				RBCP_ACK			;
		
	wire				ETH_MDIO_OE		;
	wire				ETH_MDIO_OUT	;

	assign	ETH_MDIO	= (ETH_MDIO_OE	? ETH_MDIO_OUT	: 1'bz);
	 
//------------------------------------------------------------------------------
//	main
//------------------------------------------------------------------------------
/////sequencer3 /////////////////////////////////////////////////////////////
wire FIFOwr_en;
wire FIFOrd_en;
wire FIFOempty;
reg  FIFORD_OK;
wire FIFOvalid;
wire [7:0] FIFO_OUT;
reg SQ3_count;

always@(posedge rd_clk_in)begin 
    if(reset_in||SiTCP_reset_in) begin
	     FIFORD_OK <= 1'b0;
		  SQ3_count <= 1'b0;
		  //TCP_TX_OK <= 1'b0;
    end else if(!FIFOempty && SQ3_count == 1'b0) begin
	     FIFORD_OK <= 1'b0;
		  SQ3_count <= 1'b1;
	 end else if(!FIFOempty && SQ3_count == 1'b1)begin
	     FIFORD_OK <= 1'b1;
	 end else if(FIFOempty && SQ3_count == 1'b1)begin
	     FIFORD_OK <= 1'b0;
		  SQ3_count <= 1'b0;
	 end else if(FIFOempty && SQ3_count == 1'b0)begin
	     FIFORD_OK <= 1'b0;
		  SQ3_count <= 1'b0;
	 end
end

////FIFO storing the formated data  //////////////////////////////////////////////////////////////////

SiTCP_FIFO SiTCP_FIFO(
  .clk(rd_clk_in), // input wr_clk
  .rst(reset_in||SiTCP_reset_in||SiTCP_FIFO_reset_in), // input rst
  .din(data_in), // input [31 : 0] din
  .wr_en(FIFOwr_en), // input wr_en
  .rd_en(FIFOrd_en), // input rd_en
  .dout(FIFO_OUT), // output [7 : 0] dout
  .full(SiTCP_full_out), // output full
  .empty(FIFOempty), // output empty
  .valid(FIFOvalid), // output valid
  .wr_data_count(SiTCP_data_count),  // output wire [10 : 0] wr_data_count
  .prog_full(SiTCP_busy)          // output wire prog_full
);

assign FIFOwr_en = !SiTCP_full_out && SiTCP_wr_in;
assign FIFOrd_en = !FIFOempty && FIFORD_OK && !TCP_TX_FULL;
assign TCP_TX_DATA = FIFO_OUT;
assign TCP_TX_WR = !TCP_TX_FULL && TCP_OPEN && FIFORD_OK && FIFOvalid;//addition FIFORD_OK for forbit double count
wire SiTCP_RST;
//------------------------------------------------------------------------------
//	NETWORK PROTOCOL PROCESSOR (body of SiTCP)
//------------------------------------------------------------------------------
WRAP_SiTCP_GMII_XC7K_32K		
		#(160) // = System clock frequency(MHz), integer only
	SiTCP(
		.CLK						(rd_clk_in       		),	// in	: System Clock > 129MHz
		.RST						(reset_in||SiTCP_reset_in		),	// in	: System reset
	// Configuration parameters
		//.FORCE_DEFAULTn		(FPGA_MODE			),	// in	: Load default parameters
		.FORCE_DEFAULTn		(1'b0		),	// in	: Load default parameters
		.EXT_IP_ADDR			(32'b0				),	// in	: IP address[31:0]
		.EXT_TCP_PORT			(16'd0				),	// in	: TCP port #[15:0]
		.EXT_RBCP_PORT			(16'd0				),	// in	: RBCP port #[15:0]
		.PHY_ADDR				(5'b00001			),	// in	: PHY-device MIF address[4:0]
	// EEPROM
		.EEPROM_CS				(PROM_CS				),	// out	: Chip select
		.EEPROM_SK				(PROM_SK				),	// out	: Serial data clock
		.EEPROM_DI				(PROM_DI				),	// out	: Serial write data
		.EEPROM_DO				(PROM_DO				),	// in	: Serial read data
	// user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
	   .USR_REG_X3C			(),	// out	: Stored at 0xFFFF_FF3C
	   .USR_REG_X3D			(),	// out	: Stored at 0xFFFF_FF3D
	   .USR_REG_X3E			(),	// out	: Stored at 0xFFFF_FF3E
	   .USR_REG_X3F			(),	// out	: Stored at 0xFFFF_FF3F

	// MII interface
		.GMII_RSTn				(ETH_RESET_B		),	// out	: PHY reset
//		.GMII_1000M				(~ETH_1000M_B		),	// in	: GMII mode (0:MII, 1:GMII)
        .GMII_1000M				(1'b1		),
		// TX
		.GMII_TX_CLK			(int_ETH_TXCLK	),	// in	: Tx clock
		.GMII_TX_EN				(ETH_TXEN			),	// out	: Tx enable
		.GMII_TXD				(ETH_TXD[7:0]		),	// out	: Tx data[7:0]
		.GMII_TX_ER				(ETH_TXER			),	// out	: TX error
		// RX
		.GMII_RX_CLK			(ETH_RXCLK_buf			),	// in	: Rx clock
		.GMII_RX_DV				(ETH_RXDV			),	// in	: Rx data valid
		.GMII_RXD				(ETH_RXD[7:0]		),	// in	: Rx data[7:0]
		.GMII_RX_ER				(ETH_RXER			),	// in	: Rx error
		.GMII_CRS				(ETH_CRS				),	// in	: Carrier sense
		.GMII_COL				(ETH_COL				),	// in	: Collision detected
		// Management IF
		.GMII_MDC				(ETH_MDC				),	// out	: Clock for MDIO
		.GMII_MDIO_IN			(ETH_MDIO			),	// in	: Data
		.GMII_MDIO_OUT			(ETH_MDIO_OUT		),	// out	: Data
		.GMII_MDIO_OE			(ETH_MDIO_OE		),	// out	: MDIO output enable
	// User I/F
		.SiTCP_RST				(SiTCP_RST			),	// out	: Reset for SiTCP and related circuits
		// TCP connection control
		.TCP_OPEN_REQ			(1'b0					),	// in	: Reserved input, shoud be 0
		.TCP_OPEN_ACK			(TCP_OPEN			),	// out	: Acknowledge for open (=Socket busy)
		.TCP_ERROR				(TCP_ERROR			),	// out	: TCP error, its active period is equal to MSL
		.TCP_CLOSE_REQ			(TCP_CLOSE			),	// out	: Connection close request
		.TCP_CLOSE_ACK			(TCP_CLOSE			),	// in	: Acknowledge for closing
		// FIFO I/F
		.TCP_RX_WC				(TCP_RX_WC[15:0]	),	// in	: Rx FIFO write count[15:0] (Unused bits should be set 1)
		.TCP_RX_WR				(TCP_RX_WR			),	// out	: Write enable
		.TCP_RX_DATA			(TCP_RX_DATA[7:0]	),	// out	: Write data[7:0]
		.TCP_TX_FULL			(TCP_TX_FULL		),	// out	: Almost full flag
		.TCP_TX_WR				(TCP_TX_WR			),	// in	: Write enable!!!
		.TCP_TX_DATA			(TCP_TX_DATA[7:0]	),	// in	: Write data[7:0]
		// RBCP
		.RBCP_ACT				(			),	// out	: RBCP active
		.RBCP_ADDR				(RBCP_ADDR[31:0]	),	// out	: Address[31:0]
		.RBCP_WD					(RBCP_WD[7:0]		),	// out	: Data[7:0]
		.RBCP_WE					(RBCP_WE				),	// out	: Write enable
		.RBCP_RE					(RBCP_RE				),	// out	: Read enable
		.RBCP_ACK				(RBCP_ACK			),	// in	: Access acknowledge
		.RBCP_RD					(RBCP_RD[7:0]		)	// in	: Read data[7:0]
	);
	

//------------------------------------------------------------------------------
//	RBCP Registers (free registers 0x8-0xf)
//------------------------------------------------------------------------------
reg [7:0] din0 = 8'h0;
reg [7:0] din1 = 8'h1;
reg [7:0] din2 = 8'h2;
reg [7:0] din3 = 8'h3;
reg [7:0] din4 = 8'h4;
reg [7:0] din5 = 8'h5;
reg [7:0] din6 = 8'h6;
reg [7:0] din7 = 8'h7;
wire [7:0] dout8;
wire [7:0] dout9;
wire [7:0] doutA;
wire [7:0] doutB;
wire [7:0] doutC;
wire [7:0] doutD;
wire [7:0] doutE;
wire [7:0] doutF;

	RBCP_REG		RBCP_REG(
		// System
		.CLK					(rd_clk_in				),	// in	: System clock
		.RST					(SiTCP_RST			),	// in	: System reset
		// RBCP I/F
//		.RBCP_ACT			(RBCP_ACT			),	// in	: Active
		.RBCP_ADDR			(RBCP_ADDR[31:0]	),	// in	: Address[31:0]
		.RBCP_WE				(RBCP_WE				),	// in	: Write enable
		.RBCP_WD				(RBCP_WD[7:0]		),	// in	: Write data[7:0]
		.RBCP_RE				(RBCP_RE				),	// in	: Read enable
		.RBCP_RD				(RBCP_RD[7:0]		),	// out: Read data[7:0]
		.RBCP_ACK			(RBCP_ACK			),	// out: Acknowledge
		.din0					(din0					),	// in
		.din1					(din1					),	// in
		.din2					(din2					),	// in
		.din3					(din3					),	// in
		.din4					(din4					),	// in
		.din5					(din5					),	// in
		.din6					(din6					),	// in
		.din7					(din7					),	// in
		.dout8				(dout8				),	// out
		.dout9				(dout9				),	// out
		.doutA				(doutA				),	// out
		.doutB				(doutB				),	// out
		.doutC				(doutC				),	// out
		.doutD				(doutD				),	// out
		.doutE				(doutE				),	// out
		.doutF				(doutF				)	// out
	);


endmodule