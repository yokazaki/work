`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/04/26 20:41:34
// Design Name: 
// Module Name: NewSL-BoardTest-xc7k410t
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NewSL_BoardTest_xc7k410t(
// Common Ports
    input wire  FPGA_CLK,   // Clock chosen by jumper pin: EXT, INT or TTC 
    input wire FPGA_CLK40,  // Clock from INT; Quartz on the board

// TTC Signals
    input wire L1A,
    input wire BCR,
    input wire ECR,
    input wire TTC_RESET,
    input wire TEST_PULSE_TRIGGER,
    output wire BUSY,

// GTX 
    input wire  Q2_CLK0_GTREFCLK_PAD_N_IN,
    input wire  Q2_CLK0_GTREFCLK_PAD_P_IN,
    input  wire [11:0]  RXN_IN,
    input  wire [11:0]  RXP_IN,
    output wire [11:0]  TXN_OUT,
    output wire [11:0]  TXP_OUT,
    output wire TX_DISABLE0,
    output wire TX_DISABLE1,
    output wire TX_DISABLE2,

// G-Link
    input wire [20:0] GL0,
    input wire [20:0] GL1,
    input wire [20:0] GL2,
    input wire [20:0] GL3,
    input wire [20:0] GL4,
    input wire [20:0] GL5,
    input wire [20:0] GL6,
    input wire [20:0] GL7,
    input wire [20:0] GL8,
    input wire [20:0] GL9,
    input wire [20:0] GL10,
    input wire [20:0] GL11,
    input wire [20:0] GL12,
    input wire [20:0] GL13,
    
    output wire [13:0] GLink_RX_DIV,//RX_DIV

// VME    
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

// SiTCP    
    input  wire FPGA_RESET_B,
    input  wire ETH_TXCLK,
    output wire ETH_CLKIN,
    output wire ETH_GTXCLK,
    output wire ETH_RESET_B,
    input  wire ETH_CRS,
    input  wire ETH_COL,
    output wire [7:0] ETH_TXD,
    output wire ETH_TXEN,
    output wire ETH_TXER,
    input  wire ETH_RXCLK,
    input  wire [7:0] ETH_RXD,
    input  wire ETH_RXDV,
    input  wire ETH_RXER,
    output wire ETH_MDC,
    inout  wire ETH_MDIO,
    output wire ETH_HPD,
    output wire PROM_CS,
    output wire PROM_SK,
    output wire PROM_DI,
    input  wire PROM_DO,

// Test Pins
    output wire  [15:0] TESTPIN,
    
// NIM outs    
    output wire NIMOUT_0,
    output wire NIMOUT_1,
    output wire NIMOUT_2,
    output wire NIMOUT_3
    );

// Wire Declarations

// Clock wires
wire TTC_CLK;
wire CLK_25;
wire CLK_40_0;
wire CLK_40_1;
wire CLK_40_2;
wire CLK_40_3;
wire CLK_125;
wire CLK_160;    
wire locked;

// Reset
wire RESET_TTC;
wire RESET_40;
wire RESET_160;
wire RESET_TX;
wire Scaler_reset;
wire TX_reset;
wire RX_reset;
wire Delay_reset;
wire L1Buffer_reset;
wire Derandomizer_reset;
wire ZeroSupp_reset;
wire CLK_reset;
wire FIFO_reset;
wire SiTCP_reset;
wire SiTCP_FIFO_reset;
wire TX_Logic_reset;
wire L1A_manual_reset;


// G-Link
// G-Link Phase parameters
wire [3:0] Phase_glink0;
wire [3:0] Phase_glink1;
wire [3:0] Phase_glink2;
wire [3:0] Phase_glink3;
wire [3:0] Phase_glink4;
wire [3:0] Phase_glink5;
wire [3:0] Phase_glink6;
wire [3:0] Phase_glink7;
wire [3:0] Phase_glink8;
wire [3:0] Phase_glink9;
wire [3:0] Phase_glink10;
wire [3:0] Phase_glink11;
wire [3:0] Phase_glink12;
wire [3:0] Phase_glink13;

wire [3:0] Phase_glink0_mon;
wire [3:0] Phase_glink1_mon;
wire [3:0] Phase_glink2_mon;
wire [3:0] Phase_glink3_mon;
wire [3:0] Phase_glink4_mon;
wire [3:0] Phase_glink5_mon;
wire [3:0] Phase_glink6_mon;
wire [3:0] Phase_glink7_mon;
wire [3:0] Phase_glink8_mon;
wire [3:0] Phase_glink9_mon;
wire [3:0] Phase_glink10_mon;
wire [3:0] Phase_glink11_mon;
wire [3:0] Phase_glink12_mon;
wire [3:0] Phase_glink13_mon;

wire [20:0] glink0_delayed;
wire [20:0] glink1_delayed;
wire [20:0] glink2_delayed;
wire [20:0] glink3_delayed;
wire [20:0] glink4_delayed;
wire [20:0] glink5_delayed;
wire [20:0] glink6_delayed;
wire [20:0] glink7_delayed;
wire [20:0] glink8_delayed;
wire [20:0] glink9_delayed;
wire [20:0] glink10_delayed;
wire [20:0] glink11_delayed;
wire [20:0] glink12_delayed;
wire [20:0] glink13_delayed;

//glink IFD
//G-Link IFD
wire [20:0] GL0_ifd;
wire [20:0] GL1_ifd;
wire [20:0] GL2_ifd;
wire [20:0] GL3_ifd;
wire [20:0] GL4_ifd;
wire [20:0] GL5_ifd;
wire [20:0] GL6_ifd;
wire [20:0] GL7_ifd;
wire [20:0] GL8_ifd;
wire [20:0] GL9_ifd;
wire [20:0] GL10_ifd;
wire [20:0] GL11_ifd;
wire [20:0] GL12_ifd;
wire [20:0] GL13_ifd;
wire [83:0] GL0_ifd_all;
wire [83:0] GL1_ifd_all;
wire [83:0] GL2_ifd_all;
wire [83:0] GL3_ifd_all;
wire [83:0] GL4_ifd_all;
wire [83:0] GL5_ifd_all;
wire [83:0] GL6_ifd_all;
wire [83:0] GL7_ifd_all;
wire [83:0] GL8_ifd_all;
wire [83:0] GL9_ifd_all;
wire [83:0] GL10_ifd_all;
wire [83:0] GL11_ifd_all;
wire [83:0] GL12_ifd_all;
wire [83:0] GL13_ifd_all;
wire [3:0] TMDB_BCID_phase0;
wire [3:0] TMDB_BCID_phase1;
wire [3:0] TMDB_BCID_phase2;
wire [3:0] TMDB_BCID_phase3;
wire [15:0] TMDB_BCID_error_counter_phase0;
wire [15:0] TMDB_BCID_error_counter_phase1;
wire [15:0] TMDB_BCID_error_counter_phase2;
wire [15:0] TMDB_BCID_error_counter_phase3;

wire [15:0] test_pulse_counter;

wire [31:0] counter;
wire [31:0] checker;

wire [31:0] glink0_counter;
wire [31:0] glink1_counter;
wire [31:0] glink2_counter;
wire [31:0] glink3_counter;
wire [31:0] glink4_counter;
wire [31:0] glink5_counter;
wire [31:0] glink6_counter;
wire [31:0] glink7_counter;
wire [31:0] glink8_counter;
wire [31:0] glink9_counter;
wire [31:0] glink10_counter;
wire [31:0] glink11_counter;

wire [31:0] glink0_error_counter_phase0;
wire [31:0] glink1_error_counter_phase0;
wire [31:0] glink2_error_counter_phase0;
wire [31:0] glink3_error_counter_phase0;
wire [31:0] glink4_error_counter_phase0;
wire [31:0] glink5_error_counter_phase0;
wire [31:0] glink6_error_counter_phase0;
wire [31:0] glink7_error_counter_phase0;
wire [31:0] glink8_error_counter_phase0;
wire [31:0] glink9_error_counter_phase0;
wire [31:0] glink10_error_counter_phase0;
wire [31:0] glink11_error_counter_phase0;
wire [31:0] glink12_error_counter_phase0;
wire [31:0] glink13_error_counter_phase0;
wire [31:0] glink0_error_counter_phase1;
wire [31:0] glink1_error_counter_phase1;
wire [31:0] glink2_error_counter_phase1;
wire [31:0] glink3_error_counter_phase1;
wire [31:0] glink4_error_counter_phase1;
wire [31:0] glink5_error_counter_phase1;
wire [31:0] glink6_error_counter_phase1;
wire [31:0] glink7_error_counter_phase1;
wire [31:0] glink8_error_counter_phase1;
wire [31:0] glink9_error_counter_phase1;
wire [31:0] glink10_error_counter_phase1;
wire [31:0] glink11_error_counter_phase1;
wire [31:0] glink12_error_counter_phase1;
wire [31:0] glink13_error_counter_phase1;
wire [31:0] glink0_error_counter_phase2;
wire [31:0] glink1_error_counter_phase2;
wire [31:0] glink2_error_counter_phase2;
wire [31:0] glink3_error_counter_phase2;
wire [31:0] glink4_error_counter_phase2;
wire [31:0] glink5_error_counter_phase2;
wire [31:0] glink6_error_counter_phase2;
wire [31:0] glink7_error_counter_phase2;
wire [31:0] glink8_error_counter_phase2;
wire [31:0] glink9_error_counter_phase2;
wire [31:0] glink10_error_counter_phase2;
wire [31:0] glink11_error_counter_phase2;
wire [31:0] glink12_error_counter_phase2;
wire [31:0] glink13_error_counter_phase2;
wire [31:0] glink0_error_counter_phase3;
wire [31:0] glink1_error_counter_phase3;
wire [31:0] glink2_error_counter_phase3;
wire [31:0] glink3_error_counter_phase3;
wire [31:0] glink4_error_counter_phase3;
wire [31:0] glink5_error_counter_phase3;
wire [31:0] glink6_error_counter_phase3;
wire [31:0] glink7_error_counter_phase3;
wire [31:0] glink8_error_counter_phase3;
wire [31:0] glink9_error_counter_phase3;
wire [31:0] glink10_error_counter_phase3;
wire [31:0] glink11_error_counter_phase3;
wire [31:0] glink12_error_counter_phase3;
wire [31:0] glink13_error_counter_phase3;

wire [31:0] glink0_pattern_checker;
wire [31:0] glink1_pattern_checker;
wire [31:0] glink2_pattern_checker;
wire [31:0] glink3_pattern_checker;
wire [31:0] glink4_pattern_checker;
wire [31:0] glink5_pattern_checker;
wire [31:0] glink6_pattern_checker;
wire [31:0] glink7_pattern_checker;
wire [31:0] glink8_pattern_checker;
wire [31:0] glink9_pattern_checker;
wire [31:0] glink10_pattern_checker;
wire [31:0] glink11_pattern_checker;

wire [1:0] pattern_selector1;
wire [1:0] pattern_selector2;
wire [4:0] check_pattern0;
wire [4:0] check_pattern1;
wire [4:0] check_pattern2;
wire [4:0] check_pattern3;
wire [4:0] pattern_selected1;
wire [4:0] pattern_selected2;

wire [15:0] glink_delayed;
reg [15:0] glink_delayed_reg;
wire [15:0] glink_delayed_buf;

reg [67:0] glink0_delayed_reg;
reg [67:0] glink1_delayed_reg;
reg [67:0] glink2_delayed_reg;
reg [67:0] glink3_delayed_reg;
reg [67:0] glink4_delayed_reg;
reg [67:0] glink5_delayed_reg;
reg [67:0] glink6_delayed_reg;
reg [67:0] glink7_delayed_reg;
reg [67:0] glink8_delayed_reg;
reg [67:0] glink9_delayed_reg;
reg [67:0] glink10_delayed_reg;
reg [67:0] glink11_delayed_reg;
reg [67:0] glink12_delayed_reg;
reg [67:0] glink13_delayed_reg;

wire [67:0] glink0_delayed_buf;
wire [67:0] glink1_delayed_buf;
wire [67:0] glink2_delayed_buf;
wire [67:0] glink3_delayed_buf;
wire [67:0] glink4_delayed_buf;
wire [67:0] glink5_delayed_buf;
wire [67:0] glink6_delayed_buf;
wire [67:0] glink7_delayed_buf;
wire [67:0] glink8_delayed_buf;
wire [67:0] glink9_delayed_buf;
wire [67:0] glink10_delayed_buf;
wire [67:0] glink11_delayed_buf;
wire [67:0] glink12_delayed_buf;
wire [67:0] glink13_delayed_buf;

assign GLink_RX_DIV[13:0] = 14'h0;

// monitoring FIFO
wire            write_enb;
wire            read_enb;
wire [1:0]      signal_selector;
wire            write_enb_selected;
wire           rd_cnt;
wire [203:0] FIFO_data;
wire [191:0] debug_FIFO_data;
wire           control_flag;

// wires for SiTCP
wire [63:0] SiTCP_in;
wire        SiTCP_full;
wire [12:0] SiTCP_data_count;
wire        SiTCP_busy;
wire [15:0] SiTCP_monitor;

// NIM Output
wire [7:0] NIM_OUT;

wire Quartz;

// TESTPIN
assign TESTPIN[15:0] = glink_delayed[15:0];

wire gt0_rx_reset;
wire gt1_rx_reset;
wire gt2_rx_reset;
wire gt3_rx_reset;
wire gt4_rx_reset;
wire gt5_rx_reset;
wire gt6_rx_reset;
wire gt7_rx_reset;
wire gt8_rx_reset;
wire gt9_rx_reset;
wire gt10_rx_reset;
wire gt11_rx_reset;
wire [31:0] gt0_txdata;
wire [3:0]  gt0_txcharisk;
wire [31:0] gt1_txdata;
wire [3:0]  gt1_txcharisk;
wire [31:0] gt2_txdata;
wire [3:0]  gt2_txcharisk;
wire [31:0] gt3_txdata;
wire [3:0]  gt3_txcharisk;
wire [31:0] gt4_txdata;
wire [3:0]  gt4_txcharisk;
wire [31:0] gt5_txdata;
wire [3:0]  gt5_txcharisk;
wire [31:0] gt6_txdata;
wire [3:0]  gt6_txcharisk;
wire [31:0] gt7_txdata;
wire [3:0]  gt7_txcharisk;
wire [31:0] gt8_txdata;
wire [3:0]  gt8_txcharisk;
wire [31:0] gt9_txdata;
wire [3:0]  gt9_txcharisk;
wire [31:0] gt10_txdata;
wire [3:0]  gt10_txcharisk;
wire [31:0] gt11_txdata;
wire [3:0]  gt11_txcharisk;
wire gt_txusrclk2;
wire [31:0] gt0_rxdata;
wire [3:0]  gt0_rxcharisk;
wire [31:0] gt1_rxdata;
wire [3:0]  gt1_rxcharisk;
wire [31:0] gt2_rxdata;
wire [3:0]  gt2_rxcharisk;
wire [31:0] gt3_rxdata;
wire [3:0]  gt3_rxcharisk;
wire [31:0] gt4_rxdata;
wire [3:0]  gt4_rxcharisk;
wire [31:0] gt5_rxdata;
wire [3:0]  gt5_rxcharisk;
wire [31:0] gt6_rxdata;
wire [3:0]  gt6_rxcharisk;
wire [31:0] gt7_rxdata;
wire [3:0]  gt7_rxcharisk;
wire [31:0] gt8_rxdata;
wire [3:0]  gt8_rxcharisk;
wire [31:0] gt9_rxdata;
wire [3:0]  gt9_rxcharisk;
wire [31:0] gt10_rxdata;
wire [3:0]  gt10_rxcharisk;
wire [31:0] gt11_rxdata;
wire [3:0]  gt11_rxcharisk;
wire gt0_rxusrclk;
wire gt1_rxusrclk;
wire gt2_rxusrclk;
wire gt3_rxusrclk;
wire gt4_rxusrclk;
wire gt5_rxusrclk;
wire gt6_rxusrclk;
wire gt7_rxusrclk;
wire gt8_rxusrclk;
wire gt9_rxusrclk;
wire gt10_rxusrclk;
wire gt11_rxusrclk;
wire [2:0] loopback_mode = 3'b0;
wire [31:0] eye0_data_out;
wire [31:0] eye1_data_out;
wire [31:0] eye2_data_out;
wire [31:0] eye3_data_out;
wire [31:0] eye4_data_out;
wire [31:0] eye5_data_out;
wire [31:0] eye6_data_out;
wire [31:0] eye7_data_out;
wire [31:0] eye8_data_out;
wire [31:0] eye9_data_out;
wire [31:0] eye10_data_out;
wire [31:0] eye11_data_out;
wire [14:0] eye0_data_count;
wire [14:0] eye1_data_count;
wire [14:0] eye2_data_count;
wire [14:0] eye3_data_count;
wire [14:0] eye4_data_count;
wire [14:0] eye5_data_count;
wire [14:0] eye6_data_count;
wire [14:0] eye7_data_count;
wire [14:0] eye8_data_count;
wire [14:0] eye9_data_count;
wire [14:0] eye10_data_count;
wire [14:0] eye11_data_count;
wire [4:0] eye0_state;
wire [4:0] eye1_state;
wire [4:0] eye2_state;
wire [4:0] eye3_state;
wire [4:0] eye4_state;
wire [4:0] eye5_state;
wire [4:0] eye6_state;
wire [4:0] eye7_state;
wire [4:0] eye8_state;
wire [4:0] eye9_state;
wire [4:0] eye10_state;
wire [4:0] eye11_state;
wire eye_rd_en;
wire eye_go;
wire [8:0] drpaddrReg;
wire [15:0] drpdiReg;
wire drpEnCmd;
wire drpWeCmd;
wire [11:0] hstepSize;
wire [10:0] vstepSize;
wire prbs_reset;
wire [2:0] prbssel;
wire [8:0] hstep_max;
wire [8:0] vstep_max;
wire [11:0] horz_init;
wire [10:0] vert_init;
wire [15:0] gt0_drpdo;
wire [15:0] gt1_drpdo;
wire [15:0] gt2_drpdo;
wire [15:0] gt3_drpdo;
wire [15:0] gt4_drpdo;
wire [15:0] gt5_drpdo;
wire [15:0] gt6_drpdo;
wire [15:0] gt7_drpdo;
wire [15:0] gt8_drpdo;
wire [15:0] gt9_drpdo;
wire [15:0] gt10_drpdo;
wire [15:0] gt11_drpdo;
wire lpmen;

wire debug_flag;
wire stop_flag;
wire [203:0] glink_pattern;
wire [16:0] glink0_pattern;
wire [16:0] glink1_pattern;
wire [16:0] glink2_pattern;
wire [16:0] glink3_pattern;
wire [16:0] glink4_pattern;
wire [16:0] glink5_pattern;
wire [16:0] glink6_pattern;
wire [16:0] glink7_pattern;
wire [16:0] glink8_pattern;
wire [16:0] glink9_pattern;
wire [16:0] glink10_pattern;
wire [16:0] glink11_pattern;
wire [16:0] glink12_pattern;
wire [16:0] glink13_pattern;



GTX_12lane_exdes GTX_12lane
(
    .refclk_n_in        (Q2_CLK0_GTREFCLK_PAD_N_IN),
    .refclk_p_in        (Q2_CLK0_GTREFCLK_PAD_P_IN),
    .drpclk_in          (Quartz),
    .reset_in           (RESET_TTC),
    .TX_reset_in        (TX_reset),
    .RX_reset_in        (RX_reset),
    .gt0_rx_reset_in    (gt0_rx_reset),
    .gt1_rx_reset_in    (gt1_rx_reset),
    .gt2_rx_reset_in    (gt2_rx_reset),
    .gt3_rx_reset_in    (gt3_rx_reset),
    .gt4_rx_reset_in    (gt4_rx_reset),
    .gt5_rx_reset_in    (gt5_rx_reset),
    .gt6_rx_reset_in    (gt6_rx_reset),
    .gt7_rx_reset_in    (gt7_rx_reset),
    .gt8_rx_reset_in    (gt8_rx_reset),
    .gt9_rx_reset_in    (gt9_rx_reset),
    .gt10_rx_reset_in   (gt10_rx_reset),
    .gt11_rx_reset_in   (gt11_rx_reset),
    .Scaler_reset_in    (Scaler_reset),

    .RXN_IN             (RXN_IN),
    .RXP_IN             (RXP_IN),
    .TXN_OUT            (TXN_OUT),
    .TXP_OUT            (TXP_OUT),

// tx data 
    .gt0_txdata_in      (gt0_txdata), 
    .gt1_txdata_in      (gt1_txdata), 
    .gt2_txdata_in      (gt2_txdata), 
    .gt3_txdata_in      (gt3_txdata),
    .gt4_txdata_in      (gt4_txdata), 
    .gt5_txdata_in      (gt5_txdata), 
    .gt6_txdata_in      (gt6_txdata), 
    .gt7_txdata_in      (gt7_txdata),
    .gt8_txdata_in      (gt8_txdata), 
    .gt9_txdata_in      (gt9_txdata), 
    .gt10_txdata_in     (gt10_txdata), 
    .gt11_txdata_in     (gt11_txdata),
    
// tx char is k
    .gt0_txcharisk_in   (gt0_txcharisk), 
    .gt1_txcharisk_in   (gt1_txcharisk), 
    .gt2_txcharisk_in   (gt2_txcharisk), 
    .gt3_txcharisk_in   (gt3_txcharisk),
    .gt4_txcharisk_in   (gt4_txcharisk), 
    .gt5_txcharisk_in   (gt5_txcharisk), 
    .gt6_txcharisk_in   (gt6_txcharisk), 
    .gt7_txcharisk_in   (gt7_txcharisk),
    .gt8_txcharisk_in   (gt8_txcharisk), 
    .gt9_txcharisk_in   (gt9_txcharisk), 
    .gt10_txcharisk_in  (gt10_txcharisk), 
    .gt11_txcharisk_in  (gt11_txcharisk),
    
// tx user clock    
    .gt_txusrclk2_out   (gt_txusrclk2),
        
//rx data out
    .gt0_rxdata_out     (gt0_rxdata), 
    .gt1_rxdata_out     (gt1_rxdata), 
    .gt2_rxdata_out     (gt2_rxdata), 
    .gt3_rxdata_out     (gt3_rxdata),
    .gt4_rxdata_out     (gt4_rxdata), 
    .gt5_rxdata_out     (gt5_rxdata), 
    .gt6_rxdata_out     (gt6_rxdata), 
    .gt7_rxdata_out     (gt7_rxdata),
    .gt8_rxdata_out     (gt8_rxdata), 
    .gt9_rxdata_out     (gt9_rxdata), 
    .gt10_rxdata_out    (gt10_rxdata), 
    .gt11_rxdata_out    (gt11_rxdata),
    
// rx char is k
    .gt0_rxcharisk_out  (gt0_rxcharisk), 
    .gt1_rxcharisk_out  (gt1_rxcharisk), 
    .gt2_rxcharisk_out  (gt2_rxcharisk), 
    .gt3_rxcharisk_out  (gt3_rxcharisk),
    .gt4_rxcharisk_out  (gt4_rxcharisk), 
    .gt5_rxcharisk_out  (gt5_rxcharisk), 
    .gt6_rxcharisk_out  (gt6_rxcharisk), 
    .gt7_rxcharisk_out  (gt7_rxcharisk),
    .gt8_rxcharisk_out  (gt8_rxcharisk), 
    .gt9_rxcharisk_out  (gt9_rxcharisk), 
    .gt10_rxcharisk_out (gt10_rxcharisk), 
    .gt11_rxcharisk_out (gt11_rxcharisk),
    
// rx user clock
    .gt0_rxusrclk2_out  (gt0_rxusrclk), 
    .gt1_rxusrclk2_out  (gt1_rxusrclk), 
    .gt2_rxusrclk2_out  (gt2_rxusrclk), 
    .gt3_rxusrclk2_out  (gt3_rxusrclk),
    .gt4_rxusrclk2_out  (gt4_rxusrclk), 
    .gt5_rxusrclk2_out  (gt5_rxusrclk), 
    .gt6_rxusrclk2_out  (gt6_rxusrclk), 
    .gt7_rxusrclk2_out  (gt7_rxusrclk),
    .gt8_rxusrclk2_out  (gt8_rxusrclk), 
    .gt9_rxusrclk2_out  (gt9_rxusrclk), 
    .gt10_rxusrclk2_out (gt10_rxusrclk), 
    .gt11_rxusrclk2_out (gt11_rxusrclk),
    
    
    .TX_DISABLE0_out    (TX_DISABLE0),
    .TX_DISABLE1_out    (TX_DISABLE1),
    .TX_DISABLE2_out    (TX_DISABLE2),
    .loopback_mode_in   (loopback_mode),
    .eye0_data_out       (eye0_data_out),
    .eye1_data_out       (eye1_data_out),
    .eye2_data_out       (eye2_data_out),
    .eye3_data_out       (eye3_data_out),
    .eye4_data_out       (eye4_data_out),
    .eye5_data_out       (eye5_data_out),
    .eye6_data_out       (eye6_data_out),
    .eye7_data_out       (eye7_data_out),
    .eye8_data_out       (eye8_data_out),
    .eye9_data_out       (eye9_data_out),
    .eye10_data_out       (eye10_data_out),
    .eye11_data_out       (eye11_data_out),
    .eye0_data_count       (eye0_data_count),
    .eye1_data_count       (eye1_data_count),
    .eye2_data_count       (eye2_data_count),
    .eye3_data_count       (eye3_data_count),
    .eye4_data_count       (eye4_data_count),
    .eye5_data_count       (eye5_data_count),
    .eye6_data_count       (eye6_data_count),
    .eye7_data_count       (eye7_data_count),
    .eye8_data_count       (eye8_data_count),
    .eye9_data_count       (eye9_data_count),
    .eye10_data_count       (eye10_data_count),
    .eye11_data_count       (eye11_data_count),
    .eye0_state       (eye0_state),
    .eye1_state       (eye1_state),
    .eye2_state       (eye2_state),
    .eye3_state       (eye3_state),
    .eye4_state       (eye4_state),
    .eye5_state       (eye5_state),
    .eye6_state       (eye6_state),
    .eye7_state       (eye7_state),
    .eye8_state       (eye8_state),
    .eye9_state       (eye9_state),
    .eye10_state       (eye10_state),
    .eye11_state       (eye11_state),
    .eye_rd_clk         (Quartz),
    .eye_rd_en          (eye_rd_en),
    .eye_go             (eye_go),    
    .drpaddrReg     (drpaddrReg),
    .drpdiReg       (drpdiReg),
    .drpEnCmd       (drpEnCmd),
    .drpWeCmd       (drpWeCmd),
    .hstepSize      (hstepSize),
    .vstepSize      (vstepSize),
    .prbs_reset_in  (prbs_reset),
    .prbssel        (prbssel),
    .hstep_max      (hstep_max),
    .vstep_max      (vstep_max),
    .horz_init      (horz_init),
    .vert_init      (vert_init),
    .gt0_drpdo_out  (gt0_drpdo),
    .gt1_drpdo_out  (gt1_drpdo),
    .gt2_drpdo_out  (gt2_drpdo),
    .gt3_drpdo_out  (gt3_drpdo),
    .gt4_drpdo_out  (gt4_drpdo),
    .gt5_drpdo_out  (gt5_drpdo),
    .gt6_drpdo_out  (gt6_drpdo),
    .gt7_drpdo_out  (gt7_drpdo),
    .gt8_drpdo_out  (gt8_drpdo),
    .gt9_drpdo_out  (gt9_drpdo),
    .gt10_drpdo_out  (gt10_drpdo),
    .gt11_drpdo_out  (gt11_drpdo),
    .lpmen_in       (lpmen)
);

/*
// GTX Transmit Port (TX Port)
GTX_TX GTX_TX(
// 40MHz Clock in
    .CLK_in             (Quartz),
    .reset_in           (RESET_TX),
    .TX_reset_in        (TX_reset),
    .TX_Logic_reset_in  (TX_Logic_reset),

// tx user clock in
    .gt_txusrclk_in(gt_txusrclk2),

// TTC signals and ID information

// tx data out
    .gt0_txdata_out(gt0_txdata), .gt1_txdata_out(gt1_txdata), .gt2_txdata_out(gt2_txdata), .gt3_txdata_out(gt3_txdata),
    .gt4_txdata_out(gt4_txdata), .gt5_txdata_out(gt5_txdata), .gt6_txdata_out(gt6_txdata), .gt7_txdata_out(gt7_txdata),
    .gt8_txdata_out(gt8_txdata), .gt9_txdata_out(gt9_txdata), .gt10_txdata_out(gt10_txdata), .gt11_txdata_out(gt11_txdata),

// tx char is k ouot    
    .gt0_txcharisk_out(gt0_txcharisk), .gt1_txcharisk_out(gt1_txcharisk), .gt2_txcharisk_out(gt2_txcharisk), .gt3_txcharisk_out(gt3_txcharisk),
    .gt4_txcharisk_out(gt4_txcharisk), .gt5_txcharisk_out(gt5_txcharisk), .gt6_txcharisk_out(gt6_txcharisk), .gt7_txcharisk_out(gt7_txcharisk),
    .gt8_txcharisk_out(gt8_txcharisk), .gt9_txcharisk_out(gt9_txcharisk), .gt10_txcharisk_out(gt10_txcharisk), .gt11_txcharisk_out(gt11_txcharisk)
);
*/

BUFG Quartz_BUFG( .I(FPGA_CLK40), .O(Quartz));

Clock_Generator Clock_Generator(
    .FPGA_CLK_in     (FPGA_CLK),  // from TTC
    .FPGA_CLK40_in   (Quartz),// from Quarts
   
    .reset_in        (CLK_reset), 
    .TTC_CLK_out     (TTC_CLK),   // from FPGA_CLK
    .CLK_25_out      (CLK_25),    // from FPGA_CLK40
    .CLK_40_0_out    (CLK_40_0),    // from FPGA_CLK40
    .CLK_40_1_out    (CLK_40_1),    // from FPGA_CLK40
    .CLK_40_2_out    (CLK_40_2),    // from FPGA_CLK40
    .CLK_40_3_out    (CLK_40_3),    // from FPGA_CLK40
    .CLK_125_out     (CLK_125),   // from FPGA_CLK40
    .CLK_160_out     (CLK_160),   // from FPGA_CLK40
    .locked_out      (locked)
);


wire [12:0] wr_cnt;

GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink0_IFD (
.data_in_from_pins (GL0[20:0]),
.data_in_to_device (GL0_ifd[20:0]),
.data_in_to_device_all (GL0_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink0),
.io_reset          (1'b0)
);

GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink1_IFD (
.data_in_from_pins (GL1[20:0]),
.data_in_to_device (GL1_ifd[20:0]),
.data_in_to_device_all (GL1_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink1),
.io_reset          (1'b0)
);

GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink2_IFD (
.data_in_from_pins (GL2[20:0]),
.data_in_to_device (GL2_ifd[20:0]),
.data_in_to_device_all (GL2_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink2),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink3_IFD (
.data_in_from_pins (GL3[20:0]),
.data_in_to_device (GL3_ifd[20:0]),
.data_in_to_device_all (GL3_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink3),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink4_IFD (
.data_in_from_pins (GL4[20:0]),
.data_in_to_device (GL4_ifd[20:0]),
.data_in_to_device_all (GL4_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink4),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink5_IFD (
.data_in_from_pins (GL5[20:0]),
.data_in_to_device (GL5_ifd[20:0]),
.data_in_to_device_all (GL5_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink5),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink6_IFD (
.data_in_from_pins (GL6[20:0]),
.data_in_to_device (GL6_ifd[20:0]),
.data_in_to_device_all (GL6_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink6),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink7_IFD (
.data_in_from_pins (GL7[20:0]),
.data_in_to_device (GL7_ifd[20:0]),
.data_in_to_device_all (GL7_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink7),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink8_IFD (
.data_in_from_pins (GL8[20:0]),
.data_in_to_device (GL8_ifd[20:0]),
.data_in_to_device_all (GL8_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink8),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink9_IFD (
.data_in_from_pins (GL9[20:0]),
.data_in_to_device (GL9_ifd[20:0]),
.data_in_to_device_all (GL9_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink9),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink10_IFD (
.data_in_from_pins (GL10[20:0]),
.data_in_to_device (GL10_ifd[20:0]),
.data_in_to_device_all (GL10_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink10),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink11_IFD (
.data_in_from_pins (GL11[20:0]),
.data_in_to_device (GL11_ifd[20:0]),
.data_in_to_device_all (GL11_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink11),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink12_IFD (
.data_in_from_pins (GL12[20:0]),
.data_in_to_device (GL12_ifd[20:0]),
.data_in_to_device_all (GL12_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink12),
.io_reset          (1'b0)
);
GLink_IFD_TMDB #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCOMS33")
)
GLink13_IFD (
.data_in_from_pins (GL13[20:0]),
.data_in_to_device (GL13_ifd[20:0]),
.data_in_to_device_all (GL13_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (TTC_CLK),
.phase_in          (Phase_glink13),
.io_reset          (1'b0)
);

/*
// G-LINK phase adjustment
GLink_Delay GLink_Delay(
    .CLK_0_in (CLK_40_0),
    .CLK_1_in (CLK_40_1),
    .CLK_2_in (CLK_40_2),
    .CLK_3_in (CLK_40_3),
    .reset_in (RESET_40),
    .Delay_reset_in (Delay_reset),

    .glink0_data_in     (GL0_ifd),
    .glink1_data_in     (GL1_ifd),
    .glink2_data_in     (GL2_ifd),
    .glink3_data_in     (GL3_ifd),
    .glink4_data_in     (GL4_ifd),
    .glink5_data_in     (GL5_ifd),
    .glink6_data_in     (GL6_ifd),
    .glink7_data_in     (GL7_ifd),
    .glink8_data_in     (GL8_ifd),
    .glink9_data_in     (GL9_ifd),
    .glink10_data_in    (GL10_ifd),
    .glink11_data_in    (GL11_ifd),
    .glink12_data_in    (GL12_ifd),
    .glink13_data_in    (GL13_ifd),

    .glink0_phase_in   (Phase_glink0),
    .glink1_phase_in   (Phase_glink1),
    .glink2_phase_in   (Phase_glink2),
    .glink3_phase_in   (Phase_glink3),
    .glink4_phase_in   (Phase_glink4),
    .glink5_phase_in   (Phase_glink5),
    .glink6_phase_in   (Phase_glink6),
    .glink7_phase_in   (Phase_glink7),
    .glink8_phase_in   (Phase_glink8),
    .glink9_phase_in   (Phase_glink9),
    .glink10_phase_in  (Phase_glink10),
    .glink11_phase_in  (Phase_glink11),
    .glink12_phase_in  (Phase_glink12),
    .glink13_phase_in  (Phase_glink13),

    .glink0_delayed_out (glink0_delayed),
    .glink1_delayed_out (glink1_delayed),
    .glink2_delayed_out (glink2_delayed),
    .glink3_delayed_out (glink3_delayed),
    .glink4_delayed_out (glink4_delayed),
    .glink5_delayed_out (glink5_delayed),
    .glink6_delayed_out (glink6_delayed),
    .glink7_delayed_out (glink7_delayed),
    .glink8_delayed_out (glink8_delayed),
    .glink9_delayed_out (glink9_delayed),
    .glink10_delayed_out (glink10_delayed),
    .glink11_delayed_out (glink11_delayed),
    .glink12_delayed_out (glink12_delayed),
    .glink13_delayed_out (glink13_delayed),

    .glink0_phase_out   (Phase_glink0_mon),
    .glink1_phase_out   (Phase_glink1_mon),
    .glink2_phase_out   (Phase_glink2_mon),
    .glink3_phase_out   (Phase_glink3_mon),
    .glink4_phase_out   (Phase_glink4_mon),
    .glink5_phase_out   (Phase_glink5_mon),
    .glink6_phase_out   (Phase_glink6_mon),
    .glink7_phase_out   (Phase_glink7_mon),
    .glink8_phase_out   (Phase_glink8_mon),
    .glink9_phase_out   (Phase_glink9_mon),
    .glink10_phase_out  (Phase_glink10_mon),
    .glink11_phase_out  (Phase_glink11_mon),
    .glink12_phase_out  (Phase_glink12_mon),
    .glink13_phase_out  (Phase_glink13_mon)
    
);*/
// G-LINK Error Counter
    // select which lane to put in to FIFO

// G-LINK Lane Selector
wire [6:0] lane_selector;
assign glink_delayed_buf =  (lane_selector == 6'h0)  ? glink0_delayed[15:0] : 
                        (lane_selector == 6'h1)  ? glink1_delayed[15:0] : 
                        (lane_selector == 6'h2)  ? glink2_delayed[15:0] : 
                        (lane_selector == 6'h3)  ? glink3_delayed[15:0] : 
                        (lane_selector == 6'h4)  ? glink4_delayed[15:0] : 
                        (lane_selector == 6'h5)  ? glink5_delayed[15:0] : 
                        (lane_selector == 6'h6)  ? glink6_delayed[15:0] : 
                        (lane_selector == 6'h7)  ? glink7_delayed[15:0] : 
                        (lane_selector == 6'h8)  ? glink8_delayed[15:0] : 
                        (lane_selector == 6'h9)  ? glink9_delayed[15:0] : 
                        (lane_selector == 6'ha)  ? glink10_delayed[15:0] : 
                        (lane_selector == 6'hb)  ? glink11_delayed[15:0] : 
                        (lane_selector == 6'hc)  ? glink12_delayed[15:0] : 
                        (lane_selector == 6'hd)  ? glink13_delayed[15:0] : 
                        (lane_selector == 6'h10) ? GL0_ifd[15:0] : 
                        (lane_selector == 6'h11) ? GL1_ifd[15:0] : 
                        (lane_selector == 6'h12) ? GL2_ifd[15:0] : 
                        (lane_selector == 6'h13) ? GL3_ifd[15:0] : 
                        (lane_selector == 6'h14) ? GL4_ifd[15:0] : 
                        (lane_selector == 6'h15) ? GL5_ifd[15:0] : 
                        (lane_selector == 6'h16) ? GL6_ifd[15:0] : 
                        (lane_selector == 6'h17) ? GL7_ifd[15:0] : 
                        (lane_selector == 6'h18) ? GL8_ifd[15:0] : 
                        (lane_selector == 6'h19) ? GL9_ifd[15:0] : 
                        (lane_selector == 6'h1a) ? GL10_ifd[15:0] : 
                        (lane_selector == 6'h1b) ? GL11_ifd[15:0] : 
                        (lane_selector == 6'h1c) ? GL12_ifd[15:0] : 
                        (lane_selector == 6'h1d) ? GL13_ifd[15:0] : 
                        16'h0;

always@(posedge TTC_CLK) begin
    glink_delayed_reg <= glink_delayed_buf;
    glink0_delayed_reg <= {GL0_ifd_all[79:63],GL0_ifd_all[58:42],GL0_ifd_all[37:21],GL0_ifd_all[16:0]};
    glink1_delayed_reg <= {GL1_ifd_all[79:63],GL1_ifd_all[58:42],GL1_ifd_all[37:21],GL1_ifd_all[16:0]};
    glink2_delayed_reg <= {GL2_ifd_all[79:63],GL2_ifd_all[58:42],GL2_ifd_all[37:21],GL2_ifd_all[16:0]};
    glink3_delayed_reg <= {GL3_ifd_all[79:63],GL3_ifd_all[58:42],GL3_ifd_all[37:21],GL3_ifd_all[16:0]};
    glink4_delayed_reg <= {GL4_ifd_all[79:63],GL4_ifd_all[58:42],GL4_ifd_all[37:21],GL4_ifd_all[16:0]};
    glink5_delayed_reg <= {GL5_ifd_all[79:63],GL5_ifd_all[58:42],GL5_ifd_all[37:21],GL5_ifd_all[16:0]};
    glink6_delayed_reg <= {GL6_ifd_all[79:63],GL6_ifd_all[58:42],GL6_ifd_all[37:21],GL6_ifd_all[16:0]};
    glink7_delayed_reg <= {GL7_ifd_all[79:63],GL7_ifd_all[58:42],GL7_ifd_all[37:21],GL7_ifd_all[16:0]};
    glink8_delayed_reg <= {GL8_ifd_all[79:63],GL8_ifd_all[58:42],GL8_ifd_all[37:21],GL8_ifd_all[16:0]};
    glink9_delayed_reg <= {GL9_ifd_all[79:63],GL9_ifd_all[58:42],GL9_ifd_all[37:21],GL9_ifd_all[16:0]};
    glink10_delayed_reg <= {GL10_ifd_all[79:63],GL10_ifd_all[58:42],GL10_ifd_all[37:21],GL10_ifd_all[16:0]};
    glink11_delayed_reg <= {GL11_ifd_all[79:63],GL11_ifd_all[58:42],GL11_ifd_all[37:21],GL11_ifd_all[16:0]};
    glink12_delayed_reg <= {GL12_ifd_all[79:63],GL12_ifd_all[58:42],GL12_ifd_all[37:21],GL12_ifd_all[16:0]};
    glink13_delayed_reg <= {GL13_ifd_all[79:63],GL13_ifd_all[58:42],GL13_ifd_all[37:21],GL13_ifd_all[16:0]};
end
assign glink_delayed = glink_delayed_reg;
assign glink0_delayed_buf = glink0_delayed_reg;
assign glink1_delayed_buf = glink1_delayed_reg;
assign glink2_delayed_buf = glink2_delayed_reg;
assign glink3_delayed_buf = glink3_delayed_reg;
assign glink4_delayed_buf = glink4_delayed_reg;
assign glink5_delayed_buf = glink5_delayed_reg;
assign glink6_delayed_buf = glink6_delayed_reg;
assign glink7_delayed_buf = glink7_delayed_reg;
assign glink8_delayed_buf = glink8_delayed_reg;
assign glink9_delayed_buf = glink9_delayed_reg;
assign glink10_delayed_buf = glink10_delayed_reg;
assign glink11_delayed_buf = glink11_delayed_reg;
assign glink12_delayed_buf = glink12_delayed_reg;
assign glink13_delayed_buf = glink13_delayed_reg;


reg [31:0] detect_pattern_wire;
reg [31:0] detect_pattern_wire2;
reg [31:0] detect_pattern_strip;
reg data_flag;

always@(posedge TTC_CLK) begin
    if(Scaler_reset) begin
        detect_pattern_wire <= 32'b0;
        detect_pattern_wire2 <= 32'b0;
        detect_pattern_strip <= 32'b0;
        data_flag <= 1'b0;
    end
    
    else begin
    if((|detect_pattern_wire) == 1'b0) begin
        detect_pattern_wire    <=    (|glink0_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h0) ? {16'b0, glink0_delayed_buf[15:0]} :
                                     (|glink1_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h1) ? {16'b0, glink1_delayed_buf[14:0], glink0_delayed_buf[16]} :
                                     (|glink2_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h2) ? {16'b0, glink2_delayed_buf[13:0], glink1_delayed_buf[16:15]} :
                                     (|glink3_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h3) ? {16'b0, glink3_delayed_buf[12:0], glink2_delayed_buf[16:14]} :
                                     (|glink4_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h4) ? {16'b0, glink4_delayed_buf[11:0], glink3_delayed_buf[16:13]} :
                                     (|glink5_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h5) ? {16'b0, glink5_delayed_buf[10:0], glink4_delayed_buf[16:12]} :
                                     (|glink6_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h6) ? {16'b0, glink6_delayed_buf[9:0], glink5_delayed_buf[16:11]} :
                                     (|glink7_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h7) ? {16'b0, glink7_delayed_buf[8:0], glink6_delayed_buf[16:10]} :
                                     (|glink8_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h8) ? {16'b0, glink8_delayed_buf[7:0], glink7_delayed_buf[16:9]} :
                                     (|glink9_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h9) ? {16'b0, glink9_delayed_buf[6:0], glink8_delayed_buf[16:8]} :
                                     (|glink10_delayed_buf[16:0] == 1'b1 && lane_selector == 6'ha) ? {16'b0, glink10_delayed_buf[5:0], glink9_delayed_buf[16:7]} :
                                     (|glink11_delayed_buf[16:0] == 1'b1 && lane_selector == 6'hb) ? {16'b0, glink11_delayed_buf[4:0], glink10_delayed_buf[16:6]} :
                                     (|glink11_delayed_buf[16:0] == 1'b1 && lane_selector == 6'hc) ? {20'b0, glink10_delayed_buf[16:5]} :
                                     32'b0;
         data_flag <= 1'b1;
    end
    if (data_flag) begin
           detect_pattern_wire2    <=    (|glink0_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h0) ? {16'b0, glink0_delayed_buf[15:0]} :
                                  (|glink1_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h1) ? {16'b0, glink1_delayed_buf[14:0], glink0_delayed_buf[16]} :
                                  (|glink2_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h2) ? {16'b0, glink2_delayed_buf[13:0], glink1_delayed_buf[16:15]} :
                                  (|glink3_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h3) ? {16'b0, glink3_delayed_buf[12:0], glink2_delayed_buf[16:14]} :
                                  (|glink4_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h4) ? {16'b0, glink4_delayed_buf[11:0], glink3_delayed_buf[16:13]} :
                                  (|glink5_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h5) ? {16'b0, glink5_delayed_buf[10:0], glink4_delayed_buf[16:12]} :
                                  (|glink6_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h6) ? {16'b0, glink6_delayed_buf[9:0], glink5_delayed_buf[16:11]} :
                                  (|glink7_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h7) ? {16'b0, glink7_delayed_buf[8:0], glink6_delayed_buf[16:10]} :
                                  (|glink8_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h8) ? {16'b0, glink8_delayed_buf[7:0], glink7_delayed_buf[16:9]} :
                                  (|glink9_delayed_buf[16:0] == 1'b1 && lane_selector == 6'h9) ? {16'b0, glink9_delayed_buf[6:0], glink8_delayed_buf[16:8]} :
                                  (|glink10_delayed_buf[16:0] == 1'b1 && lane_selector == 6'ha) ? {16'b0, glink10_delayed_buf[5:0], glink9_delayed_buf[16:7]} :
                                  (|glink11_delayed_buf[16:0] == 1'b1 && lane_selector == 6'hb) ? {16'b0, glink11_delayed_buf[4:0], glink10_delayed_buf[16:6]} :
                                  (|glink11_delayed_buf[16:0] == 1'b1 && lane_selector == 6'hc) ? {20'b0, glink10_delayed_buf[16:5]} :
                                  32'b0;
            data_flag <=1'b0;
      end
    if(|detect_pattern_strip == 1'b0) begin
        detect_pattern_strip    <=    (|{glink5_delayed_buf[0], glink4_delayed_buf[16:0]} == 1'b1 && lane_selector == 6'h4) ?  {7'b0, glink5_delayed_buf[0], glink4_delayed_buf[16:9], 7'b0, glink4_delayed_buf[8:0]} :
                                      (|glink5_delayed_buf[16:1] == 1'b1 && lane_selector == 6'h5) ? {8'b0, glink5_delayed_buf[16:9], 8'b0, glink5_delayed_buf[8:1]} :
                                      (|{glink10_delayed_buf[0], glink10_delayed_buf[16:0]} == 1'b1 && lane_selector == 6'ha) ? {7'b0, glink11_delayed_buf[0], glink10_delayed_buf[16:9], 7'b0, glink10_delayed_buf[8:0]} :
                                      (|glink11_delayed_buf[16:1] == 1'b1 && lane_selector == 6'hb) ? {8'b0, glink11_delayed_buf[16:9], 8'b0, glink11_delayed_buf[8:1]} :
                                      32'b0;
                                      
    end
    end
end


assign pattern_selected1 = (pattern_selector1 == 2'h1) ? check_pattern1 :
                           (pattern_selector1 == 2'h2) ? check_pattern2 :
                           (pattern_selector1 == 2'h3) ? check_pattern3 :
                           check_pattern0 ;
assign pattern_selected2 = (pattern_selector2 == 2'h1) ? check_pattern1 :
                           (pattern_selector2 == 2'h2) ? check_pattern2 :
                           (pattern_selector2 == 2'h3) ? check_pattern3 :
                           check_pattern0 ;
    
//G-LINK Input Counter
GLink_Input_Monitor GLink_Input_Monitor(
    .CLK_IN      (CLK_40_0),
    .glink0_in        (glink0_delayed_buf[16:0]),
    .glink1_in        (glink1_delayed_buf[16:0]),
    .glink2_in        (glink2_delayed_buf[16:0]),
    .glink3_in        (glink3_delayed_buf[16:0]),
    .glink4_in        (glink4_delayed_buf[16:0]),
    .glink5_in        (glink5_delayed_buf[16:0]),
    .glink6_in        (glink6_delayed_buf[16:0]),
    .glink7_in        (glink7_delayed_buf[16:0]),
    .glink8_in        (glink8_delayed_buf[16:0]),
    .glink9_in        (glink9_delayed_buf[16:0]),
    .glink10_in       (glink10_delayed_buf[16:0]),
    .glink11_in       (glink11_delayed_buf[16:0]),
    .pattern1_in      (pattern_selected1[4:0]),
    .pattern2_in      (pattern_selected2[4:0]),
    .reset_in         (RESET_TTC),
    .counter_reset_in (Scaler_reset),
    .glink0_counter_out  (glink0_counter),
    .glink1_counter_out  (glink1_counter),
    .glink2_counter_out  (glink2_counter),
    .glink3_counter_out  (glink3_counter),
    .glink4_counter_out  (glink4_counter),
    .glink5_counter_out  (glink5_counter),
    .glink6_counter_out  (glink6_counter),
    .glink7_counter_out  (glink7_counter),
    .glink8_counter_out  (glink8_counter),
    .glink9_counter_out  (glink9_counter),
    .glink10_counter_out  (glink10_counter),
    .glink11_counter_out  (glink11_counter),
    .glink0_pattern_checker_out  (glink0_pattern_checker),
    .glink1_pattern_checker_out  (glink1_pattern_checker),
    .glink2_pattern_checker_out  (glink2_pattern_checker),
    .glink3_pattern_checker_out  (glink3_pattern_checker),
    .glink4_pattern_checker_out  (glink4_pattern_checker),
    .glink5_pattern_checker_out  (glink5_pattern_checker),
    .glink6_pattern_checker_out  (glink6_pattern_checker),
    .glink7_pattern_checker_out  (glink7_pattern_checker),
    .glink8_pattern_checker_out  (glink8_pattern_checker),
    .glink9_pattern_checker_out  (glink9_pattern_checker),
    .glink10_pattern_checker_out  (glink10_pattern_checker),
    .glink11_pattern_checker_out  (glink11_pattern_checker),
    .debug_flag (debug_flag),
    .stop_flag  (stop_flag)
);
    
    
reg TEST_PULSE_TRIGGER_flag;
reg [31:0] TEST_PULSE_TRIGGER_counter;
    
assign counter     =    (lane_selector == 6'h0)  ? glink0_counter[31:0] : 
                        (lane_selector == 6'h1)  ? glink1_counter[31:0] : 
                        (lane_selector == 6'h2)  ? glink2_counter[31:0] : 
                        (lane_selector == 6'h3)  ? glink3_counter[31:0] : 
                        (lane_selector == 6'h4)  ? glink4_counter[31:0] : 
                        (lane_selector == 6'h5)  ? glink5_counter[31:0] : 
                        (lane_selector == 6'h6)  ? glink6_counter[31:0] : 
                        (lane_selector == 6'h7)  ? glink7_counter[31:0] : 
                        (lane_selector == 6'h8)  ? glink8_counter[31:0] : 
                        (lane_selector == 6'h9)  ? glink9_counter[31:0] : 
                        (lane_selector == 6'ha)  ? glink10_counter[31:0] : 
                        (lane_selector == 6'hb)  ? glink11_counter[31:0] : 
                        TEST_PULSE_TRIGGER_counter[31:0];

assign checker     =    (lane_selector == 6'h0)  ? glink0_pattern_checker[31:0] : 
                        (lane_selector == 6'h1)  ? glink1_pattern_checker[31:0] : 
                        (lane_selector == 6'h2)  ? glink2_pattern_checker[31:0] : 
                        (lane_selector == 6'h3)  ? glink3_pattern_checker[31:0] : 
                        (lane_selector == 6'h4)  ? glink4_pattern_checker[31:0] : 
                        (lane_selector == 6'h5)  ? glink5_pattern_checker[31:0] : 
                        (lane_selector == 6'h6)  ? glink6_pattern_checker[31:0] : 
                        (lane_selector == 6'h7)  ? glink7_pattern_checker[31:0] : 
                        (lane_selector == 6'h8)  ? glink8_pattern_checker[31:0] : 
                        (lane_selector == 6'h9)  ? glink9_pattern_checker[31:0] : 
                        (lane_selector == 6'ha)  ? glink10_pattern_checker[31:0] : 
                        (lane_selector == 6'hb)  ? glink11_pattern_checker[31:0] : 
                        TEST_PULSE_TRIGGER_counter[31:0];




//G-LINK Error Counter
GLink_Error_Counter GLink_Error_Counter(
    .CLK_IN      (CLK_40_0),
    .glink0_in        (glink0_delayed_buf[67:0]),
    .glink1_in        (glink1_delayed_buf[67:0]),
    .glink2_in        (glink2_delayed_buf[67:0]),
    .glink3_in        (glink3_delayed_buf[67:0]),
    .glink4_in        (glink4_delayed_buf[67:0]),
    .glink5_in        (glink5_delayed_buf[67:0]),
    .glink6_in        (glink6_delayed_buf[67:0]),
    .glink7_in        (glink7_delayed_buf[67:0]),
    .glink8_in        (glink8_delayed_buf[67:0]),
    .glink9_in        (glink9_delayed_buf[67:0]),
    .glink10_in       (glink10_delayed_buf[67:0]),
    .glink11_in       (glink11_delayed_buf[67:0]),
    .glink12_in       (glink12_delayed_buf[67:0]),
    .glink13_in       (glink13_delayed_buf[67:0]),
    .glink0_pattern      (glink0_pattern[16:0]),
    .glink1_pattern      (glink1_pattern[16:0]),
    .glink2_pattern      (glink2_pattern[16:0]),
    .glink3_pattern      (glink3_pattern[16:0]),
    .glink4_pattern      (glink4_pattern[16:0]),
    .glink5_pattern      (glink5_pattern[16:0]),
    .glink6_pattern      (glink6_pattern[16:0]),
    .glink7_pattern      (glink7_pattern[16:0]),
    .glink8_pattern      (glink8_pattern[16:0]),
    .glink9_pattern      (glink9_pattern[16:0]),
    .glink10_pattern      (glink10_pattern[16:0]),
    .glink11_pattern      (glink11_pattern[16:0]),
    .glink12_pattern      (glink12_pattern[16:0]),
    .glink13_pattern      (glink13_pattern[16:0]),
    .reset_in         (RESET_TTC),
    .counter_reset_in (Scaler_reset),
    .glink0_error_counter_out_phase0  (glink0_error_counter_phase0),
    .glink1_error_counter_out_phase0  (glink1_error_counter_phase0),
    .glink2_error_counter_out_phase0  (glink2_error_counter_phase0),
    .glink3_error_counter_out_phase0  (glink3_error_counter_phase0),
    .glink4_error_counter_out_phase0  (glink4_error_counter_phase0),
    .glink5_error_counter_out_phase0  (glink5_error_counter_phase0),
    .glink6_error_counter_out_phase0  (glink6_error_counter_phase0),
    .glink7_error_counter_out_phase0  (glink7_error_counter_phase0),
    .glink8_error_counter_out_phase0  (glink8_error_counter_phase0),
    .glink9_error_counter_out_phase0  (glink9_error_counter_phase0),
    .glink10_error_counter_out_phase0  (glink10_error_counter_phase0),
    .glink11_error_counter_out_phase0  (glink11_error_counter_phase0),
    .glink12_error_counter_out_phase0  (glink12_error_counter_phase0),
    .glink13_error_counter_out_phase0  (glink13_error_counter_phase0),
    .glink0_error_counter_out_phase1  (glink0_error_counter_phase1),
    .glink1_error_counter_out_phase1  (glink1_error_counter_phase1),
    .glink2_error_counter_out_phase1  (glink2_error_counter_phase1),
    .glink3_error_counter_out_phase1  (glink3_error_counter_phase1),
    .glink4_error_counter_out_phase1  (glink4_error_counter_phase1),
    .glink5_error_counter_out_phase1  (glink5_error_counter_phase1),
    .glink6_error_counter_out_phase1  (glink6_error_counter_phase1),
    .glink7_error_counter_out_phase1  (glink7_error_counter_phase1),
    .glink8_error_counter_out_phase1  (glink8_error_counter_phase1),
    .glink9_error_counter_out_phase1  (glink9_error_counter_phase1),
    .glink10_error_counter_out_phase1  (glink10_error_counter_phase1),
    .glink11_error_counter_out_phase1  (glink11_error_counter_phase1),
    .glink12_error_counter_out_phase1  (glink12_error_counter_phase1),
    .glink13_error_counter_out_phase1  (glink13_error_counter_phase1),
    .glink0_error_counter_out_phase2  (glink0_error_counter_phase2),
    .glink1_error_counter_out_phase2  (glink1_error_counter_phase2),
    .glink2_error_counter_out_phase2  (glink2_error_counter_phase2),
    .glink3_error_counter_out_phase2  (glink3_error_counter_phase2),
    .glink4_error_counter_out_phase2  (glink4_error_counter_phase2),
    .glink5_error_counter_out_phase2  (glink5_error_counter_phase2),
    .glink6_error_counter_out_phase2  (glink6_error_counter_phase2),
    .glink7_error_counter_out_phase2  (glink7_error_counter_phase2),
    .glink8_error_counter_out_phase2  (glink8_error_counter_phase2),
    .glink9_error_counter_out_phase2  (glink9_error_counter_phase2),
    .glink10_error_counter_out_phase2  (glink10_error_counter_phase2),
    .glink11_error_counter_out_phase2  (glink11_error_counter_phase2),
    .glink12_error_counter_out_phase2  (glink12_error_counter_phase2),
    .glink13_error_counter_out_phase2  (glink13_error_counter_phase2),
    .glink0_error_counter_out_phase3  (glink0_error_counter_phase3),
    .glink1_error_counter_out_phase3  (glink1_error_counter_phase3),
    .glink2_error_counter_out_phase3  (glink2_error_counter_phase3),
    .glink3_error_counter_out_phase3  (glink3_error_counter_phase3),
    .glink4_error_counter_out_phase3  (glink4_error_counter_phase3),
    .glink5_error_counter_out_phase3  (glink5_error_counter_phase3),
    .glink6_error_counter_out_phase3  (glink6_error_counter_phase3),
    .glink7_error_counter_out_phase3  (glink7_error_counter_phase3),
    .glink8_error_counter_out_phase3  (glink8_error_counter_phase3),
    .glink9_error_counter_out_phase3  (glink9_error_counter_phase3),
    .glink10_error_counter_out_phase3  (glink10_error_counter_phase3),
    .glink11_error_counter_out_phase3  (glink11_error_counter_phase3),
    .glink12_error_counter_out_phase3  (glink12_error_counter_phase3),
    .glink13_error_counter_out_phase3  (glink13_error_counter_phase3)
);


reg non_zero_flag;
always@ (posedge TTC_CLK) begin
 if(|{glink11_delayed_buf[15:0],
               glink10_delayed_buf[15:0],
               glink9_delayed_buf[15:0],
               glink8_delayed_buf[15:0],
               glink7_delayed_buf[15:0],
               glink6_delayed_buf[15:0],
               glink5_delayed_buf[15:0],
               glink4_delayed_buf[15:0],
               glink3_delayed_buf[15:0],
               glink2_delayed_buf[15:0],
               glink1_delayed_buf[15:0],
               glink0_delayed_buf[15:0]}) begin
non_zero_flag = 1'b1;
end
else begin
non_zero_flag = 1'b0;
end
end
    

assign write_enb_selected = (signal_selector == 2'h1) ? TEST_PULSE_TRIGGER :
                            (signal_selector == 2'h2) ? non_zero_flag :
                            (signal_selector == 2'h3) ? TTC_CLK :
                            write_enb;
                            
                            

//wire wr_en = write_enb_selected;

wire [67:0] gl_4phase_input = (lane_selector == 4'h0) ? {1'b0,GL0_ifd_all[78:63],1'b0,GL0_ifd_all[57:42],1'b0,GL0_ifd_all[36:21],1'b0,GL0_ifd_all[15:0]} :
                               (lane_selector == 4'h1) ? {1'b0,GL1_ifd_all[78:63],1'b0,GL1_ifd_all[57:42],1'b0,GL1_ifd_all[36:21],1'b0,GL1_ifd_all[15:0]} :
                               (lane_selector == 4'h2) ? {1'b0,GL2_ifd_all[78:63],1'b0,GL2_ifd_all[57:42],1'b0,GL2_ifd_all[36:21],1'b0,GL2_ifd_all[15:0]} :
                               (lane_selector == 4'h3) ? {1'b0,GL3_ifd_all[78:63],1'b0,GL3_ifd_all[57:42],1'b0,GL3_ifd_all[36:21],1'b0,GL3_ifd_all[15:0]} :
                               (lane_selector == 4'h4) ? {1'b0,GL4_ifd_all[78:63],1'b0,GL4_ifd_all[57:42],1'b0,GL4_ifd_all[36:21],1'b0,GL4_ifd_all[15:0]} :
                               (lane_selector == 4'h5) ? {1'b0,GL5_ifd_all[78:63],1'b0,GL5_ifd_all[57:42],1'b0,GL5_ifd_all[36:21],1'b0,GL5_ifd_all[15:0]} :
                               (lane_selector == 4'h6) ? {1'b0,GL6_ifd_all[78:63],1'b0,GL6_ifd_all[57:42],1'b0,GL6_ifd_all[36:21],1'b0,GL6_ifd_all[15:0]} :
                               (lane_selector == 4'h7) ? {1'b0,GL7_ifd_all[78:63],1'b0,GL7_ifd_all[57:42],1'b0,GL7_ifd_all[36:21],1'b0,GL7_ifd_all[15:0]} :
                               (lane_selector == 4'h8) ? {1'b0,GL8_ifd_all[78:63],1'b0,GL8_ifd_all[57:42],1'b0,GL8_ifd_all[36:21],1'b0,GL8_ifd_all[15:0]} :
                               (lane_selector == 4'h9) ? {1'b0,GL9_ifd_all[78:63],1'b0,GL9_ifd_all[57:42],1'b0,GL9_ifd_all[36:21],1'b0,GL9_ifd_all[15:0]} :
                               (lane_selector == 4'ha) ? {1'b0,GL10_ifd_all[78:63],1'b0,GL10_ifd_all[57:42],1'b0,GL10_ifd_all[36:21],1'b0,GL10_ifd_all[15:0]} :
                               (lane_selector == 4'hb) ? {1'b0,GL11_ifd_all[78:63],1'b0,GL11_ifd_all[57:42],1'b0,GL11_ifd_all[36:21],1'b0,GL11_ifd_all[15:0]} :
                               (lane_selector == 4'hc) ? {1'b0,GL12_ifd_all[78:63],1'b0,GL12_ifd_all[57:42],1'b0,GL12_ifd_all[36:21],1'b0,GL12_ifd_all[15:0]} :
                               (lane_selector == 4'hd) ? {1'b0,GL13_ifd_all[78:63],1'b0,GL13_ifd_all[57:42],1'b0,GL13_ifd_all[36:21],1'b0,GL13_ifd_all[15:0]} :
                               {1'b0,GL0_ifd_all[78:63],1'b0,GL0_ifd_all[57:42],1'b0,GL0_ifd_all[36:21],1'b0,GL0_ifd_all[15:0]};
// G-LINK debug FIFO
Monitoring Monitoring(
    .wr_CLK_in      (CLK_40_0),
    .rd_CLK_in      (CLK_160),
    .data_in        ({85'b0,
                    1'b0, GL2_ifd[15:0],
                    1'b0, GL1_ifd[15:0],
                    1'b0,
                    GL0_ifd[15:0],
                    gl_4phase_input[67:0]}),
    .RST_in         (FIFO_reset),
    .write_enb      (write_enb_selected),
    .read_enb       (read_enb),   
    .wr_cnt         (wr_cnt),
    .FIFO_data_out  (FIFO_data),
    .control_flag   (control_flag),
    .debug_flag (debug_flag)
);


// G-LINK debug FIFO
debug debug(
    .wr_CLK_in      (CLK_40_0),
    .rd_CLK_in      (CLK_160),
/*    .data_in        ({glink11_delayed_buf[15:0],
                   glink10_delayed_buf[15:0],
                   glink9_delayed_buf[15:0],
                   glink8_delayed_buf[15:0],
                   glink7_delayed_buf[15:0],
                   glink6_delayed_buf[15:0],
                   glink5_delayed_buf[15:0],
                   glink4_delayed_buf[15:0],
                   glink3_delayed_buf[15:0],
                   glink2_delayed_buf[15:0],
                   glink1_delayed_buf[15:0],
                   glink0_delayed_buf[15:0]}),
                   */
    .data_in        ({            glink4_pattern_checker[31:0],
                                  glink0_pattern_checker[31:0],
                                  glink4_counter[31:0],
                                  glink0_counter[31:0],
                                  glink4_error_counter_phase0[31:0],
                                  glink0_error_counter_phase0[31:0]}),
    .RST_in         (FIFO_reset),
    .write_enb      (non_zero_flag),
    .read_enb       (read_enb),   
    .FIFO_data_out  (debug_FIFO_data),
    .debug_flag (debug_flag)
);

// TTC signal assignment
    assign BUSY = 1'b1;
    // assign VME output, to BUSY, probably for 1 clock per posedge

// TTC signal counter
reg [15:0] L1A_counter;
reg [15:0] BCR_counter;
reg [15:0] ECR_counter;
reg [15:0] TTC_RESET_counter;
reg [40:0] TTC_counter;

//  Test Flags
reg L1A_flag;
reg BCR_flag;
reg ECR_flag;
reg TTC_RESET_flag;

always@ (posedge TTC_CLK) begin
    if(Scaler_reset||RESET_TTC) begin
        L1A_counter = 0;
        BCR_counter = 0;
        ECR_counter = 0;
        TTC_counter = 0;
        TTC_RESET_counter = 0;
        TEST_PULSE_TRIGGER_counter = 0;
    end
    else if (TTC_counter <= 40'ha000000000)begin
        TTC_counter <= TTC_counter + 1;
        if(L1A == 1) begin
            L1A_counter <= L1A_counter + 1;
        end
        if(BCR == 1 ) begin
            BCR_counter <= BCR_counter + 1;
        end
        if(ECR == 1 ) begin
            ECR_counter <= ECR_counter + 1;
        end
        if(TTC_RESET == 1 ) begin
            TTC_RESET_counter <= TTC_RESET_counter + 1;
        end
        if(TEST_PULSE_TRIGGER == 1) begin
            TEST_PULSE_TRIGGER_counter <= TEST_PULSE_TRIGGER_counter + 1;
        end
     end
end

// NIM output assignment
// assign VME output, to 4 channels individually

assign NIMOUT_0 = (NIM_OUT[4]) ? CLK_40_0 : NIM_OUT[0];
assign NIMOUT_1 = (NIM_OUT[5]) ? CLK_40_1 : NIM_OUT[1];
assign NIMOUT_2 = (NIM_OUT[6]) ? CLK_40_2 : NIM_OUT[2];
assign NIMOUT_3 = (NIM_OUT[7]) ? CLK_40_3 : NIM_OUT[3];

wire SiTCP_wr_en = glink_delayed[15:12] == 4'hf;
wire [15:0] EXT_IP1;
wire [15:0] EXT_IP2;


// SiTCP readout port
/*
SiTCP SiTCP(
    .EXT_IP ({EXT_IP1, EXT_IP2}),
    
    .CLK_25_in          (CLK_25),
    .CLK_125_in         (CLK_125),
    .rd_clk_in          (CLK_160),   //160 MHz
    .SiTCP_wr_in        (SiTCP_wr_en),
    .data_in            (glink_delayed),
    .SiTCP_full_out     (SiTCP_full),
    .SiTCP_reset_in     (SiTCP_reset),
    .reset_in           (RESET_160),
    .SiTCP_FIFO_reset_in(SiTCP_FIFO_reset),
    .ETH_TXCLK          (ETH_TXCLK),
    .ETH_CLKIN          (ETH_CLKIN),
    .ETH_GTXCLK         (ETH_GTXCLK),
    .ETH_RESET_B        (ETH_RESET_B),
    .ETH_CRS            (ETH_CRS),
    .ETH_COL            (ETH_COL),
    .ETH_TXD            (ETH_TXD),
    .ETH_TXEN           (ETH_TXEN),
    .ETH_TXER           (ETH_TXER),
    .ETH_RXCLK          (ETH_RXCLK),
    .ETH_RXD            (ETH_RXD),
    .ETH_RXDV           (ETH_RXDV),
    .ETH_RXER           (ETH_RXER),
    .ETH_MDC            (ETH_MDC),
    .ETH_MDIO           (ETH_MDIO),
    .ETH_HPD            (ETH_HPD),
    .PROM_CS            (PROM_CS),
    .PROM_SK            (PROM_SK),
    .PROM_DI            (PROM_DI),
    .PROM_DO            (PROM_DO),
    .SiTCP_data_count   (SiTCP_data_count),
    .SiTCP_busy         (SiTCP_busy)
);
*/


// VME Module
vme vme(

    .EXT_IP1 (EXT_IP1),
    .EXT_IP2 (EXT_IP2),

    .TTC_CLK            (TTC_CLK),
    .CLK_40             (Quartz),
    .CLK_160            (CLK_160),
    .TXUSRCLK_in        (gt_txusrclk2),
    .OE                 (OE),
    .CE                 (CE),
    .WE                 (WE),
    .VME_A              (VME_A[11:0]),
    .VME_D              (VME_D[15:0]),
    
//  Reset       
    .Reset_TTC_out          (RESET_TTC),
    .Reset_40_out           (RESET_40),
    .Reset_160_out          (RESET_160),
    .GTX_TX_reset_out       (TX_reset),
    .GTX_RX_reset_out       (RX_reset),
    .Scaler_reset_out       (Scaler_reset),
    .Delay_reset_out        (Delay_reset),
    .CLK_reset_out          (CLK_reset),
    .FIFO_reset_out         (FIFO_reset),
    .SiTCP_reset_out        (SiTCP_reset),
    .SiTCP_FIFO_reset_out   (SiTCP_FIFO_reset),
//    .Monitoring_reset_out   (Monitoring_reset),
//    .LUT_init_reset_out     (LUT_init_reset),
    .TX_Logic_reset_out     (TX_Logic_reset),
    .gt0_rx_reset_out       (gt0_rx_reset),
    .gt1_rx_reset_out       (gt1_rx_reset),
    .gt2_rx_reset_out       (gt2_rx_reset),
    .gt3_rx_reset_out       (gt3_rx_reset),
    .gt4_rx_reset_out       (gt4_rx_reset),
    .gt5_rx_reset_out       (gt5_rx_reset),
    .gt6_rx_reset_out       (gt6_rx_reset),
    .gt7_rx_reset_out       (gt7_rx_reset),
    .gt8_rx_reset_out       (gt8_rx_reset),
    .gt9_rx_reset_out       (gt9_rx_reset),
    .gt10_rx_reset_out      (gt10_rx_reset),
    .gt11_rx_reset_out      (gt11_rx_reset),
 /*   
// GTX status
    .Error_Scaler_gt_0     (gt0_error_scaler),
    .Error_Scaler_gt_1     (gt1_error_scaler),
    .Error_Scaler_gt_2     (gt2_error_scaler),
    .Error_Scaler_gt_3     (gt3_error_scaler),
    .Error_Scaler_gt_4     (gt4_error_scaler),
    .Error_Scaler_gt_5     (gt5_error_scaler),
    .Error_Scaler_gt_6     (gt6_error_scaler),
    .Error_Scaler_gt_7     (gt7_error_scaler),
    .Error_Scaler_gt_8     (gt8_error_scaler),
    .Error_Scaler_gt_9     (gt9_error_scaler),
    .Error_Scaler_gt_10    (gt10_error_scaler),
    .Error_Scaler_gt_11    (gt11_error_scaler),

    .Status_gt_0         ({10'h0, gt0_status}),
    .Status_gt_1         ({10'h0, gt1_status}),    
    .Status_gt_2         ({10'h0, gt2_status}),
    .Status_gt_3         ({10'h0, gt3_status}),
    .Status_gt_4         ({10'h0, gt4_status}),
    .Status_gt_5         ({10'h0, gt5_status}),
    .Status_gt_6         ({10'h0, gt6_status}),
    .Status_gt_7         ({10'h0, gt7_status}),
    .Status_gt_8         ({10'h0, gt8_status}),
    .Status_gt_9         ({10'h0, gt9_status}),
    .Status_gt_10         ({10'h0, gt10_status}),
    .Status_gt_11         ({10'h0, gt11_status}),*/

// Delay
    .Phase_glink0         (Phase_glink0),
    .Phase_glink1         (Phase_glink1),
    .Phase_glink2         (Phase_glink2),
    .Phase_glink3         (Phase_glink3),
    .Phase_glink4         (Phase_glink4),
    .Phase_glink5         (Phase_glink5),
    .Phase_glink6         (Phase_glink6),
    .Phase_glink7         (Phase_glink7),
    .Phase_glink8         (Phase_glink8),
    .Phase_glink9         (Phase_glink9),
    .Phase_glink10        (Phase_glink10),
    .Phase_glink11        (Phase_glink11),
    .Phase_glink12        (Phase_glink12),
    .Phase_glink13        (Phase_glink13),
    
    .Phase_glink0_mon     (Phase_glink0_mon),
    .Phase_glink1_mon     (Phase_glink1_mon),
    .Phase_glink2_mon     (Phase_glink2_mon),
    .Phase_glink3_mon     (Phase_glink3_mon),
    .Phase_glink4_mon     (Phase_glink4_mon),
    .Phase_glink5_mon     (Phase_glink5_mon),
    .Phase_glink6_mon     (Phase_glink6_mon),
    .Phase_glink7_mon     (Phase_glink7_mon),
    .Phase_glink8_mon     (Phase_glink8_mon),
    .Phase_glink9_mon     (Phase_glink9_mon),
    .Phase_glink10_mon    (Phase_glink10_mon),
    .Phase_glink11_mon    (Phase_glink11_mon),
    .Phase_glink12_mon    (Phase_glink12_mon),
    .Phase_glink13_mon    (Phase_glink13_mon),
    
        .detect_pattern_wire  (detect_pattern_wire),
        .detect_pattern_wire2  (detect_pattern_wire2),
    .detect_pattern_strip  (detect_pattern_strip),
    
    .counter              (counter),
    .checker              (checker),
    
    .glink0_counter       (glink0_counter),
    .glink1_counter       (glink1_counter),
    .glink2_counter       (glink2_counter),
    .glink3_counter       (glink3_counter),
    .glink4_counter       (glink4_counter),
    .glink5_counter       (glink5_counter),
    .glink6_counter       (glink6_counter),
    .glink7_counter       (glink7_counter),
    .glink8_counter       (glink8_counter),
    .glink9_counter       (glink9_counter),
    .glink10_counter      (glink10_counter),
    .glink11_counter      (glink11_counter),

    .glink0_checker       (glink0_pattern_checker),
    .glink1_checker       (glink1_pattern_checker),
    .glink2_checker       (glink2_pattern_checker),
    .glink3_checker       (glink3_pattern_checker),
    .glink4_checker       (glink4_pattern_checker),
    .glink5_checker       (glink5_pattern_checker),
    .glink6_checker       (glink6_pattern_checker),
    .glink7_checker       (glink7_pattern_checker),
    .glink8_checker       (glink8_pattern_checker),
    .glink9_checker       (glink9_pattern_checker),
    .glink10_checker       (glink10_pattern_checker),
    .glink11_checker       (glink11_pattern_checker),
    
    .pattern_selector1      (pattern_selector1),
    .pattern_selector2      (pattern_selector2),
    .check_pattern0         (check_pattern0),
    .check_pattern1         (check_pattern1),
    .check_pattern2         (check_pattern2),
    .check_pattern3         (check_pattern3),
    
    .glink_pattern          (glink_pattern),
    .glink0_pattern          (glink0_pattern),
    .glink1_pattern          (glink1_pattern),
    .glink2_pattern          (glink2_pattern),
    .glink3_pattern          (glink3_pattern),
    .glink4_pattern          (glink4_pattern),
    .glink5_pattern          (glink5_pattern),
    .glink6_pattern          (glink6_pattern),
    .glink7_pattern          (glink7_pattern),
    .glink8_pattern          (glink8_pattern),
    .glink9_pattern          (glink9_pattern),
    .glink10_pattern          (glink10_pattern),
    .glink11_pattern          (glink11_pattern),
    .glink12_pattern          (glink12_pattern),
    .glink13_pattern          (glink13_pattern),
    .glink0_error_counter_phase0   (glink0_error_counter_phase0),
    .glink1_error_counter_phase0   (glink1_error_counter_phase0),
    .glink2_error_counter_phase0   (glink2_error_counter_phase0),
    .glink3_error_counter_phase0   (glink3_error_counter_phase0),
    .glink4_error_counter_phase0   (glink4_error_counter_phase0),
    .glink5_error_counter_phase0   (glink5_error_counter_phase0),
    .glink6_error_counter_phase0   (glink6_error_counter_phase0),
    .glink7_error_counter_phase0   (glink7_error_counter_phase0),
    .glink8_error_counter_phase0   (glink8_error_counter_phase0),
    .glink9_error_counter_phase0   (glink9_error_counter_phase0),
    .glink10_error_counter_phase0  (glink10_error_counter_phase0),
    .glink11_error_counter_phase0  (glink11_error_counter_phase0),
    .glink12_error_counter_phase0  (glink12_error_counter_phase0),
    .glink13_error_counter_phase0  (glink13_error_counter_phase0),
    .glink0_error_counter_phase1   (glink0_error_counter_phase1),
    .glink1_error_counter_phase1   (glink1_error_counter_phase1),
    .glink2_error_counter_phase1   (glink2_error_counter_phase1),
    .glink3_error_counter_phase1   (glink3_error_counter_phase1),
    .glink4_error_counter_phase1   (glink4_error_counter_phase1),
    .glink5_error_counter_phase1   (glink5_error_counter_phase1),
    .glink6_error_counter_phase1   (glink6_error_counter_phase1),
    .glink7_error_counter_phase1   (glink7_error_counter_phase1),
    .glink8_error_counter_phase1   (glink8_error_counter_phase1),
    .glink9_error_counter_phase1   (glink9_error_counter_phase1),
    .glink10_error_counter_phase1  (glink10_error_counter_phase1),
    .glink11_error_counter_phase1  (glink11_error_counter_phase1),
    .glink12_error_counter_phase1  (glink12_error_counter_phase1),
    .glink13_error_counter_phase1  (glink13_error_counter_phase1),
    .glink0_error_counter_phase2   (glink0_error_counter_phase2),
    .glink1_error_counter_phase2   (glink1_error_counter_phase2),
    .glink2_error_counter_phase2   (glink2_error_counter_phase2),
    .glink3_error_counter_phase2   (glink3_error_counter_phase2),
    .glink4_error_counter_phase2   (glink4_error_counter_phase2),
    .glink5_error_counter_phase2   (glink5_error_counter_phase2),
    .glink6_error_counter_phase2   (glink6_error_counter_phase2),
    .glink7_error_counter_phase2   (glink7_error_counter_phase2),
    .glink8_error_counter_phase2   (glink8_error_counter_phase2),
    .glink9_error_counter_phase2   (glink9_error_counter_phase2),
    .glink10_error_counter_phase2  (glink10_error_counter_phase2),
    .glink11_error_counter_phase2  (glink11_error_counter_phase2),
    .glink12_error_counter_phase2  (glink12_error_counter_phase2),
    .glink13_error_counter_phase2  (glink13_error_counter_phase2),
    .glink0_error_counter_phase3   (glink0_error_counter_phase3),
    .glink1_error_counter_phase3   (glink1_error_counter_phase3),
    .glink2_error_counter_phase3   (glink2_error_counter_phase3),
    .glink3_error_counter_phase3   (glink3_error_counter_phase3),
    .glink4_error_counter_phase3   (glink4_error_counter_phase3),
    .glink5_error_counter_phase3   (glink5_error_counter_phase3),
    .glink6_error_counter_phase3   (glink6_error_counter_phase3),
    .glink7_error_counter_phase3   (glink7_error_counter_phase3),
    .glink8_error_counter_phase3   (glink8_error_counter_phase3),
    .glink9_error_counter_phase3   (glink9_error_counter_phase3),
    .glink10_error_counter_phase3  (glink10_error_counter_phase3),
    .glink11_error_counter_phase3  (glink11_error_counter_phase3),
    .glink12_error_counter_phase3  (glink12_error_counter_phase3),
    .glink13_error_counter_phase3  (glink13_error_counter_phase3),


    .monitoring_FIFO_out_0        (FIFO_data[15:0]),
    .monitoring_FIFO_out_1        ({15'b0, FIFO_data[16]}), 
    .monitoring_FIFO_out_2        (FIFO_data[32:17]), 
    .monitoring_FIFO_out_3        ({15'b0, FIFO_data[33]}), 
    .monitoring_FIFO_out_4        (FIFO_data[49:34]), 
    .monitoring_FIFO_out_5        ({15'b0, FIFO_data[50]}), 
    .monitoring_FIFO_out_6        (FIFO_data[66:51]),
    .monitoring_FIFO_out_7        ({15'b0, FIFO_data[67]}),
    .monitoring_FIFO_out_8        (FIFO_data[83:68]),
    .monitoring_FIFO_out_9        ({15'b0, FIFO_data[84]}),
    .monitoring_FIFO_out_10       (FIFO_data[100:85]),
    .monitoring_FIFO_out_11       ({15'b0, FIFO_data[101]}),
    .monitoring_FIFO_out_12        (FIFO_data[117:102]),
    .monitoring_FIFO_out_13       ({15'b0, FIFO_data[118]}), 
    .monitoring_FIFO_out_14        (FIFO_data[134:119]), 
    .monitoring_FIFO_out_15        ({15'b0, FIFO_data[135]}), 
    .monitoring_FIFO_out_16        (FIFO_data[151:136]), 
    .monitoring_FIFO_out_17        ({15'b0, FIFO_data[152]}), 
    .monitoring_FIFO_out_18        (FIFO_data[168:153]),
    .monitoring_FIFO_out_19        ({15'b0, FIFO_data[169]}),
    .monitoring_FIFO_out_20        (FIFO_data[185:170]),
    .monitoring_FIFO_out_21       ({15'b0, FIFO_data[186]}),
    .monitoring_FIFO_out_22       (FIFO_data[202:187]),
    .monitoring_FIFO_out_23       ({15'b0, FIFO_data[203]}),
 //   .monitoring_FIFO_out_13       (FIFO_data[223:208]),
  //  .monitoring_FIFO_out_14       (FIFO_data[239:224]),
   // .monitoring_FIFO_out_15       (FIFO_data[255:240]),

    .debug_monitoring_FIFO_out_0        (debug_FIFO_data[15:0]),
    .debug_monitoring_FIFO_out_1        (debug_FIFO_data[31:16]), 
    .debug_monitoring_FIFO_out_2        (debug_FIFO_data[47:32]), 
    .debug_monitoring_FIFO_out_3        (debug_FIFO_data[63:48]), 
    .debug_monitoring_FIFO_out_4        (debug_FIFO_data[79:64]), 
    .debug_monitoring_FIFO_out_5        (debug_FIFO_data[95:80]), 
    .debug_monitoring_FIFO_out_6        (debug_FIFO_data[111:96]),
    .debug_monitoring_FIFO_out_7        (debug_FIFO_data[127:112]),
    .debug_monitoring_FIFO_out_8        (debug_FIFO_data[143:128]),
    .debug_monitoring_FIFO_out_9        (debug_FIFO_data[159:144]),
    .debug_monitoring_FIFO_out_10       (debug_FIFO_data[175:160]),
    .debug_monitoring_FIFO_out_11       (debug_FIFO_data[191:176]),
    
    .monitoring_FIFO_wr_en     (write_enb),
    .monitoring_FIFO_rd_en     (read_enb),
    .monitoring_wr_cnt          (wr_cnt),
    .control_flag               (control_flag),
        
    .signal_selector_out        (signal_selector),    
    .lane_selector_out      (lane_selector),    
    .SiTCP_Count            ({3'b0,SiTCP_data_count}),
    .L1A_counter(L1A_counter),
    .BCR_counter(BCR_counter),
    .ECR_counter(ECR_counter),
    .TTC_RESET_counter(TTC_RESET_counter),
    .TEST_PULSE_counter(TEST_PULSE_TRIGGER_counter),
    
    .NIM_OUT(NIM_OUT),
    .eye0_data           (eye0_data_out),
    .eye1_data           (eye1_data_out),
    .eye2_data           (eye2_data_out),
    .eye3_data           (eye3_data_out),
    .eye4_data           (eye4_data_out),
    .eye5_data           (eye5_data_out),
    .eye6_data           (eye6_data_out),
    .eye7_data           (eye7_data_out),
    .eye8_data           (eye8_data_out),
    .eye9_data           (eye9_data_out),
    .eye10_data           (eye10_data_out),
    .eye11_data           (eye11_data_out),
    .eye_rd_en          (eye_rd_en),
    .eye_go             (eye_go),
    .drpaddrReg     (drpaddrReg),
    .drpdiReg       (drpdiReg),
    .drpEnCmd       (drpEnCmd),
    .drpWeCmd       (drpWeCmd),
    .eye0_data_count     (eye0_data_count),
    .eye1_data_count     (eye1_data_count),
    .eye2_data_count     (eye2_data_count),
    .eye3_data_count     (eye3_data_count),
    .eye4_data_count     (eye4_data_count),
    .eye5_data_count     (eye5_data_count),
    .eye6_data_count     (eye6_data_count),
    .eye7_data_count     (eye7_data_count),
    .eye8_data_count     (eye8_data_count),
    .eye9_data_count     (eye9_data_count),
    .eye10_data_count     (eye10_data_count),
    .eye11_data_count     (eye11_data_count),
    .eye0_state     (eye0_state),
    .eye1_state     (eye1_state),
    .eye2_state     (eye2_state),
    .eye3_state     (eye3_state),
    .eye4_state     (eye4_state),
    .eye5_state     (eye5_state),
    .eye6_state     (eye6_state),
    .eye7_state     (eye7_state),
    .eye8_state     (eye8_state),
    .eye9_state     (eye9_state),
    .eye10_state     (eye10_state),
    .eye11_state     (eye11_state),
    .hstepSize          (hstepSize),
    .vstepSize          (vstepSize),
    .prbs_reset         (prbs_reset),
    .prbssel            (prbssel),
    .hstep_max      (hstep_max),
    .vstep_max      (vstep_max),
    .horz_init      (horz_init),
    .vert_init      (vert_init),
    .gt0_drpdo      (gt0_drpdo),
    .gt1_drpdo      (gt1_drpdo),
    .gt2_drpdo      (gt2_drpdo),
    .gt3_drpdo      (gt3_drpdo),
    .gt4_drpdo      (gt4_drpdo),
    .gt5_drpdo      (gt5_drpdo),
    .gt6_drpdo      (gt6_drpdo),
    .gt7_drpdo      (gt7_drpdo),
    .gt8_drpdo      (gt8_drpdo),
    .gt9_drpdo      (gt9_drpdo),
    .gt10_drpdo      (gt10_drpdo),
    .gt11_drpdo      (gt11_drpdo),
    .lpmen             (lpmen)
    );  



endmodule
