`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/01 17:21:02
// Design Name: 
// Module Name: Error_Counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Error_Counter(
    input wire CLK_in,
    input wire reset_in,
    input wire [16:0] data_in,
    input wire [16:0] pattern_in,
    output reg [31:0] count_out
    );

reg data_flag;
reg [16:0] data_reg;
reg [40:0] count_range;

always @(posedge CLK_in) begin
    if (reset_in) begin
        count_out[31:0] <= 32'b0;
        data_flag <= 1'b0;
        count_range[40:0] <= 41'b0;
    end
    else if(count_range[31:0] <= 32'ha0000000) begin
        
        //data input
        if ((|data_in) == 1'b1) begin
            if(data_in != pattern_in) begin
                count_out[31:0] <= count_out[31:0] + 32'b1;
            end
            if(data_reg != 17'b0) begin
                count_out[31:0] <= count_out[31:0] + 32'b1;
            end
            data_flag <= 1'b1;        
            count_range <= count_range + 40'b10;
        end
        //after data
        if(data_flag) begin
            if(data_in != 17'b0) begin
                count_out[31:0] <= count_out[31:0] + 32'b1;
            end
            data_flag <= 1'b0;        
            count_range <= count_range + 40'b1;
        end
        
        data_reg <= data_in;
        
    end
end



endmodule
