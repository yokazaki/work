`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 00:23:43
// Design Name: 
// Module Name: GTX_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_TX(
    input wire          CLK_in,    
    input wire          reset_in,
    input wire          TX_reset_in,
    input wire          TX_Logic_reset_in,

    input wire          gt_txusrclk_in,

    output reg [31:0]   gt0_txdata_out,
    output reg [31:0]   gt1_txdata_out,
    output reg [31:0]   gt2_txdata_out,
    output reg [31:0]   gt3_txdata_out,
    output reg [31:0]   gt4_txdata_out,
    output reg [31:0]   gt5_txdata_out,
    output reg [31:0]   gt6_txdata_out,
    output reg [31:0]   gt7_txdata_out,
    output reg [31:0]   gt8_txdata_out,
    output reg [31:0]   gt9_txdata_out,
    output reg [31:0]   gt10_txdata_out,
    output reg [31:0]   gt11_txdata_out,

    output reg [3:0]    gt0_txcharisk_out,
    output reg [3:0]    gt1_txcharisk_out,
    output reg [3:0]    gt2_txcharisk_out,
    output reg [3:0]    gt3_txcharisk_out,
    output reg [3:0]    gt4_txcharisk_out,
    output reg [3:0]    gt5_txcharisk_out,
    output reg [3:0]    gt6_txcharisk_out,
    output reg [3:0]    gt7_txcharisk_out,
    output reg [3:0]    gt8_txcharisk_out,
    output reg [3:0]    gt9_txcharisk_out,
    output reg [3:0]    gt10_txcharisk_out,
    output reg [3:0]    gt11_txcharisk_out
    );
    
wire RST_in = reset_in||TX_reset_in||TX_Logic_reset_in;
    
parameter PAUSE = 5'b1;
parameter HEADER = 5'b10;
parameter TRACK1 = 5'b100;
parameter TRACK2 = 5'b1000;
parameter FOOTER = 5'b10000;
    
// tmp
///// TX LOGIC ////////
       
reg [4:0] TX0_state = 5'b1;
reg [4:0] TX10_state = 5'b1;
reg TX0_CLK_is_high = 1'b1;
reg TX10_CLK_is_high = 1'b1;
    
//reg [15:0] TXcounterall = 16'h0;
//wire [15:0] TXcounterall_w;
//reg [15:0] TXcounter = 16'h0;
//reg [15:0] TXcounter80 = 16'h0;

//assign TXcounterall_w = TXcounterall;
    
    always @(posedge gt_txusrclk_in or posedge RST_in) begin
       //TXcounterall <= TXcounterall + 16'b1; //for loopback test
       //TXcounter <= TXcounterall_w; // for loopback test
        if (RST_in) begin
            TX0_state <= PAUSE;
            gt0_txdata_out <= 32'hbcbc;
            gt0_txcharisk_out <= 4'h1;
            gt1_txdata_out <= 32'hbcbc;
            gt1_txcharisk_out <= 4'h1;
            gt2_txdata_out <= 32'hbcbc;
            gt2_txcharisk_out <= 4'h1;
            gt3_txdata_out <= 32'hbcbc;
            gt3_txcharisk_out <= 4'h1;
            gt4_txdata_out <= 32'hbcbc;
            gt4_txcharisk_out <= 4'h1;
            gt5_txdata_out <= 32'hbcbc;
            gt5_txcharisk_out <= 4'h1;
            gt6_txdata_out <= 32'hbcbc;
            gt6_txcharisk_out <= 4'h1;
            gt7_txdata_out <= 32'hbcbc;
            gt7_txcharisk_out <= 4'h1;
            gt8_txdata_out <= 32'hbcbc;
            gt8_txcharisk_out <= 4'h1;
            gt9_txdata_out <= 32'hbcbc;
            gt9_txcharisk_out <= 4'h1;
            gt10_txdata_out <= 32'hbcbc;
            gt10_txcharisk_out <= 4'h1;
            gt11_txdata_out <= 32'hbcbc;
            gt11_txcharisk_out <= 4'h1;
            TX0_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX0_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX0_CLK_is_high <= 1'b0;
                        TX0_state <= PAUSE;
                    end
                    else if(CLK_in && !TX0_CLK_is_high) begin
                        TX0_CLK_is_high <= 1'b1;                        
                        TX0_state <= HEADER;
                    end
                    else begin
                        TX0_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    gt0_txdata_out <= 32'b0;
                    gt0_txcharisk_out <= 4'b0;
                    gt1_txdata_out <= 32'b0;
                    gt1_txcharisk_out <= 4'b0;
                    gt2_txdata_out <= 32'b0;
                    gt2_txcharisk_out <= 4'b0;
                    gt3_txdata_out <= 32'b0;
                    gt3_txcharisk_out <= 4'b0;
                    gt4_txdata_out <= 32'b0;
                    gt4_txcharisk_out <= 4'b0;
                    gt5_txdata_out <= 32'b0;
                    gt5_txcharisk_out <= 4'b0;
                    gt6_txdata_out <= 32'b0;
                    gt6_txcharisk_out <= 4'b0;
                    gt7_txdata_out <= 32'b0;
                    gt7_txcharisk_out <= 4'b0;
                    gt8_txdata_out <= 32'b0;
                    gt8_txcharisk_out <= 4'b0;
                    gt9_txdata_out <= 32'b0;
                    gt9_txcharisk_out <= 4'b0;
                    gt10_txdata_out <= 32'b0;
                    gt10_txcharisk_out <= 4'b0;
                    gt11_txdata_out <= 32'b0;
                    gt11_txcharisk_out <= 4'b0;
                    
                    TX0_state <= TRACK1;
                end                
                TRACK1 : begin
                    gt0_txdata_out <= 32'b0;
                    gt0_txcharisk_out <= 4'h0;
                    gt1_txdata_out <= 32'b0;
                    gt1_txcharisk_out <= 4'h0;
                    gt2_txdata_out <= 32'b0;
                    gt2_txcharisk_out <= 4'h0;
                    gt3_txdata_out <= 32'b0;
                    gt3_txcharisk_out <= 4'h0;
                    gt4_txdata_out <= 32'b0;
                    gt4_txcharisk_out <= 4'h0;
                    gt5_txdata_out <= 32'b0;
                    gt5_txcharisk_out <= 4'h0;
                    gt6_txdata_out <= 32'b0;
                    gt6_txcharisk_out <= 4'h0;
                    gt7_txdata_out <= 32'b0;
                    gt7_txcharisk_out <= 4'h0;
                    gt8_txdata_out <= 32'b0;
                    gt8_txcharisk_out <= 4'h0;
                    gt9_txdata_out <= 32'b0;
                    gt9_txcharisk_out <= 4'h0;
                    gt10_txdata_out <= 32'b0;
                    gt10_txcharisk_out <= 4'h0;
                    gt11_txdata_out <= 32'b0;
                    gt11_txcharisk_out <= 4'h0;
                    
                    TX0_state <= TRACK2;
                end
                TRACK2 : begin
                    gt0_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt0_txcharisk_out <= 4'b0100;
                    gt1_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt1_txcharisk_out <= 4'b0100;
                    gt2_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt2_txcharisk_out <= 4'b0100;
                    gt3_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt3_txcharisk_out <= 4'b0100;
                    gt4_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt4_txcharisk_out <= 4'b0100;
                    gt5_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt5_txcharisk_out <= 4'b0100;
                    gt6_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt6_txcharisk_out <= 4'b0100;
                    gt7_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt7_txcharisk_out <= 4'b0100;
                    gt8_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt8_txcharisk_out <= 4'b0100;
                    gt9_txdata_out <= {8'b0, 8'hfd, 16'b0};
                    gt9_txcharisk_out <= 4'b0100;
                    gt10_txdata_out <= {16'hfd, 16'b0};
                    gt10_txcharisk_out <= 4'b0100;
                    gt11_txdata_out <= {16'hfd, 16'b0};
                    gt11_txcharisk_out <= 4'b0100;
                                                                                
                    TX0_state <= FOOTER;
                end
                FOOTER : begin
                    gt0_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt0_txcharisk_out <= 4'b1;                
                    gt1_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt1_txcharisk_out <= 4'b1;
                    gt2_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt2_txcharisk_out <= 4'b1;
                    gt3_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt3_txcharisk_out <= 4'b1;
                    gt4_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt4_txcharisk_out <= 4'b1;
                    gt5_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt5_txcharisk_out <= 4'b1;
                    gt6_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt6_txcharisk_out <= 4'b1;
                    gt7_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt7_txcharisk_out <= 4'b1;
                    gt8_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt8_txcharisk_out <= 4'b1;
                    gt9_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt9_txcharisk_out <= 4'b1;
                    gt10_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt10_txcharisk_out <= 4'b1;
                    gt11_txdata_out <= {8'hc5, 8'hc5, 8'hc5, 8'hbc};
                    gt11_txcharisk_out <= 4'b1;
                    
                    TX0_state <= HEADER;
                end
                default : begin
                    gt0_txdata_out <= 32'heeee;
                    gt0_txcharisk_out <= 4'h0;
                    gt1_txdata_out <= 32'heeee;
                    gt1_txcharisk_out <= 4'h0;
                    gt2_txdata_out <= 32'heeee;
                    gt2_txcharisk_out <= 4'h0;
                    gt3_txdata_out <= 32'heeee;
                    gt3_txcharisk_out <= 4'h0;
                    gt4_txdata_out <= 32'heeee;
                    gt4_txcharisk_out <= 4'h0;
                    gt5_txdata_out <= 32'heeee;
                    gt5_txcharisk_out <= 4'h0;
                    gt6_txdata_out <= 32'heeee;
                    gt6_txcharisk_out <= 4'h0;
                    gt7_txdata_out <= 32'heeee;
                    gt7_txcharisk_out <= 4'h0;
                    gt8_txdata_out <= 32'heeee;
                    gt8_txcharisk_out <= 4'h0;
                    gt9_txdata_out <= 32'heeee;
                    gt9_txcharisk_out <= 4'h0;
                    gt10_txdata_out <= 16'heeee;
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= 16'heeee;
                    gt11_txcharisk_out <= 2'b00;
                    TX0_state <= PAUSE;
                end                        
            endcase
        end
    end
    
/*    
    always @(posedge gt_txusrclk_in_80 or posedge RST_in) begin
        //TXcounter80 <= TXcounterall_w; //for loopback test
        if (RST_in) begin
            TX10_state <= PAUSE;
            gt10_txdata_out <= 16'hbcbc;
            gt10_txcharisk_out <= 2'h1;
            gt11_txdata_out <= 16'hbcbc;
            gt11_txcharisk_out <= 2'h1;
            TX10_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX10_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX10_CLK_is_high <= 1'b0;
                        TX10_state <= PAUSE;
                    end
                    else if(CLK_in && !TX10_CLK_is_high) begin
                        TX10_CLK_is_high <= 1'b1;                        
                        TX10_state <= HEADER;
                    end
                    else begin
                        TX10_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    gt10_txdata_out <= {8'hbc, 8'hbc};
                    gt10_txcharisk_out <= 2'h1;
                    gt11_txdata_out <= {8'hbc, 8'hbc};
                    gt11_txcharisk_out <= 2'h1;
                    TX10_state <= FOOTER;
                end                
                FOOTER : begin
                    gt10_txdata_out <= {flag_S0_in[3:0], 12'b0};
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= {flag_S1_in[3:0], 12'b0};
                    gt11_txcharisk_out <= 2'b00;                 
                    TX10_state <= HEADER;
                end
                default : begin
                    gt10_txdata_out <= 16'heeee;
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= 16'heeee;
                    gt11_txcharisk_out <= 2'b00;
                    TX10_state <= PAUSE;
                end                        
            endcase
        end
    end
*/
endmodule
