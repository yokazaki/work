`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/04/26 21:30:31
// Design Name: 
// Module Name: PhaseAlign
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PhaseAlign(
    input wire CLK_0_in,
    input wire CLK_1_in,
    input wire CLK_2_in,
    input wire CLK_3_in,
    input wire [20:0] data_in,
    input wire [3:0] phase_in,
    output reg [3:0] phase_out,
    input wire reset_in,
    output reg [20:0] data_out
    );

reg [20:0] databuf_0 = 21'b0;
reg [20:0] databuf_1 = 21'b0;
reg [20:0] databuf_2 = 21'b0;
reg [20:0] databuf_3 = 21'b0;

always@ (posedge CLK_0_in) begin
    databuf_0 <= data_in;
    
    if(reset_in) data_out <= 21'h0;
    else if(phase_in==0) data_out <= databuf_0;
    else if(phase_in==1) data_out <= databuf_1;
    else if(phase_in==2) data_out <= databuf_2;
    else if(phase_in==3) data_out <= databuf_3;
    else data_out <= data_in; // through, fastest
    
    // alignment information to be monitored
    if(reset_in) phase_out = 4'hf;
    else if(databuf_0 != databuf_1) phase_out=0;
    else if(databuf_1 != databuf_2) phase_out=1;
    else if(databuf_2 != databuf_3) phase_out=2;
    else if(databuf_3 != data_in) phase_out=3;
    else phase_out = 4'he;
    
end

always@ (posedge CLK_1_in) begin
    databuf_1 <= data_in;
end

always@ (posedge CLK_2_in) begin
    databuf_2 <= data_in;
end

always@ (posedge CLK_3_in) begin
    databuf_3 <= data_in;
end

endmodule
