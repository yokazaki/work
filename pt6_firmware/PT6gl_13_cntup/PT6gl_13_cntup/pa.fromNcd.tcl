
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name PT6_gl1TCP -dir "C:/Users/takayuki/Documents/PT6/PT6gl_13/planAhead_run_1" -part xc6slx150tfgg676-2
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "C:/Users/takayuki/Documents/PT6/PT6gl_13/gl1TCP.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/takayuki/Documents/PT6/PT6gl_13} {ipcore_dir} }
add_files [list {ipcore_dir/fifo_generator_16to8.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "gl1TCP.ucf" [current_fileset -constrset]
add_files [list {gl1TCP.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "C:/Users/takayuki/Documents/PT6/PT6gl_13/gl1TCP.ncd"
if {[catch {read_twx -name results_1 -file "C:/Users/takayuki/Documents/PT6/PT6gl_13/gl1TCP.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"C:/Users/takayuki/Documents/PT6/PT6gl_13/gl1TCP.twx\": $eInfo"
}
