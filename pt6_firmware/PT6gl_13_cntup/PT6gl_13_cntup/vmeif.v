module vmeif(
	clk,
	rst,
	ia,
	data,
	innerdata1,
	data_in,
	wstr_b,
	rstr_b,
	select,
	dvalid);

input clk;
input rst;
input [3:0] ia;
inout [15:0] data;
input [15:0] innerdata1;
output reg [7:0] data_in;
reg [15:0] data_out;
input wstr_b;
input rstr_b;
input select;
reg [1:0] fpgaread;
reg [1:0] fpgawrite;
output dvalid;

always @(posedge clk or posedge rst) begin
	if (rst) begin
		data_in <= 8'h0;
		data_out <= 16'hdef0;
	end
	else if (select) begin
		data_in <= data[7:0];
		case (ia)
			4'h1: data_out <= innerdata1;
			default: data_out <= 16'hdef0;
		endcase
	end
end

always @(posedge clk) begin
	fpgaread <= {fpgaread[0], ~rstr_b};
	fpgawrite <= {fpgawrite[0], ~wstr_b};
end

assign data = (&fpgaread)? data_out: 16'hz;
assign dvalid = fpgawrite==2'b01;

endmodule
