// file: sys_dcm.v
//
// DISCLAIMER OF LIABILITY
//
// This file contains proprietary and confidential information of
// Xilinx, Inc. ("Xilinx"), that is distributed under a license
// from Xilinx, and may be used, copied and/or disclosed only
// pursuant to the terms of a valid license agreement with Xilinx.
//
// XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION
// ("MATERIALS") "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
// EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING WITHOUT
// LIMITATION, ANY WARRANTY WITH RESPECT TO NONINFRINGEMENT,
// MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE. Xilinx
// does not warrant that functions included in the Materials will
// meet the requirements of Licensee, or that the operation of the
// Materials will be uninterrupted or error-free, or that defects
// in the Materials will be corrected. Furthermore, Xilinx does
// not warrant or make any representations regarding use, or the
// results of the use, of the Materials in terms of correctness,
// accuracy, reliability or otherwise.
//
// Xilinx products are not designed or intended to be fail-safe,
// or for use in any application requiring fail-safe performance,
// such as life-support or safety devices or systems, Class III
// medical devices, nuclear facilities, applications related to
// the deployment of airbags, or any other applications that could
// lead to death, personal injury or severe property or
// environmental damage (individually and collectively, "critical
// applications"). Customer assumes the sole risk and liability
// of any use of Xilinx products in critical applications,
// subject only to applicable laws and regulations governing
// limitations on product liability.
//
// Copyright 2008, 2009 Xilinx, Inc.
// All rights reserved.
//
// This disclaimer and copyright notice must be retained as part
// of this file at all times.
//
//----------------------------------------------------------------------------
// User entered comments
//----------------------------------------------------------------------------
// None
//
//----------------------------------------------------------------------------
// Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
// Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
//----------------------------------------------------------------------------
// CLK_OUT1   150.000      0.000    50.000      212.278     60.000
//
//----------------------------------------------------------------------------
// Input Clock   Input Freq (MHz)   Input Jitter (UI)
//----------------------------------------------------------------------------
// primary             125            0.010

`timescale 1ps/1ps

(* CORE_GENERATION_INFO = "sys_dcm,clk_wiz_v1_4,{component_name=sys_dcm,use_phase_alignment=true,use_min_o_jitter=false,use_max_i_jitter=false,use_dyn_phase_shift=false,use_inclk_switchover=false,use_dyn_reconfig=false,feedback_source=FDBK_AUTO,diff_ext_feedback=false,primtype_sel=DCM_SP,num_out_clk=1,clkin1_period=5.0,clkin2_period=5.0,use_power_down=false}" *)
module dcm25q
 (// Clock in ports
  input         CLK_IN1,
  // Clock out ports
  output        CLK_OUT1,
  // Status and control signals
  input         RESET,
  output        LOCKED
 );

  // Input buffering
  //------------------------------------
  assign clkin1 = CLK_IN1;



  // Clocking primitive
  //------------------------------------
  // Instantiation of the DCM primitive
  //    * Unused inputs are tied off
  //    * Unused outputs are labeled unused
//  wire        psdone_unused;
  wire        locked_int;
//  wire [7:0]  status_int;
  wire clkfb;
  wire clk0;
//  wire clk90_unused;
//  wire clk180_unused;
//  wire clk270_unused;
//  wire clk2x_unused;
//  wire clk2x180_unused;
  wire clkfx;
//  wire clkfx180_unused;
//  wire clkdv_unused;

  DCM_SP
  #(.CLKDV_DIVIDE          (2.000),
    .CLKFX_DIVIDE          (10),
    .CLKFX_MULTIPLY        (2),
    .CLKIN_DIVIDE_BY_2     ("FALSE"),
    .CLKIN_PERIOD          (5.0),
    .CLKOUT_PHASE_SHIFT    ("NONE"),
    .CLK_FEEDBACK          ("1X"),
    .DESKEW_ADJUST         ("SYSTEM_SYNCHRONOUS"),
    .PHASE_SHIFT           (0),
    .STARTUP_WAIT          ("FALSE"))
  dcm_sp_inst
    // Input clock
   (.CLKIN                 (clkin1),
    .CLKFB                 (clkfb),
    // Output clocks
    .CLK0                  (clk0),
//    .CLK90                 (clk90_unused),
//    .CLK180                (clk180_unused),
//    .CLK270                (clk270_unused),
//    .CLK2X                 (clk2x_unused),
//    .CLK2X180              (clk2x180_unused),
    .CLKFX                 (clkfx),
//    .CLKFX180              (clkfx180_unused),
//    .CLKDV                 (clkdv_unused),
    // Ports for dynamic phase shift
    .PSCLK                 (1'b0),
    .PSEN                  (1'b0),
    .PSINCDEC              (1'b0),
//    .PSDONE                (psdone_unused),
    // Other control and status signals
    .LOCKED                (locked_int),
//    .STATUS                (status_int),
    .RST                   (RESET),
    // Unused pin- tie low
    .DSSEN                 (1'b0));

    assign LOCKED = locked_int;


  // Output buffering
  //-----------------------------------
  AUTOBUF #(.BUFFER_TYPE ("BUFG")) clkout1_buf
   (.O (CLK_OUT1),
    .I (clkfx));

  AUTOBUF #(.BUFFER_TYPE ("BUFG")) clkf_buf
   (.O (clkfb),
    .I (clk0));




endmodule