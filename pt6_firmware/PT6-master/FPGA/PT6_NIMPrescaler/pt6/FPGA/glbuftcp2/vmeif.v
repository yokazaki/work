module vmeif
(
	input wire clk,
	input wire rst,
	input wire [3:0] ia,
	inout wire [15:0] data,
	input wire [15:0] innerdata1,
	input wire [15:0] innerdata2,
	output reg [15:0] data_in,
	input wire wstr_b,
	input wire rstr_b,
	input wire select,
	output wire dvalid
);

	reg [15:0] data_out;
	reg [1:0] fpgaread;
	reg [1:0] fpgawrite;

always @(posedge clk or posedge rst) begin
	if (rst) begin
		data_in <= 16'h0;
		data_out <= 16'hdef0;
	end
	else if (select) begin
		data_in <= data[15:0];
		case (ia)
			4'h1: data_out <= innerdata1;
			4'h2: data_out <= innerdata2;
			default: data_out <= 16'hdef0;
		endcase
	end
end

always @(posedge clk) begin
	fpgaread <= {fpgaread[0], ~rstr_b};
	fpgawrite <= {fpgawrite[0], ~wstr_b};
end

assign data = (&fpgaread)? data_out: 16'hz;
assign dvalid = fpgawrite==2'b01;

endmodule
