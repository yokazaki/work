module tri_fifo
(
input rx0clk,
input rx1clk,
input sysclk,
input rst,

input [15:0] rx0,
input [15:0] rx1,
input [1:0] rx0flag,
input [1:0] rx1flag,

input tcp_open,
input tcp_txfull,
output tcp_txwr,
output [7:0] tcp_txdata,

input [15:0] maxcount,
input g0en,
input g1en,
input nimtrig,
input rbcp_trig,
output trig,
output reg [7:0] ovfcnt = 8'h0,
output reg [7:0] ovfcnt0 = 8'h0,
output reg [7:0] ovfcnt1 = 8'h0
);

wire valid0, valid1;
wire [15:0] rd_data_count0, rd_data_count1;
wire [17:0] dout0, dout1;
wire [10:0] wr_data_count;
wire overflow, overflow0, overflow1;

reg [17:0] rx0data_reg = 18'h0ffff;
reg [17:0] rx0_reg = 18'h0ffff;
reg [17:0] rx1data_reg = 18'h0ffff;
reg [17:0] rx1_reg = 18'h0ffff;
reg [31:0] cnt0 = 32'h0;
reg [31:0] cnt1 = 32'h0;
reg trig0 = 1'b0;
reg trig1 = 1'b0;
reg trig0_sync1 = 1'b0;
reg trig1_sync0 = 1'b0;
reg [15:0] maxcnt_sync0 = 16'h0400;
reg [15:0] maxcnt_sync1 = 16'h0400;

reg trig_sync0 = 1'b0;
reg trig_sync1 = 1'b0;
reg store_del = 1'b0;

//------------------------------------------------------------------------------
//	receiver for glink rx0
//------------------------------------------------------------------------------

always @(posedge rx0clk) begin
	rx0_reg <= {rx0flag[1:0], rx0[15:0]};
	maxcnt_sync0 <= maxcount;
end

always @(posedge rx0clk or posedge rst) begin
	if (rst) begin
		rx0data_reg <= 18'h0ffff;
		cnt0 <= 32'h0;
		trig0 <= 1'b0;
		trig0_sync1 <= 1'b0;
	end
	else if (!g0en) begin
		rx0data_reg[17:0] <= 18'h0ffff;
		cnt0 <= 32'h0;
		trig0 <= 1'b0;
		trig0_sync1 <= 1'b0;
	end
	else if (cnt0[31:16] > maxcnt_sync0 || nimtrig || rbcp_trig) begin
		rx0data_reg <= 18'h0ffff;
		cnt0 <= cnt0;
		trig0 <= 1'b1;
		trig0_sync1 <= 1'b1;
	end
	else begin
		trig0 <= trig0;
		trig0_sync1 <= trig1;
		rx0data_reg <= rx0_reg;
		if (rx0_reg[17:16]!=2'b11) begin
			cnt0 <= cnt0 + 1;
		end
		else begin
			cnt0 <= 32'h0;
		end
	end
end

//------------------------------------------------------------------------------
//	receiver for glink rx1
//------------------------------------------------------------------------------

always @(posedge rx1clk) begin
	rx1_reg <= {rx1flag[1:0], rx1[15:0]};
	maxcnt_sync1 <= maxcount;
end

always @(posedge rx1clk or posedge rst) begin
	if (rst) begin
		rx1data_reg <= 18'h0ffff;
		cnt1 <= 32'h0;
		trig1 <= 1'b0;
		trig1_sync0 <= 1'b0;
	end
	else if (!g1en) begin
		rx1data_reg[17:0] <= 18'h0ffff;
		cnt1 <= 32'h0;
		trig1 <= 1'b0;
		trig1_sync0 <= 1'b0;
	end
	else if (cnt1[31:16] > maxcnt_sync1 || nimtrig || rbcp_trig) begin
		rx1data_reg <= 18'h0ffff;
		cnt1 <= cnt1;
		trig1 <= 1'b1; // stop writting glbuf
		trig1_sync0 <= 1'b1;
	end
	else begin
		trig1 <= trig1;
		trig1_sync0 <= trig0;
		rx1data_reg <= rx1_reg; // to glbuf
		if (rx1_reg[17:16]!=2'b11) begin
			cnt1 <= cnt1 + 1;
		end
		else begin
			cnt1 <= 32'h0;
		end
	end
end

//------------------------------------------------------------------------------
//	trigger
//------------------------------------------------------------------------------

always @(posedge sysclk) begin
	store_del <= store;
	trig_sync0 <= trig0;
	trig_sync1 <= trig1;
end

assign trig = trig_sync0 || trig_sync1;
wire discard0 = !trig && (rd_data_count0 > 16'hff00);
wire discard1 = !trig && (rd_data_count1 > 16'hff00);
wire store = trig && (wr_data_count < 11'h7c0);

//------------------------------------------------------------------------------
//	fifo for glink rx0
//------------------------------------------------------------------------------

glbuf glbuf0 (
  .rst(rst), // input rst
  .wr_clk(rx0clk), // input wr_clk
  .rd_clk(sysclk), // input rd_clk
  .din(rx0data_reg[17:0]), // input [17 : 0] din
  .wr_en(rx0data_reg[17:16]==2'b11 && !trig0 && !trig0_sync1), // input wr_en
  .rd_en(discard0 || store), // input rd_en
  .dout(dout0), // output [17 : 0] dout
  .full(), // output full
  .almost_full(), // output almost_full
  .wr_ack(), // output wr_ack
  .overflow(overflow0), // output overflow
  .empty(), // output empty
  .almost_empty(), // output almost_empty
  .valid(valid0),  // output valid
  .underflow(), // output underflow
  .rd_data_count(rd_data_count0), // output [15 : 0] rd_data_count
  .wr_data_count() // output [15 : 0] wr_data_count
);

always @(posedge rx0clk or posedge rst) begin
	if (rst) begin
		ovfcnt0 <= 8'h0;
	end
	else if (overflow0 && ovfcnt0 < 8'hff) begin
		ovfcnt0 <= ovfcnt0 + 1;
	end
end

//------------------------------------------------------------------------------
//	fifo for glink rx1
//------------------------------------------------------------------------------

glbuf glbuf1 (
  .rst(rst), // input rst
  .wr_clk(rx1clk), // input wr_clk
  .rd_clk(sysclk), // input rd_clk
  .din(rx1data_reg[17:0]), // input [17 : 0] din
  .wr_en(rx1data_reg[17:16]==2'b11 && !trig1 && !trig1_sync0), // input wr_en
  .rd_en(discard1 || store), // input rd_en
  .dout(dout1), // output [17 : 0] dout
  .full(), // output full
  .almost_full(), // output almost_full
  .wr_ack(), // output wr_ack
  .overflow(overflow1), // output overflow
  .empty(), // output empty
  .almost_empty(), // output almost_empty
  .valid(valid1),  // output valid
  .underflow(), // output underflow
  .rd_data_count(rd_data_count1), // output [15 : 0] rd_data_count
  .wr_data_count() // output [15 : 0] wr_data_count
);

always @(posedge rx1clk or posedge rst) begin
	if (rst) begin
		ovfcnt1 <= 8'h0;
	end
	else if (overflow1 && ovfcnt1 < 8'hff) begin
		ovfcnt1 <= ovfcnt1 + 1;
	end
end

//------------------------------------------------------------------------------
//	fifo for TCP tx
//------------------------------------------------------------------------------

fifo32_8 fifo32_8 (
  .rst(rst), // input rst
  .wr_clk(sysclk), // input wr_clk
  .rd_clk(sysclk), // input rd_clk
  .din({dout1[15:0], dout0[15:0]}), // input [31 : 0] din
  .wr_en(store_del && (valid1 || valid0) && tcp_open), // input wr_en
  .rd_en(!tcp_txfull), // input rd_en
  .dout(tcp_txdata), // output [7 : 0] dout
  .full(), // output full
  .almost_full(), // output almost_full
  .wr_ack(), // output wr_ack
  .overflow(overflow), // output overflow
  .empty(), // output empty
  .almost_empty(), // output almost_empty
  .valid(tcp_txwr), // output valid
  .underflow(), // output underflow
  .rd_data_count(), // output [12 : 0] rd_data_count
  .wr_data_count(wr_data_count) // output [10 : 0] wr_data_count
);

always @(posedge sysclk or posedge rst) begin
	if (rst) begin
		ovfcnt <= 8'h0;
	end
	else if (overflow && ovfcnt < 8'hff) begin
		ovfcnt <= ovfcnt + 1;
	end
end

endmodule
