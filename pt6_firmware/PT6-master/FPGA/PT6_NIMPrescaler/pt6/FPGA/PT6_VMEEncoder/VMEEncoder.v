`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:31:38 02/17/2015 
// Design Name: 
// Module Name:    VMEEncoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


// Address Definition
`define		ADDR_Prescale_Factor		8'hf0
`define		ADDR_TestReg				8'h00





module VMEEncoder(
    input	wire 				RESET_B,
	 input	wire				CLK,
    input 	wire				CE_B,	// Chip Enable
    input 	wire				WE_B,	// Write Enable
    input 	wire	 			OE_B,	// Output Enable
    input 	wire	[7:0] 	VME_A,
    inout 	wire	[15:0] 	VME_D,
    output	reg 	[3:0] 	Prescale_Factor
    );
// Internal Register
reg	[15:0]	TestReg; 



// VME
reg rs, ws;
always @(posedge CLK)
	begin
		rs <= !(!OE_B && !CE_B);
		ws <= !(!WE_B && !CE_B);
	end
wire	[15:0]	VME_DIN;
reg	[15:0]	VME_DOUT;
assign VME_DIN = VME_D;
assign VME_D = ((!OE_B & !CE_B)) ? VME_DOUT : 16'hzzzz;



initial begin
	Prescale_Factor	<=	4'd10;
	TestReg				<= 16'h1234;
end



// WRITE
always @(negedge ws or negedge RESET_B ) begin
	if (!RESET_B) begin
		Prescale_Factor	<=	4'd10;
	end
	else begin
		case(VME_A[7:0])
			`ADDR_Prescale_Factor : begin
				Prescale_Factor <= VME_DIN[3:0];
			end
			`ADDR_TestReg : begin
				TestReg <= VME_DIN[15:0];
			end
			default : begin
				TestReg	<=	16'haaaa;
			end
		endcase
	end
end

// READ
always @(negedge rs or negedge RESET_B) begin
	if(!RESET_B) begin
		VME_DOUT <= 16'habcd;
	end
	else begin
		case(VME_A[7:0])
			`ADDR_Prescale_Factor : begin
				VME_DOUT[15:0]	<=	{12'h0, Prescale_Factor};
			end
			`ADDR_TestReg : begin
				VME_DOUT[15:0]	<=	TestReg[15:0];
			end
			default : begin
				VME_DOUT[15:0] <= 16'hffff;
			end
		endcase
	end
end

endmodule
