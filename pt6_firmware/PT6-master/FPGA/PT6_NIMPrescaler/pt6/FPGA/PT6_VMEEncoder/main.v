`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Kyoto University
// Engineer: Takuto KUNIGO
// 
// Create Date:    12/02/2015
// Design Name: 
// Module Name:    NIM Prescaler
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


 module NIMPrescaler
(
	output	wire				EXT_OUT,
	input		wire				FPGA_GCLK,
	input		wire				ALL_RESET_B,
	input		wire				FPGA_SELECT, FPGA_WSTR_B, FPGA_RSTR_B,
	input		wire	[7:0]		VME_A,
	inout		wire	[15:0]	VME_D
);

wire	[3:0]	fact;



Prescaler	Prescaler(.INPUT(FPGA_GCLK), .RESET(ALL_RESET_B), .FACTOR(fact), .OUTPUT(EXT_OUT) );

VMEEncoder	VMEEncoder( .RESET_B(ALL_RESET_B), .CLK(FPGA_GCLK),
								.CE_B(FPGA_SELECT), .WE_B(FPGA_WSTR_B), .OE_B(FPGA_RSTR_B),
								.VME_A(VME_A), .VME_D(VME_D),
								.Prescale_Factor(fact) );

endmodule // end of NIMPrescaler