`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:53:53 02/12/2015 
// Design Name: 
// Module Name:    Prescaler 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Prescaler(
    input	wire				INPUT,
	 input	wire				RESET,
    input	wire	[3:0]		FACTOR,
    output	reg				OUTPUT
    );

reg	[3:0]	counter;
initial begin
	counter	<=	4'h0;
end
 
always @(posedge INPUT or negedge RESET) begin
	if (!RESET) begin
		counter <=	4'b0;
	end
	else begin
		counter <= counter + 4'b1;

		if( counter == FACTOR ) begin
			OUTPUT	<=	1'b1;
			counter	<=	4'b0;
		end
		else begin
			OUTPUT	<=	1'b0;
		end
	end
end

endmodule
