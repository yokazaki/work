`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:31:38 02/17/2015 
// Design Name: 
// Module Name:    VMEEncoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


// Address Definition
`define		ADDR_TestReg					8'h00

`define		ADDR_glink_status_1			8'hf1
`define		ADDR_glink_status_2			8'hf2

`define		ADDR_data_10					8'h10
`define		ADDR_data_11					8'h11
`define		ADDR_data_12					8'h12
`define		ADDR_data_13					8'h13
`define		ADDR_data_14					8'h14
`define		ADDR_data_15					8'h15
`define		ADDR_data_16					8'h16
`define		ADDR_data_17					8'h17
`define		ADDR_data_18					8'h18
`define		ADDR_data_19					8'h19
`define		ADDR_data_1a					8'h1a
`define		ADDR_data_1b					8'h1b
`define		ADDR_data_1c					8'h1c
`define		ADDR_data_1d					8'h1d
`define		ADDR_data_1e					8'h1e
`define		ADDR_data_1f					8'h1f

`define		ADDR_data_20					8'h20
`define		ADDR_data_21					8'h21
`define		ADDR_data_22					8'h22
`define		ADDR_data_23					8'h23
`define		ADDR_data_24					8'h24
`define		ADDR_data_25					8'h25
`define		ADDR_data_26					8'h26
`define		ADDR_data_27					8'h27
`define		ADDR_data_28					8'h28
`define		ADDR_data_29					8'h29
`define		ADDR_data_2a					8'h2a
`define		ADDR_data_2b					8'h2b
`define		ADDR_data_2c					8'h2c
`define		ADDR_data_2d					8'h2d
`define		ADDR_data_2e					8'h2e
`define		ADDR_data_2f					8'h2f

`define		ADDR_flag_10					8'h30
`define		ADDR_flag_11					8'h31
`define		ADDR_flag_12					8'h32
`define		ADDR_flag_13					8'h33
`define		ADDR_flag_14					8'h34
`define		ADDR_flag_15					8'h35
`define		ADDR_flag_16					8'h36
`define		ADDR_flag_17					8'h37
`define		ADDR_flag_18					8'h38
`define		ADDR_flag_19					8'h39
`define		ADDR_flag_1a					8'h3a
`define		ADDR_flag_1b					8'h3b
`define		ADDR_flag_1c					8'h3c
`define		ADDR_flag_1d					8'h3d
`define		ADDR_flag_1e					8'h3e
`define		ADDR_flag_1f					8'h3f

`define		ADDR_flag_20					8'h40
`define		ADDR_flag_21					8'h41
`define		ADDR_flag_22					8'h42
`define		ADDR_flag_23					8'h43
`define		ADDR_flag_24					8'h44
`define		ADDR_flag_25					8'h45
`define		ADDR_flag_26					8'h46
`define		ADDR_flag_27					8'h47
`define		ADDR_flag_28					8'h48
`define		ADDR_flag_29					8'h49
`define		ADDR_flag_2a					8'h4a
`define		ADDR_flag_2b					8'h4b
`define		ADDR_flag_2c					8'h4c
`define		ADDR_flag_2d					8'h4d
`define		ADDR_flag_2e					8'h4e
`define		ADDR_flag_2f					8'h4f





module VMEEncoder(
    input 						RESET_B,
	 input						CLK,
    input 						CE_B,	// Chip Enable
    input 						WE_B,	// Write Enable
    input 						OE_B,	// Output Enable
    input 			[7:0] 	VME_A,
    inout 			[15:0] 	VME_D,
	 output	reg	[8:0]	glink_status_1 = 9'b0_0000_0101, glink_status_2 = 9'b0_0000_0101,
	 output	reg	[15:0]	glink_data_10,
	 output	reg	[15:0]	glink_data_11,
	 output	reg	[15:0]	glink_data_12,
	 output	reg	[15:0]	glink_data_13,
	 output	reg	[15:0]	glink_data_14,
	 output	reg	[15:0]	glink_data_15,
	 output	reg	[15:0]	glink_data_16,
	 output	reg	[15:0]	glink_data_17,
	 output	reg	[15:0]	glink_data_18,
	 output	reg	[15:0]	glink_data_19,
	 output	reg	[15:0]	glink_data_1a,
	 output	reg	[15:0]	glink_data_1b,
	 output	reg	[15:0]	glink_data_1c,
	 output	reg	[15:0]	glink_data_1d,
	 output	reg	[15:0]	glink_data_1e,
	 output	reg	[15:0]	glink_data_1f,
	 output	reg	[15:0]	glink_data_20,
	 output	reg	[15:0]	glink_data_21,
	 output	reg	[15:0]	glink_data_22,
	 output	reg	[15:0]	glink_data_23,
	 output	reg	[15:0]	glink_data_24,
	 output	reg	[15:0]	glink_data_25,
	 output	reg	[15:0]	glink_data_26,
	 output	reg	[15:0]	glink_data_27,
	 output	reg	[15:0]	glink_data_28,
	 output	reg	[15:0]	glink_data_29,
	 output	reg	[15:0]	glink_data_2a,
	 output	reg	[15:0]	glink_data_2b,
	 output	reg	[15:0]	glink_data_2c,
	 output	reg	[15:0]	glink_data_2d,
	 output	reg	[15:0]	glink_data_2e,
	 output	reg	[15:0]	glink_data_2f,
	 output	reg				glink_flag_10,
	 output	reg				glink_flag_11,
	 output	reg				glink_flag_12,
	 output	reg				glink_flag_13,
	 output	reg				glink_flag_14,
	 output	reg				glink_flag_15,
	 output	reg				glink_flag_16,
	 output	reg				glink_flag_17,
	 output	reg				glink_flag_18,
	 output	reg				glink_flag_19,
	 output	reg				glink_flag_1a,
	 output	reg				glink_flag_1b,
	 output	reg				glink_flag_1c,
	 output	reg				glink_flag_1d,
	 output	reg				glink_flag_1e,
	 output	reg				glink_flag_1f,
	 output	reg				glink_flag_20,
	 output	reg				glink_flag_21,
	 output	reg				glink_flag_22,
	 output	reg				glink_flag_23,
	 output	reg				glink_flag_24,
	 output	reg				glink_flag_25,
	 output	reg				glink_flag_26,
	 output	reg				glink_flag_27,
	 output	reg				glink_flag_28,
	 output	reg				glink_flag_29,
	 output	reg				glink_flag_2a,
	 output	reg				glink_flag_2b,
	 output	reg				glink_flag_2c,
	 output	reg				glink_flag_2d,
	 output	reg				glink_flag_2e,
	 output	reg				glink_flag_2f
    );
// Internal Register
reg	[15:0]	TestReg;


// VME
reg rs, ws;
always @(posedge CLK)
	begin
		rs <= !(!OE_B & !CE_B);
		ws <= !(!WE_B & !CE_B);
	end
wire	[15:0]	VME_DIN;
reg	[15:0]	VME_DOUT;
assign VME_DIN = VME_D;
assign VME_D = ((!OE_B & !CE_B)) ? VME_DOUT : 16'hzzzz;



initial begin
	TestReg				<= 16'h1234;

	glink_data_10	=	16'h0;
	glink_data_11	=	16'h0;
	glink_data_12	=	16'h0;
	glink_data_13	=	16'h0;
	glink_data_14	=	16'h0;
	glink_data_15	=	16'h0;
	glink_data_16	=	16'h0;
	glink_data_17	=	16'h0;
	glink_data_18	=	16'h0;
	glink_data_19	=	16'h0;
	glink_data_1a	=	16'h0;
	glink_data_1b	=	16'h0;
	glink_data_1c	=	16'h0;
	glink_data_1d	=	16'h0;
	glink_data_1e	=	16'h0;
	glink_data_1f	=	16'h0;
	glink_data_20	=	16'h0;
	glink_data_21	=	16'h0;
	glink_data_22	=	16'h0;
	glink_data_23	=	16'h0;
	glink_data_24	=	16'h0;
	glink_data_25	=	16'h0;
	glink_data_26	=	16'h0;
	glink_data_27	=	16'h0;
	glink_data_28	=	16'h0;
	glink_data_29	=	16'h0;
	glink_data_2a	=	16'h0;
	glink_data_2b	=	16'h0;
	glink_data_2c	=	16'h0;
	glink_data_2d	=	16'h0;
	glink_data_2e	=	16'h0;
	glink_data_2f	=	16'h0;
	glink_flag_10	=	1'b0;
	glink_flag_11	=	1'b0;
	glink_flag_12	=	1'b0;
	glink_flag_13	=	1'b0;
	glink_flag_14	=	1'b0;
	glink_flag_15	=	1'b0;
	glink_flag_16	=	1'b0;
	glink_flag_17	=	1'b0;
	glink_flag_18	=	1'b0;
	glink_flag_19	=	1'b0;
	glink_flag_1a	=	1'b0;
	glink_flag_1b	=	1'b0;
	glink_flag_1c	=	1'b0;
	glink_flag_1d	=	1'b0;
	glink_flag_1e	=	1'b0;
	glink_flag_1f	=	1'b0;
	glink_flag_20	=	1'b0;
	glink_flag_21	=	1'b0;
	glink_flag_22	=	1'b0;
	glink_flag_23	=	1'b0;
	glink_flag_24	=	1'b0;
	glink_flag_25	=	1'b0;
	glink_flag_26	=	1'b0;
	glink_flag_27	=	1'b0;
	glink_flag_28	=	1'b0;
	glink_flag_29	=	1'b0;
	glink_flag_2a	=	1'b0;
	glink_flag_2b	=	1'b0;
	glink_flag_2c	=	1'b0;
	glink_flag_2d	=	1'b0;
	glink_flag_2e	=	1'b0;
	glink_flag_2f	=	1'b0;
end


// WRITE
always @(negedge ws or negedge RESET_B ) begin
	if (!RESET_B) begin
		glink_status_1		<= 9'b0_0000_0101;
		glink_status_2		<= 9'b0_0000_0101;
	end
	else begin
		case(VME_A[7:0])
			`ADDR_TestReg : begin
				TestReg <= VME_DIN[15:0];
			end
			`ADDR_glink_status_1 : begin
				glink_status_1[8:0] <= VME_DIN[8:0];
			end
			`ADDR_glink_status_2 : begin
				glink_status_2[8:0] <= VME_DIN[8:0];
			end
			`ADDR_data_10 : begin
				glink_data_10 <= VME_DIN[15:0];
			end
			`ADDR_data_11 : begin
				glink_data_11 <= VME_DIN[15:0];
			end
			`ADDR_data_12 : begin
				glink_data_12 <= VME_DIN[15:0];
			end
			`ADDR_data_13 : begin
				glink_data_13 <= VME_DIN[15:0];
			end
			`ADDR_data_14 : begin
				glink_data_14 <= VME_DIN[15:0];
			end
			`ADDR_data_15 : begin
				glink_data_15 <= VME_DIN[15:0];
			end
			`ADDR_data_16 : begin
				glink_data_16 <= VME_DIN[15:0];
			end
			`ADDR_data_17 : begin
				glink_data_17 <= VME_DIN[15:0];
			end
			`ADDR_data_18 : begin
				glink_data_18 <= VME_DIN[15:0];
			end
			`ADDR_data_19 : begin
				glink_data_19 <= VME_DIN[15:0];
			end
			`ADDR_data_1a : begin
				glink_data_1a <= VME_DIN[15:0];
			end
			`ADDR_data_1b : begin
				glink_data_1b <= VME_DIN[15:0];
			end
			`ADDR_data_1c : begin
				glink_data_1c <= VME_DIN[15:0];
			end
			`ADDR_data_1d : begin
				glink_data_1d <= VME_DIN[15:0];
			end
			`ADDR_data_1e : begin
				glink_data_1e <= VME_DIN[15:0];
			end
			`ADDR_data_1f : begin
				glink_data_1f <= VME_DIN[15:0];
			end
			`ADDR_data_20 : begin
				glink_data_20 <= VME_DIN[15:0];
			end
			`ADDR_data_21 : begin
				glink_data_21 <= VME_DIN[15:0];
			end
			`ADDR_data_22 : begin
				glink_data_22 <= VME_DIN[15:0];
			end
			`ADDR_data_23 : begin
				glink_data_23 <= VME_DIN[15:0];
			end
			`ADDR_data_24 : begin
				glink_data_24 <= VME_DIN[15:0];
			end
			`ADDR_data_25 : begin
				glink_data_25 <= VME_DIN[15:0];
			end
			`ADDR_data_26 : begin
				glink_data_26 <= VME_DIN[15:0];
			end
			`ADDR_data_27 : begin
				glink_data_27 <= VME_DIN[15:0];
			end
			`ADDR_data_28 : begin
				glink_data_28 <= VME_DIN[15:0];
			end
			`ADDR_data_29 : begin
				glink_data_29 <= VME_DIN[15:0];
			end
			`ADDR_data_2a : begin
				glink_data_2a <= VME_DIN[15:0];
			end
			`ADDR_data_2b : begin
				glink_data_2b <= VME_DIN[15:0];
			end
			`ADDR_data_2c : begin
				glink_data_2c <= VME_DIN[15:0];
			end
			`ADDR_data_2d : begin
				glink_data_2d <= VME_DIN[15:0];
			end
			`ADDR_data_2e : begin
				glink_data_2e <= VME_DIN[15:0];
			end
			`ADDR_data_2f : begin
				glink_data_2f <= VME_DIN[15:0];
			end
			`ADDR_flag_10 : begin
				glink_flag_10 <= VME_DIN[0];
			end
			`ADDR_flag_11 : begin
				glink_flag_11 <= VME_DIN[0];
			end
			`ADDR_flag_12 : begin
				glink_flag_12 <= VME_DIN[0];
			end
			`ADDR_flag_13 : begin
				glink_flag_13 <= VME_DIN[0];
			end
			`ADDR_flag_14 : begin
				glink_flag_14 <= VME_DIN[0];
			end
			`ADDR_flag_15 : begin
				glink_flag_15 <= VME_DIN[0];
			end
			`ADDR_flag_16 : begin
				glink_flag_16 <= VME_DIN[0];
			end
			`ADDR_flag_17 : begin
				glink_flag_17 <= VME_DIN[0];
			end
			`ADDR_flag_18 : begin
				glink_flag_18 <= VME_DIN[0];
			end
			`ADDR_flag_19 : begin
				glink_flag_19 <= VME_DIN[0];
			end
			`ADDR_flag_1a : begin
				glink_flag_1a <= VME_DIN[0];
			end
			`ADDR_flag_1b : begin
				glink_flag_1b <= VME_DIN[0];
			end
			`ADDR_flag_1c : begin
				glink_flag_1c <= VME_DIN[0];
			end
			`ADDR_flag_1d : begin
				glink_flag_1d <= VME_DIN[0];
			end
			`ADDR_flag_1e : begin
				glink_flag_1e <= VME_DIN[0];
			end
			`ADDR_flag_1f : begin
				glink_flag_1f <= VME_DIN[0];
			end
			`ADDR_flag_20 : begin
				glink_flag_20 <= VME_DIN[0];
			end
			`ADDR_flag_21 : begin
				glink_flag_21 <= VME_DIN[0];
			end
			`ADDR_flag_22 : begin
				glink_flag_22 <= VME_DIN[0];
			end
			`ADDR_flag_23 : begin
				glink_flag_23 <= VME_DIN[0];
			end
			`ADDR_flag_24 : begin
				glink_flag_24 <= VME_DIN[0];
			end
			`ADDR_flag_25 : begin
				glink_flag_25 <= VME_DIN[0];
			end
			`ADDR_flag_26 : begin
				glink_flag_26 <= VME_DIN[0];
			end
			`ADDR_flag_27 : begin
				glink_flag_27 <= VME_DIN[0];
			end
			`ADDR_flag_28 : begin
				glink_flag_28 <= VME_DIN[0];
			end
			`ADDR_flag_29 : begin
				glink_flag_29 <= VME_DIN[0];
			end
			`ADDR_flag_2a : begin
				glink_flag_2a <= VME_DIN[0];
			end
			`ADDR_flag_2b : begin
				glink_flag_2b <= VME_DIN[0];
			end
			`ADDR_flag_2c : begin
				glink_flag_2c <= VME_DIN[0];
			end
			`ADDR_flag_2d : begin
				glink_flag_2d <= VME_DIN[0];
			end
			`ADDR_flag_2e : begin
				glink_flag_2e <= VME_DIN[0];
			end
			`ADDR_flag_2f : begin
				glink_flag_2f <= VME_DIN[0];
			end
			default : begin
				TestReg	<=	16'haaaa;
			end
		endcase
	end
end


// READ
always @(negedge rs or negedge RESET_B) begin
	if(!RESET_B) begin
		VME_DOUT 			<= 16'habcd;
	end
	else begin
		case(VME_A[7:0])
			`ADDR_TestReg : begin
				VME_DOUT[15:0]	<=	TestReg[15:0];
			end
			`ADDR_glink_status_1 : begin
				VME_DOUT[15:0]	<=	{ 7'h0, glink_status_1[8:0]};
			end
			`ADDR_glink_status_2 : begin
				VME_DOUT[15:0]	<= { 7'h0, glink_status_2[8:0]};
			end
			`ADDR_data_10 : begin
				VME_DOUT[15:0]	<=	glink_data_10;
			end
			`ADDR_data_11 : begin
				VME_DOUT[15:0]	<=	glink_data_11;
			end
			`ADDR_data_12 : begin
				VME_DOUT[15:0]	<=	glink_data_12;
			end
			`ADDR_data_13 : begin
				VME_DOUT[15:0]	<=	glink_data_13;
			end
			`ADDR_data_14 : begin
				VME_DOUT[15:0]	<=	glink_data_14;
			end
			`ADDR_data_15 : begin
				VME_DOUT[15:0]	<=	glink_data_15;
			end
			`ADDR_data_16 : begin
				VME_DOUT[15:0]	<=	glink_data_16;
			end
			`ADDR_data_17 : begin
				VME_DOUT[15:0]	<=	glink_data_17;
			end
			`ADDR_data_18 : begin
				VME_DOUT[15:0]	<=	glink_data_18;
			end
			`ADDR_data_19 : begin
				VME_DOUT[15:0]	<=	glink_data_19;
			end
			`ADDR_data_1a : begin
				VME_DOUT[15:0]	<=	glink_data_1a;
			end
			`ADDR_data_1b : begin
				VME_DOUT[15:0]	<=	glink_data_1b;
			end
			`ADDR_data_1c : begin
				VME_DOUT[15:0]	<=	glink_data_1c;
			end
			`ADDR_data_1d : begin
				VME_DOUT[15:0]	<=	glink_data_1d;
			end
			`ADDR_data_1e : begin
				VME_DOUT[15:0]	<=	glink_data_1e;
			end
			`ADDR_data_1f : begin
				VME_DOUT[15:0]	<=	glink_data_1f;
			end
			`ADDR_data_20 : begin
				VME_DOUT[15:0]	<=	glink_data_20;
			end
			`ADDR_data_21 : begin
				VME_DOUT[15:0]	<=	glink_data_21;
			end
			`ADDR_data_22 : begin
				VME_DOUT[15:0]	<=	glink_data_22;
			end
			`ADDR_data_23 : begin
				VME_DOUT[15:0]	<=	glink_data_23;
			end
			`ADDR_data_24 : begin
				VME_DOUT[15:0]	<=	glink_data_24;
			end
			`ADDR_data_25 : begin
				VME_DOUT[15:0]	<=	glink_data_25;
			end
			`ADDR_data_26 : begin
				VME_DOUT[15:0]	<=	glink_data_26;
			end
			`ADDR_data_27 : begin
				VME_DOUT[15:0]	<=	glink_data_27;
			end
			`ADDR_data_28 : begin
				VME_DOUT[15:0]	<=	glink_data_28;
			end
			`ADDR_data_29 : begin
				VME_DOUT[15:0]	<=	glink_data_29;
			end
			`ADDR_data_2a : begin
				VME_DOUT[15:0]	<=	glink_data_2a;
			end
			`ADDR_data_2b : begin
				VME_DOUT[15:0]	<=	glink_data_2b;
			end
			`ADDR_data_2c : begin
				VME_DOUT[15:0]	<=	glink_data_2c;
			end
			`ADDR_data_2d : begin
				VME_DOUT[15:0]	<=	glink_data_2d;
			end
			`ADDR_data_2e : begin
				VME_DOUT[15:0]	<=	glink_data_2e;
			end
			`ADDR_data_2f : begin
				VME_DOUT[15:0]	<=	glink_data_2f;
			end
			`ADDR_flag_10 : begin
				VME_DOUT[15:0]	<=	{ 15'h0, glink_flag_10 };
			end
			`ADDR_flag_11 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_11 };
			end
			`ADDR_flag_12 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_12 };
			end
			`ADDR_flag_13 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_13 };
			end
			`ADDR_flag_14 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_14 };
			end
			`ADDR_flag_15 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_15 };
			end
			`ADDR_flag_16 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_16 };
			end
			`ADDR_flag_17 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_17 };
			end
			`ADDR_flag_18 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_18 };
			end
			`ADDR_flag_19 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_19 };
			end
			`ADDR_flag_1a : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1a };
			end
			`ADDR_flag_1b : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1b };
			end
			`ADDR_flag_1c : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1c };
			end
			`ADDR_flag_1d : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1d };
			end
			`ADDR_flag_1e : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1e };
			end
			`ADDR_flag_1f : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_1f };
			end
			`ADDR_flag_20 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_20 };
			end
			`ADDR_flag_21 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_21 };
			end
			`ADDR_flag_22 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_22 };
			end
			`ADDR_flag_23 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_23 };
			end
			`ADDR_flag_24 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_24 };
			end
			`ADDR_flag_25 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_25 };
			end
			`ADDR_flag_26 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_26 };
			end
			`ADDR_flag_27 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_27 };
			end
			`ADDR_flag_28 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_28 };
			end
			`ADDR_flag_29 : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_29 };
			end
			`ADDR_flag_2a : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2a };
			end
			`ADDR_flag_2b : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2b };
			end
			`ADDR_flag_2c : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2c };
			end
			`ADDR_flag_2d : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2d };
			end
			`ADDR_flag_2e : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2e };
			end
			`ADDR_flag_2f : begin
				VME_DOUT[15:0]	<= { 15'h0, glink_flag_2f };
			end
			default : begin
				VME_DOUT[15:0] <= 16'hffff;
			end
		endcase
	end
end

endmodule
