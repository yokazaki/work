// pt6.cc

#include <iostream>
#include <iomanip>
#include <string>
#include <stdint.h>
#include <cstdlib>

#include "Pt6Module.hh"

using namespace std;

enum op{ CONFIGURE, ERASE, DUMP, SDUMP, BLANK, READ, WRITE } operation;
uint32_t BASE_ADDRESS = 0xff000000;  // default Address
uint32_t address = 0;
uint32_t value = 0;
char * filename = 0;

Pt6Module::chip_id chip;
Pt6Module::result return_code;

void show_usage( void )
{
	cerr << endl
	<< "pt6 <operation> (filename) <pt6 address> [arguments]" << endl
	<< endl
	<< "pt6 c <filename> <pt6 address>                          :configure FPGA" << endl
	<< "pt6 e            <pt6 address>                          :erase FPGA" << endl
	<< "pt6 d <filename> <pt6 address>                          :dump DPM" << endl
//	<< "pt6 b            <pt6 address>                          :blank DPM" << endl
	<< "pt6 r            <pt6 address> <chip> <address>         :read"  << endl
	<< "pt6 w            <pt6 address> <chip> <address> <value> :write" << endl
	<< endl
	<< "pt6 address : PT6's 32bit address '0x'+(3char)" << endl
	<< "chip        : f(FPGA), c(CPLD), d(DPM)" << endl
	<< "address     : [0-15] for FPGA, [0-65535] for DPM" << endl
	<< "              [0-15] for CPLD" << endl
	<< "value       : 32bit" << endl
	<< endl;
}

void get_operation( char * argument )
{
	if ( string( argument ) == "c" ) {
		operation = CONFIGURE;
		return;
	} else if ( string( argument ) == "e" ) {
		operation = ERASE;
		return;
	} else if ( string( argument ) == "d" ) {
		operation = DUMP;
		return;
	} else if ( string( argument ) == "s" ) {
		operation = SDUMP;
		return;
//	} else if ( string( argument ) == "b" ) {
//		operation = BLANK;
//		return;
	} else if ( string( argument ) == "r" ) {
		operation = READ;
		return;
	} else if ( string( argument ) == "w" ) {
		operation = WRITE;
		return;
	} else {
		show_usage();
		exit( 1 );
	cout << "Class" << endl;
	}
}

void get_chip_id( char * argument )
{
	if ( string( argument ) == "f" ) {
		chip = Pt6Module::FPGA;
		return;
	} else if ( string( argument ) == "c" ) {
		chip = Pt6Module::CPLD;
		return;
	} else if ( string( argument ) == "d" ) {
		chip = Pt6Module::DPM;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_address( char * argument )
{
	char * invalid_string;
	uint32_t pre_address;
	pre_address = strtoul( argument, &invalid_string, 0 );
	
	if ( ( *invalid_string == '\0' )
		&& ( 0 <= pre_address )
		&& ( pre_address <= 65535 ) )
	{
		address = pre_address;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_pt6Address( char * argument )
{
	char * invalid_string;
	uint32_t pre_address;
	
	pre_address = strtoul( argument, &invalid_string, 0 );
	
	if ( ( *invalid_string == '\0' ) && ( pre_address >= 0 ) && ( pre_address <= 4095 ) ) {
		BASE_ADDRESS = ( pre_address << 20 );
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_value( char * argument )
{
	uint32_t pre_value;
	char * invalid_string;
	pre_value = strtoul( argument, &invalid_string, 0 );
	if ( *invalid_string == '\0' ) {
		value = pre_value;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void set_operation( int argc, char * argv[] )
{
	if ( argc <= 2 ) {
		show_usage();
		exit( 1 );
	}
	get_operation( argv[1] );
	switch ( operation ) {
		case CONFIGURE:
		if ( argc != 4 ) {
				show_usage();
				exit( 1 );
		}
		filename = argv[2];
		get_pt6Address( argv[3] );
		return;
		
		case ERASE:
		if ( argc !=3 ) {
			show_usage();
			exit( 1 );
		}
		get_pt6Address( argv[2] );
		return;
		
		case DUMP:
		if ( argc !=4 ) {
			show_usage();
			exit( 1 );
		}
		filename = argv[2];
		get_pt6Address( argv[3] );
		return;
		
		case SDUMP:
		if ( argc !=4 ) {
			show_usage();
			exit( 1 );
		}
		filename = argv[2];
		get_pt6Address( argv[3] );
		return;
		
//		case BLANK:
//		if ( argc !=3 ) {
//			show_usage();
//			exit( 1 );
//		}
//		get_pt6Address( argv[2] );
//		return;
		
		case READ:
		if ( argc !=5 ) {
			show_usage();
			exit( 1 );
		}
		get_pt6Address( argv[2] );
		get_chip_id( argv[3] );
		get_address( argv[4] );
		return;
		
		case WRITE:
		if ( argc !=6 ) {
			show_usage();
			exit( 1 );
		}
		get_pt6Address( argv[2] );
		get_chip_id( argv[3] );
		get_address( argv[4] );
		get_value( argv[5] );
		return;
		
		default: // do nothing ( impossible option )
		exit( 10 );
	}
}

int main( int argc, char * argv[] )
{
  //	cout << "Start" << endl;
	set_operation( argc, argv );
	Pt6Module pt6( BASE_ADDRESS );
	switch ( operation ) {
		case CONFIGURE:
		return_code = pt6.configure( filename );
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "configure: complete" << endl;
			return 0;
			
			case Pt6Module::CANNOT_OPEN_FILE:
			cerr << "cannot open " << filename <<endl;
			return -1;
			
			case Pt6Module::INVALID_FILE:
			cerr << "configure: invalid file" << endl;
			return -1;
			
			case Pt6Module::CONF_FAIL:
			cerr << "configure: failure" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case ERASE:
		return_code = pt6.erase();
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "erase: complete" << endl;
			return 0;
			
			case Pt6Module::ERASE_FAIL:
			cerr << "erase: failure" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case DUMP:
		return_code = pt6.dump( filename );
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "dump: complete" << endl;
			return 0;
			
			case Pt6Module::CANNOT_OPEN_FILE:
			cerr << "cannot open " << filename <<endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case SDUMP:
		return_code = pt6.sdump( filename );
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "sdump: complete" << endl;
			return 0;
			
			case Pt6Module::CANNOT_OPEN_FILE:
			cerr << "cannot open " << filename <<endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
//		case BLANK:
//		return_code = pt6.blank();
//		switch ( return_code ) {
//			case Pt6Module::NO_ERROR:
//			cerr << "blank: complete" << endl;
//			return 0;
//			
//			default: // do nothing ( impossible option )
//			return -10;
//		}
//		return -10;
		
		case READ:
		return_code = pt6.readvme( chip, address, &value );
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "read: chip = " << argv[3] << ", "
			<< "address = " << setw( 2 ) << address << ", "
			<< "value = 0x" << setw( 8 ) << setfill('0') << hex << value << endl;
			return 0;
			
			case Pt6Module::INVALID_CHIP_ID:
			cerr << "read: invalid chip ID" << endl
			<< "f(FPGA), c(CPLD), d(DPM) are allowed" << endl;
			return -1;
			
			case Pt6Module::INVALID_ADDRESS:
			cerr << "read: invalid address" << endl
			<< "[0-15] for FPGA and CPLD," << endl
			<< "[0-65535] for DPM are allowed" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case WRITE:
		return_code = pt6.writevme( chip, address, value );
		switch ( return_code ) {
			case Pt6Module::NO_ERROR:
			cerr << "write: chip = " << argv[3] << ", "
			<< "address = " << setw( 2 ) << address << ", "
			<< "value = 0x"
			<< setw( 8 ) << setfill('0') << hex
			<< value << endl;
			return 0;
			
			case Pt6Module::INVALID_CHIP_ID:
			cerr << "write: invalid chip ID" << endl
			<< "f(FPGA), c(CPLD), d(DPM) are allowed" << endl;
			return -1;
			
			case Pt6Module::INVALID_ADDRESS:
			cerr << "write: invalid address" << endl
			<< "[0-15] for FPGA and CPLD," << endl
			<< "[0-65535] for DPM are allowed" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		default: // do nothing ( impossible option )
		return -10;
	}
}
