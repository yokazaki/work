// This is a brief code for dumpping TCP protocol data
// header for C++
#include <iostream>
#include <ofstream>
#include <string>
#include <cstdlib>
#include <cstdint>

// header for socket programming
#include <sys/socket.h>
#include <netdb.h>



void show_usage()
{
  cout << "[usage] TCPdump [output file name] [Server IP 0] [Server IP 1] ..." << endl;
  cout << "[usage] Server IP 192.168.10.xxx" << endl;
  cout << "[usage] Server IP : lowest significant byte of IP address" << endl;
  cout << "[usage] e.g. TCPdump test 16" << endl;
}


// server IP address
const int32_t ip_1st = 192;
const int32_t ip_2nd = 168;
const int32_t ip_3rd =  10;
const int32_t tcp_port = 24;




int main( int argc, char* argv[] )
{
  if ( argc < 3 ) {
    show_usage();
    return EXIT_FAIRULE;
  }


  vector <uint32_t> addr;
  for ( int32_t srv_i  = 2;  srv_i < argc; srv_i++ ) {
    uint32_t temp_addr = atol( argv[i] );


    cout << "server : IP address = 192.168.10." << temp_addr << endl;
    cout << "server : TCP port number = " << tcp_port << endl;
  } 





  return EXIT_SUCCESS;
}
