`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/11/29 21:54:22
// Design Name: 
// Module Name: BW_Coincidence
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BW_Coincidence(
    input wire CLK_in,
    input wire reset_in,

    input wire [6:0] BW_R, 
    input wire [4:0] BW_Phi0, 
    input wire [4:0] BW_Phi1, 
    input wire [4:0] BW_Phi2, 
    input wire [4:0] BW_Phi3, 

    input wire [4:0] LUT_init_data_in,
    input wire [11:0] LUT_init_Address_in,
    input wire [2:0] LUT_init_type_in,
    input wire LUT_init_flg_in,
    input wire LUT_init_R_in,
    input wire [1:0] LUT_init_Phi_in,
    input wire [1:0] LUT_init_mode_in,

    input wire [11:0] LUT_rd_address_in,
    input wire [2:0] LUT_rd_type_in,
    input wire LUT_rd_R_in,
    input wire [2:0] LUT_rd_Phi_in,
    output wire [4:0] LUT_rd_data_out,

    output reg [8:0] trig_SSC_BW
    );

function valid_hit;
input [3:0] v_pT;
begin
    if ( (v_pT != 4'b1111) && (v_pT != 4'b0000) ) begin
        valid_hit = 1'b1;
    end
    else begin
        valid_hit = 1'b0;
    end
end
endfunction

function [1:0] whichSSC;
input  [4:0] w_pT_L;
input  [4:0] w_pT_R;
begin
    if((valid_hit(w_pT_L[3:0]) == 1'b1) && (valid_hit(w_pT_R[3:0]) == 1'b1)) begin
        if(w_pT_L[3:0] >= w_pT_R[3:0]) begin
            whichSSC[1] = 1'b0;
            whichSSC[0] = w_pT_L[4];
        end
        else begin
            whichSSC[1] = 1'b1;
            whichSSC[0] = w_pT_R[4];
        end
    end
    else if(valid_hit(w_pT_L[3:0]) == 1'b1) begin
        whichSSC[1] = 1'b0;
        whichSSC[0] = w_pT_L[4];
    end
    else if(valid_hit(w_pT_R[3:0]) == 1'b1) begin
        whichSSC[1] = 1'b1;
        whichSSC[0] = w_pT_R[4];
    end
    else begin
        whichSSC[1:0] = 2'b00;
    end
end
endfunction     

function [5:0] comparepT;
input  [5:0] w_pT_L;
input  [5:0] w_pT_R;
begin
    if((valid_hit(w_pT_L[3:0]) == 1'b1) && (valid_hit(w_pT_R[3:0]) == 1'b1)) begin
        if(w_pT_L[3:0] >= w_pT_R[3:0]) begin
            comparepT[5:0] = w_pT_L[5:0];
        end
        else begin
            comparepT[5:0] = w_pT_R[5:0];
        end
    end
    else if(valid_hit(w_pT_L[3:0]) == 1'b1) begin
        comparepT[5:0] = w_pT_L[5:0];
    end
    else if(valid_hit(w_pT_R[3:0]) == 1'b1) begin
        comparepT[5:0] = w_pT_R[5:0];
    end
    else begin
        comparepT[5:0] = 6'b0;
    end
end
endfunction // comparepT

function Set_flag;
input wire_flag;
input strip_flag;
begin   
    if((wire_flag == 1'b1) && (strip_flag == 1'b1)) begin
        Set_flag = 1'b1;
    end
    else begin
        Set_flag = 1'b0;
    end
end
endfunction
   

wire wea_phi0 = (LUT_init_Phi_in == 0) ? LUT_init_flg_in : 1'b0;
wire wea_phi1 = (LUT_init_Phi_in == 1) ? LUT_init_flg_in : 1'b0;
wire wea_phi2 = (LUT_init_Phi_in == 2) ? LUT_init_flg_in : 1'b0;
wire wea_phi3 = (LUT_init_Phi_in == 3) ? LUT_init_flg_in : 1'b0;
wire ena=1'b1;

wire BW_flag0 = Set_flag({BW_R[5]},{BW_Phi0[4]});
wire BW_flag1 = Set_flag({BW_R[5]},{BW_Phi1[4]});
wire BW_flag2 = Set_flag({BW_R[5]},{BW_Phi2[4]});
wire BW_flag3 = Set_flag({BW_R[5]},{BW_Phi3[4]});

wire [3:0] pT_0_BW;
wire [3:0] pT_1_BW;
wire [3:0] pT_2_BW;
wire [3:0] pT_3_BW;
wire  charge_0_BW;
wire  charge_1_BW;
wire  charge_2_BW;
wire  charge_3_BW;

wire [11:0] LUT_address =  (LUT_init_mode_in == 1) ? LUT_init_Address_in : 
                        (LUT_init_mode_in == 2) ? LUT_rd_address_in : 
                        12'h0;

assign LUT_rd_data_out[4:0] = 
(LUT_rd_type_in == 1 && LUT_rd_R_in == 1 && LUT_rd_Phi_in == 0) ? {charge_0_BW, pT_0_BW } :
(LUT_rd_type_in == 1 && LUT_rd_R_in == 1 && LUT_rd_Phi_in == 1) ? {charge_1_BW, pT_1_BW } :
(LUT_rd_type_in == 1 && LUT_rd_R_in == 1 && LUT_rd_Phi_in == 2) ? {charge_2_BW, pT_2_BW } :
(LUT_rd_type_in == 1 && LUT_rd_R_in == 1 && LUT_rd_Phi_in == 3) ? {charge_3_BW, pT_3_BW } : 5'b0;

// real

// LUT input data format : {R_pos, PhiHL, RHL, +-dPhi, +-dR}
//
LUT12 BW_Phi0_R0(  .clka(CLK_in), .ena(ena), .wea(wea_phi0), .addra( (LUT_init_mode_in == 0) ? {BW_R[6], BW_Phi0[4], BW_R[5], BW_Phi0[3:0], BW_R[4:0]} : LUT_address[11:0]), .dina(LUT_init_data_in[4:0]), .douta({charge_0_BW, pT_0_BW}) );
LUT12 BW_Phi1_R0(  .clka(CLK_in), .ena(ena), .wea(wea_phi1), .addra( (LUT_init_mode_in == 0) ? {BW_R[6], BW_Phi1[4], BW_R[5], BW_Phi1[3:0], BW_R[4:0]} : LUT_address[11:0]), .dina(LUT_init_data_in[4:0]), .douta({charge_1_BW, pT_1_BW}) );
LUT12 BW_Phi2_R0(  .clka(CLK_in), .ena(ena), .wea(wea_phi2), .addra( (LUT_init_mode_in == 0) ? {BW_R[6], BW_Phi2[4], BW_R[5], BW_Phi2[3:0], BW_R[4:0]} : LUT_address[11:0]), .dina(LUT_init_data_in[4:0]), .douta({charge_2_BW, pT_2_BW}) );
LUT12 BW_Phi3_R0(  .clka(CLK_in), .ena(ena), .wea(wea_phi3), .addra( (LUT_init_mode_in == 0) ? {BW_R[6], BW_Phi3[4], BW_R[5], BW_Phi3[3:0], BW_R[4:0]} : LUT_address[11:0]), .dina(LUT_init_data_in[4:0]), .douta({charge_3_BW, pT_3_BW}) );


//simple test
/*
reg [3:0] pT0;
reg [3:0] pT1;
reg [3:0] pT2;
reg [3:0] pT3;
assign pT_0_BW = pT0;
assign pT_1_BW = pT1;
assign pT_2_BW = pT2;
assign pT_3_BW = pT3;


always @(posedge CLK_in) begin
    pT0 <= {BW_R[1:0], BW_Phi0[1:0]};
    pT1 <= {BW_R[1:0], BW_Phi1[1:0]};
    pT2 <= {BW_R[1:0], BW_Phi2[1:0]};
    pT3 <= {BW_R[1:0], BW_Phi3[1:0]};
end
*/
//simple test

wire [7:0] SSC_pT_L = {whichSSC({1'b0, pT_0_BW}, {1'b1, pT_1_BW}), comparepT({BW_flag0, charge_0_BW, pT_0_BW}, {BW_flag1, charge_1_BW, pT_1_BW})};
wire [7:0] SSC_pT_R = {whichSSC({1'b0, pT_2_BW}, {1'b1, pT_3_BW}), comparepT({BW_flag2, charge_2_BW, pT_2_BW}, {BW_flag3, charge_3_BW, pT_3_BW})};

wire [5:0] SSC_pT = comparepT({SSC_pT_L[5:0]}, {SSC_pT_R[5:0]});
wire [2:0] pos_SSC;
assign pos_SSC[1:0] = whichSSC({SSC_pT_L[6], SSC_pT_L[3:0]}, {SSC_pT_R[6], SSC_pT_R[3:0]});

reg [1:0] pre_BW_R;

always @(posedge CLK_in or posedge reset_in) begin
    if(reset_in) begin
        trig_SSC_BW <= 8'b0;
        pre_BW_R <= 2'b0;
    end
    else begin
        pre_BW_R[0] <= BW_R[6];
        trig_SSC_BW[8:0] <= {SSC_pT[5:4], pre_BW_R[0], pos_SSC[1:0],SSC_pT[3:0]};
    end
end

endmodule
